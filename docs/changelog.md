changelog
===========

1.3
------
briefme view
switched to wkwebview
blur overhaul
push notification fixes
request optimization
watch handoff fixes
removed tutorial
updated event tagging
fixed memory leaks
night theme update
increased app timeout
updated icons

1.2
------
watch redesign
6+/ipad landscape launch fix
webview auto-scroll fix
watch events

1.1.7
------
Updated tutorial
added and fixed event tagging
ios 7 notification bug
removed winners animation
Auto theme and bedtime view prompt
feedback prompt update
improved swipe back
status bar in-article
videos work in landscape
videos sound override ringer
added faq
added terms
open in safari extension
updated nighttime highlight color
better docs
timeout 5 min -> 10 min

1.1.3-1.1.6
------
ios 7 onboarding bug
daily three banner fix
ratings prompt fix
new webblocks
BriefMe News -> BriefMe
updated tutorial

1.1.2
------
Winners -> Daily Three
removed unused assets
added Fabric
padding and font sizes

1.1.0-1.1.1
------
new stream cell
visited cell updates
updated AFNetworking
fixed tab bar bugs
crashes to localytics
ios 7 compatibility errors
fixed black viewcontroller bug
feedback cell fixed bug
new webblocks
source icon fade

1.0.20
------
fixed resetCells via feedbackcell
winners hide banner fix
expanding cell fix
feedback depends on config

1.0.18-1.0.19
------
tutorial progress bar
tutorial bobbing and scroll fix
briefme score -> source image
new source image icons for sections
latent feedback cell
fb share updated

1.0.17
------
winners share fix

1.0.16
------
Sharing fixes
Red -> charcoal for score, ranking

1.0.15
------
Localytics v2->v3
Removed auto theme
Winners visited
Localytics tutorial fix
Some adblocks
Added parse
6 plus bar issue
Sharing uses shortened url

1.0.8-1.0.14
------
Winners banner and fixes
webblocks fixes
Tutorial fixes
Image changes on refresh fade
Blur changes
Fixed black circle bug
uses /all route

1.0.7
------
Swiping between sections
Improved cell summaries
Added pocket integration
Removed social section

1.0.6
------
Removed intrusive article ads
Background timeout refresh
Updated night theme
Fixed twitter bugs
Added static reddit section
Added article summary on hold

1.0.5
------
Initial working build
