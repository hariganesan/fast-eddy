merge/upload checklist
===============
- Update changelog
- Test sharing on FB, TW, Copy, Mail, Message, Winners, Stream
- Make sure all settings work
- Do at least one refresh where the image changes (stream is easiest)
- Tap briefme scores and check values
- Make sure launches well on all devices
- test iOS 7
- test all themes
- test notifications/daily 3 views
- check TODOs
- build for all active architectures
...
