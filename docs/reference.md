Reference
=========
Please add any new material that you find useful to this repository!

Version Control
---------------
- [git cheatsheet](http://www.ndpsoftware.com/git-cheatsheet.html)
- [git operations](http://stackoverflow.com/questions/3329943/git-branch-fork-fetch-merge-rebase-and-clone-what-are-the-differences)
- [git branching](http://nvie.com/posts/a-successful-git-branching-model/)
- [git aliases](https://git-scm.com/book/en/v2/Git-Basics-Git-Aliases)

General Info
------------
- [fun objective-c tutorial](http://tryobjectivec.codeschool.com)
- network link conditioner, or some other speed inhibitor depending on your computer

Apple Docs
----------
- [apple HIG](https://developer.apple.com/library/ios/documentation/UserExperience/Conceptual/MobileHIG/MobileHIG.pdf)
- apple ios developer docs... option-click on things in Xcode
