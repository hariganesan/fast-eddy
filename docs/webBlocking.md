Using webBlocks.json
===============

###I use three basic js commands to hide objects in the uiwebviews. 
- The main command sets the css 'display' property to 'none'. Display:none hides an element, and it will not take up any space. 
- The second command sets the css 'visibility' property to 'hidden'. Visibility:hidden hides an element, but it will still take up the same space as before. 
- The third command clicks an element, usually an (x) on an annoying add. 

###Each command can be called on a DOM object in the html. I use chrome's developer tools (inspect element etc) to test these commands in browser first. To emulate an iPhone screen, I use device-mode in Google Chrome Canary. To access the specific objects that we wan't to block:
- Use document.getElementById(<id>) for an obj with an id.
- Use document.getElementsByClass(<class>) for an obj with a class.

###webBlocks.json is a big object, using website hosts as keys. A template object is:

    "FAKE-JSON-TEMPLATE.com": {
        "display-none": {
            "ids": ["FAKEID1", "FAKEID2"],
            "class-arrays": ["FAKECLASS1", "FAKECLASS2"],
            "classes": [{
                "name": "FAKECLASS3",
                "index": 0
            }, {
                "name": "FAKCECLASS4",
                "index": 0
            }]
        },
        "visibility-hidden": {
            "ids": ["FAKCEID3", "FAKEID4"],
            "class-arrays": ["FAKECLASS5"],
            "classes": [{
                "name": "FAKECLASS6",
                "index": 0
            }]
        },
        "click": {
            "ids": ["FAKEID5", "FAKEID6"],
            "class-arrays": ["FAKECLASS7"],
            "classes": [{
                "name": "FAKECLASS8",
                "index": 0
            }]
        }
    },

###Each website object can have any of the 3 commands "display-none", "visibility-hidden", and/or "click". For each command, there are three options for the objects which the command will be called on. 
- The first option is an array of id names which has the key "ids".
- The second option is an array of class names. For each class in this array, the command will execute on every object that has that class using a for loop.
- The third option is an array of class objects. The class objects have name and index fields. The index specifies the index in the class array to execute the command on. For example, index 2 calls the command on document.getElementsByClass('<name>')[2]. It's usually index 0 because cool web coders use classes of length 1 when they should use id's.

###Any combination (except none) of these three commands and three options is valid. They will need to be updated when sources change their websites.

###The embeddedArticleView class will load these commands, formulate the correct javascript, and execute it on the webpage after it loads. The js created will check if the DOM objects exist before trying to hide them.



