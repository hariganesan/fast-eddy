style
=======
- Keep the project as thin as possible. The easiest code to maintain is no code at all!
- Follow MVC closely. Views should not communicate with Models and vice versa. DO NOT attach model data to a view.
- All indent and bracing style should follow [K&R](https://en.wikipedia.org/wiki/Indent_style#K.26R_style).
- Do not randomly throw things in the Delegate -- make sure it is needed in many places first.
- Only import things that are needed. Only import base classes and Apple frameworks in header files; use @class for other things.
- Try to cut all files to < 500 lines. If necessary, use composition or inheritance to allow for better encapsulation.
- Favor composition over inheritance. Inheritance trees should be fairly flat.
- Be conservative about including third-party libraries and frameworks -- if it can be done in-house, do it, but don't reinvent the wheel. All libraries should be filed under a Support group within Xcode.
- If you can reduce asset sizes without damaging the user experience, creating complex code, or inflating memory, do it.
- Comment liberally. Every class's implementation file should have a complementary header file which should have a description of the class and descriptions for all of the functions within that class's interface. For reference, check out Localytics.h, apple's own [headerdoc user guide](https://developer.apple.com/library/mac/documentation/DeveloperTools/Conceptual/HeaderDoc/tags/tags.html), or [ray wenderlich's](http://www.raywenderlich.com/66395/documenting-in-xcode-with-headerdoc-tutorial).
- Do not include properties and methods in the interface of a class that are strictly internal to that class.
- Use enums everywhere. Avoid string comparison if possible.
- [Use BOOL, not bool](http://www.bignerdranch.com/blog/bools-sharp-corners/)
- Use typedefs when possible, such as CGFloat instead of float.
- Pay attention to calling super when writing functions inherited from a base class.
- All properties should be declared (nonatomic, strong) unless being accessed from multiple places at the same time.
- All objects that have parameters within the view (height, width) should use SCALEUP unless otherwise specified.
- Beware of getters, setters, and other word modifiers inherent to Obj C and UIKit; i.e. don't name a method "setBlah" or "newBlah".
- Use the underscore (_property) to allow for better understanding of class scope (using self.property implies an inherited property).
- Favor long, clear, and descriptive names over short and concise names for properties and methods (tableView:heightForRowAtIndexPath: > tv:rowHeightAtIP:)
- No warnings allowed except in rare instances within third-party libraries.
- Pay attention to read-only properties!

Version Control and Github
-------------------
- Include the hash of any relevant commits within the issue before closing.
- Include any useful links and screenshots to the issue whenever possible.
- Branch whenever possible. All branching should follow [this model](http://nvie.com/posts/a-successful-git-branching-model/) with the exception of release branches.

And finally...
---------
- DO NOT add or remove elements from a collection as you are iterating through it!!! Make a deep copy!!!
