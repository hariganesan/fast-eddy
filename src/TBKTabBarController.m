

#import "TBKTabBarController.h"
#import "TBKTabBar.h"
#import <QuartzCore/QuartzCore.h>
#import "UITabBarController+hidable.h"
#import "ArticleListViewController.h"
#import "BlurrableImageView.h"
#import "Delegate.h"
#import "Localytics.h"
#import "NavController.h"

@interface TBKTabBarController () <UINavigationControllerDelegate>
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, assign) CGFloat tabBarHeight;
@property (nonatomic, assign) BOOL navigationHidden;
@property (nonatomic) CGFloat animationVelocity;
@property (nonatomic) CGFloat velocityThreshold;
@property (nonatomic) NSTimeInterval drawerMinimumAnimationDuration;
-(void) loadViewControllers;

@end

#pragma mark -

@implementation TBKTabBarController

@synthesize delegate;
@synthesize tabBar;
@synthesize tabBarHeight;
@synthesize containerView;
@synthesize viewControllers;
@synthesize selectedViewController;
@synthesize selectedIndex;

- (id)init
{
    self = [super init];
    
    if (self) {
        self.tabBarHeight = NAVIGATION_BAR_HEIGHT;
        self.navigationHidden = NO;
        self.animationVelocity = 840.0f;
        self.drawerMinimumAnimationDuration = 0.15f;
        self.animatingDrawer = NO;
        _parallaxFactor = 1.5f;
        self.velocityThreshold = 500;
        self.initialSectionSelected = 0;
    }
    
    return self;
}

- (void)loadView
{
	[super loadView];
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	self.view.backgroundColor = [UIColor clearColor];
		
    self.containerView = [[UIView alloc] initWithFrame:self.view.frame];
    self.view = self.containerView;
    
    self.tabBar = [[TBKTabBar alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.bounds) - self.tabBarHeight*2, CGRectGetWidth(self.view.bounds), self.tabBarHeight)];
    self.tabBar.delegate = self;
    
    [self.containerView addSubview:self.tabBar];
    [self loadViewControllers];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}

- (void)setViewControllers:(NSArray *)controllers animated:(BOOL)animated
{
	self.viewControllers = controllers;
}

- (void)loadViewControllers
{
	NSMutableArray *controllerTabs = [NSMutableArray arrayWithCapacity:[self.viewControllers count]];
	NSUInteger tagIndex = 0;
	for (ArticleListViewController *controller in self.viewControllers) {
        if ([controller isKindOfClass:[ArticleListViewController class]]) {
            UIPanGestureRecognizer * pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureCallback:)];
            [pan setDelegate:(ArticleListViewController *)controller];
            [controller.view addGestureRecognizer:pan];
        }
        
        // accomodate tab bar in view
        controller.view.frame = CGRectMake(controller.view.frame.origin.x, controller.view.frame.origin.y, controller.view.frame.size.width, controller.view.frame.size.height - self.tabBarHeight);
        
		UIButton *tabItem = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width/[self.viewControllers count], self.tabBarHeight)];
        [tabItem setImage:controller.tabBarItem.image forState:UIControlStateNormal];
        [tabItem setImage:controller.tabBarItem.selectedImage forState:UIControlStateSelected];
		[controllerTabs addObject:tabItem];

		tagIndex++;
	}
    
	self.tabBar.items = controllerTabs;
}

#pragma mark - <TBKTabBarDelegate>

- (void)tabBar:(TBKTabBar *)aTabBar didSelectTabAtIndex:(NSUInteger)anIndex
{
	ArticleListViewController *vc = [self.viewControllers objectAtIndex:anIndex];
    [Delegate sharedDel].nc.currentViewController = vc;
    
	if (self.selectedViewController != vc) {
        [Localytics tagEvent:@"Section Selected" attributes:@{@"section": BM_ROUTES[vc.section]}];
		self.selectedViewController = vc;
        [self.containerView removeFromSuperview];
        self.containerView = self.selectedViewController.view;
        
        [self.view addSubview:self.containerView];
        [self.view sendSubviewToBack:self.containerView];
        [self.containerView setNeedsLayout];
		self.selectedIndex = anIndex;
	}
	
	if (self.delegate && [self.delegate respondsToSelector:@selector(tabBarController:didSelectViewController:)]) {
		[self.delegate tabBarController:self didSelectViewController:self.selectedViewController];
	}
}

- (void)showNavigation
{
    if (_navigationHidden) {
        _navigationHidden = NO;
        [self.tabBarController showNavigation];
        
        [UIView animateWithDuration:0.2 animations:^{
            tabBar.frame = CGRectMake(tabBar.frame.origin.x, tabBar.frame.origin.y-tabBar.frame.size.height, tabBar.frame.size.width, tabBar.frame.size.height);
        } completion:^(BOOL finished) {}];
    }
}

- (void)hideNavigation
{
    if (!_navigationHidden) {
        _navigationHidden = YES;
        [self.tabBarController hideNavigation];
        
        [UIView animateWithDuration:0.2 animations:^{
            tabBar.frame = CGRectMake(tabBar.frame.origin.x, tabBar.frame.origin.y+tabBar.frame.size.height, tabBar.frame.size.width, tabBar.frame.size.height);
        } completion:^(BOOL finished) {}];
    }
}

- (void)panGestureCallback:(UIPanGestureRecognizer *)panGesture{
    CGPoint translatedPoint = [panGesture translationInView:self.selectedViewController.view];

    if (panGesture.state == UIGestureRecognizerStateBegan) {
        if(self.animatingDrawer){
            [panGesture setEnabled:NO];
        } else {
            self.startingPanRect = self.selectedViewController.view.frame;
            self.startingImageRect = self.selectedViewController.bgImageView.frame;
            
            if ([self drawerOnSide:DrawerSideRight]) {
                [self.containerView addSubview:[self drawerOnSide:DrawerSideRight].view];
                [self drawerOnSide:DrawerSideRight].view.frame = CGRectMake(self.startingPanRect.origin.x+MAIN_WIDTH, self.startingPanRect.origin.y, MAIN_WIDTH, MAIN_HEIGHT-NAVIGATION_BAR_HEIGHT);
                [self drawerOnSide:DrawerSideRight].view.bounds = CGRectMake(0, 0, MAIN_WIDTH, MAIN_HEIGHT-NAVIGATION_BAR_HEIGHT);
                [[self drawerOnSide:DrawerSideRight].view setClipsToBounds:YES];
            }
            
            if ([self drawerOnSide:DrawerSideLeft]) {
                [self.containerView addSubview:[self drawerOnSide:DrawerSideLeft].view];
                [self drawerOnSide:DrawerSideLeft].view.frame = CGRectMake(self.startingPanRect.origin.x-MAIN_WIDTH, self.startingPanRect.origin.y, MAIN_WIDTH, MAIN_HEIGHT-NAVIGATION_BAR_HEIGHT);
                [self drawerOnSide:DrawerSideLeft].view.bounds = CGRectMake(0, 0, MAIN_WIDTH, MAIN_HEIGHT-NAVIGATION_BAR_HEIGHT);
                [[self drawerOnSide:DrawerSideLeft].view setClipsToBounds:YES];
            }
         }
    } else if (panGesture.state == UIGestureRecognizerStateChanged) {
        self.view.userInteractionEnabled = NO;
        CGRect newFrame = self.startingPanRect;
        newFrame.origin.x = [self roundedOriginXForDrawerConstriants:CGRectGetMinX(self.startingPanRect)+translatedPoint.x];
        newFrame = CGRectIntegral(newFrame);
        CGFloat xOffset = newFrame.origin.x;
        
        DrawerSide visibleSide = DrawerSideNone;
        CGFloat percentVisible = 0.0;
        if(xOffset > 0){
            if ([self drawerOnSide:DrawerSideLeft] == nil) {
                return;
            }
            
            visibleSide = DrawerSideLeft;
            percentVisible = xOffset/MAIN_WIDTH;
        } else if(xOffset < 0){
            if ([self drawerOnSide:DrawerSideRight] == nil) {
                return;
            }
            
            visibleSide = DrawerSideRight;
            percentVisible = ABS(xOffset)/MAIN_WIDTH;
        } else {
            return;
        }
        
        [self.selectedViewController.view setCenter:CGPointMake(CGRectGetMidX(newFrame), CGRectGetMidY(newFrame))];
        // parallax the background image of selected view
        [self.selectedViewController.bgImageView setCenter:CGPointMake(CGRectGetMidX(self.startingPanRect)-xOffset*(_parallaxFactor-1), self.selectedViewController.bgImageView.center.y)];
        // parallax the drawer from the started offset
        if (visibleSide == DrawerSideLeft) {
            [[self drawerOnSide:visibleSide].bgImageView setCenter:CGPointMake(CGRectGetMidX(self.startingPanRect)+(MAIN_WIDTH-xOffset)*(_parallaxFactor-1), self.selectedViewController.bgImageView.center.y)];
        } else if (visibleSide == DrawerSideRight) {
            [[self drawerOnSide:visibleSide].bgImageView setCenter:CGPointMake(CGRectGetMidX(self.startingPanRect)+(-MAIN_WIDTH-xOffset)*(_parallaxFactor-1), self.selectedViewController.bgImageView.center.y)];
        }
        
        [self.tabBar setArrowPositionWithOffset:xOffset];
        
        if (self.openSide != visibleSide) {
            [self setOpenSide:visibleSide];
        }
        
    } else if (panGesture.state == UIGestureRecognizerStateEnded || panGesture.state == UIGestureRecognizerStateCancelled) {
        self.startingPanRect = CGRectNull;
        CGPoint velocity = [panGesture velocityInView:self.selectedViewController.view];
        [self finishAnimationForPanGestureWithXVelocity:velocity.x completion:nil];
        self.view.userInteractionEnabled = YES;
    }
}

- (void)finishAnimationForPanGestureWithXVelocity:(CGFloat)xVelocity completion:(void(^)(BOOL finished))completion
{
    CGFloat currentOriginX = CGRectGetMinX(self.selectedViewController.view.frame);
    
    if(self.openSide == DrawerSideLeft) {
        CGFloat midPoint = MAIN_WIDTH / 2.0;

        if(currentOriginX > midPoint || xVelocity > self.velocityThreshold) {
            [self openDrawerSide:DrawerSideLeft animated:YES completion:completion];
        } else {
            [self closeDrawerAnimated:YES completion:completion];
        }
    } else if(self.openSide == DrawerSideRight){
        currentOriginX = CGRectGetMaxX(self.selectedViewController.view.frame);
        CGFloat midPoint = (MAIN_WIDTH / 2.0);

        if(currentOriginX < midPoint || xVelocity < -self.velocityThreshold) {
            [self openDrawerSide:DrawerSideRight animated:YES completion:completion];
        } else {
            [self closeDrawerAnimated:YES completion:completion];
        }
    } else {
        [self closeDrawerAnimated:YES completion:completion];
    }
}

- (CGFloat)roundedOriginXForDrawerConstriants:(CGFloat)originX {
    if (originX < -MAIN_WIDTH) {
        return -MAIN_WIDTH;
    } else if(originX > MAIN_WIDTH) {
        return MAIN_WIDTH;
    }
    
    return originX;
}

- (ArticleListViewController *)drawerOnSide:(DrawerSide)drawerSide
{
    if (drawerSide == DrawerSideRight && selectedIndex+1 < [self.viewControllers count]) {
        return [self.viewControllers objectAtIndex:selectedIndex+1];
    } else if (drawerSide == DrawerSideLeft && selectedIndex > 0) {
        return [self.viewControllers objectAtIndex:selectedIndex-1];
    } else if (drawerSide == DrawerSideNone) {
        return [self.viewControllers objectAtIndex:selectedIndex];
    }
    
    return nil;
}

- (void)openDrawerSide:(DrawerSide)drawerSide animated:(BOOL)animated completion:(void (^)(BOOL finished))completion
{
    CGFloat velocity = self.animationVelocity;

    NSParameterAssert(drawerSide != DrawerSideNone);
    if (self.animatingDrawer) {
        if(completion){
            completion(NO);
        }
    } else {
        [self setAnimatingDrawer:animated];
        
        if([self drawerOnSide:drawerSide]){
            CGRect newFrame;
            CGRect oldFrame = self.selectedViewController.view.frame;
            if(drawerSide == DrawerSideLeft){
                newFrame = self.selectedViewController.view.frame;
                newFrame.origin.x = MAIN_WIDTH;
            } else {
                newFrame = self.selectedViewController.view.frame;
                newFrame.origin.x = -MAIN_WIDTH;
            }
            
            CGFloat distance = ABS(CGRectGetMinX(oldFrame)-newFrame.origin.x);
            NSTimeInterval duration = MAX(distance/ABS(velocity),self.drawerMinimumAnimationDuration);
            
            [UIView
             animateWithDuration:(animated?duration:0.0)
             delay:0.0
             options:UIViewAnimationOptionCurveEaseInOut
             animations:^{
                 [self.selectedViewController.view setFrame:newFrame];
                 [[self drawerOnSide:DrawerSideLeft].bgImageView setFrame:self.startingImageRect];
                 [[self drawerOnSide:DrawerSideRight].bgImageView setFrame:self.startingImageRect];
             }
             completion:^(BOOL finished) {
                 // reset original selected controller
                 [self resetDrawerFrame:DrawerSideNone];
                 [self resetDrawerFrame:DrawerSideRight];
                 [self resetDrawerFrame:DrawerSideLeft];
                 
                 [[self drawerOnSide:DrawerSideRight].view removeFromSuperview];
                 [[self drawerOnSide:DrawerSideLeft].view removeFromSuperview];
                 
                 // reset bgimage
                 [self.selectedViewController.bgImageView setFrame:self.startingImageRect];

                 if (drawerSide == DrawerSideLeft) {
                     [self.tabBar didSelectTabBarItem:[self.tabBar.items objectAtIndex:selectedIndex-1]];
                 } else if (drawerSide == DrawerSideRight) {
                     [self.tabBar didSelectTabBarItem:[self.tabBar.items objectAtIndex:selectedIndex+1]];
                 }
                 
                 [self setOpenSide:DrawerSideNone];
                 [self setAnimatingDrawer:NO];
                 if(completion){
                     completion(finished);
                 }
             }];
        }
    }
}

- (void)resetDrawerFrame:(DrawerSide)drawerSide
{
    if (drawerSide == DrawerSideNone) {
        CGRect newFrame = self.selectedViewController.view.frame;
        newFrame.origin.x = 0;
        self.selectedViewController.view.frame = newFrame;
    } else {
        CGRect newFrame = [self drawerOnSide:drawerSide].view.frame;
        newFrame.origin.x = 0;
        [self drawerOnSide:drawerSide].view.frame = newFrame;
        [[self drawerOnSide:drawerSide].view setClipsToBounds:NO];
    }
}

- (void)closeDrawerAnimated:(BOOL)animated completion:(void (^)(BOOL finished))completion
{
    CGFloat velocity = self.animationVelocity;
    if(self.animatingDrawer){
        if(completion){
            completion(NO);
        }
    } else {
        [self setAnimatingDrawer:animated];
        CGRect newFrame = self.selectedViewController.view.bounds;
        
        CGFloat distance = ABS(CGRectGetMinX(self.selectedViewController.view.frame));
        NSTimeInterval duration = MAX(distance/ABS(velocity),self.drawerMinimumAnimationDuration);
        
        BOOL leftDrawerVisible = CGRectGetMinX(self.selectedViewController.view.frame) > 0;
        BOOL rightDrawerVisible = CGRectGetMinX(self.selectedViewController.view.frame) < 0;
        
        DrawerSide visibleSide = DrawerSideNone;
        CGFloat percentVisible = 0.0;
        
        if(leftDrawerVisible){
            CGFloat visibleDrawerPoints = CGRectGetMinX(self.selectedViewController.view.frame);
            percentVisible = MAX(0.0,visibleDrawerPoints/MAIN_WIDTH);
            visibleSide = DrawerSideLeft;
        } else if(rightDrawerVisible){
            CGFloat visibleDrawerPoints = CGRectGetWidth(self.selectedViewController.view.frame)-CGRectGetMaxX(self.selectedViewController.view.frame);
            percentVisible = MAX(0.0,visibleDrawerPoints/MAIN_WIDTH);
            visibleSide = DrawerSideRight;
        }

        [UIView
         animateWithDuration:(animated?duration:0.0)
         delay:0.0
         options:UIViewAnimationOptionCurveEaseInOut
         animations:^{
             [self.selectedViewController.view setFrame:newFrame];
             [self.selectedViewController.bgImageView setFrame:self.startingImageRect];
             [[self drawerOnSide:DrawerSideRight].bgImageView setFrame:self.startingImageRect];
             [[self drawerOnSide:DrawerSideLeft].bgImageView setFrame:self.startingImageRect];
             [self.tabBar setArrowPositionWithOffset:0];
         }
         completion:^(BOOL finished) {
             [self resetDrawerFrame:DrawerSideRight];
             [self resetDrawerFrame:DrawerSideLeft];

             [[self drawerOnSide:DrawerSideRight].view removeFromSuperview];
             [[self drawerOnSide:DrawerSideLeft].view removeFromSuperview];
             
             [self setOpenSide:DrawerSideNone];
             [self setAnimatingDrawer:NO];
             if(completion){
                 completion(finished);
             }
         }];
    }
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
