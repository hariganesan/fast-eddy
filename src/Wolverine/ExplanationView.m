//
//  ExplanationView.m
//  MOST
//
//  Created by Charlie Vrettos on 11/13/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import "ExplanationView.h"
#import "Localytics.h"

@implementation ExplanationView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];

    if (self) {
        self.backgroundColor = [Delegate sharedDel].cellAlternateTransparentColor;
        _hideButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_hideButton setFrame:self.frame];
        [_hideButton addTarget:self action:@selector(hideExplanation) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *vsSubtitle = nil;
        if (MAIN_HEIGHT == IPHONE_4_HEIGHT)
            vsSubtitle = [[UILabel alloc] initWithFrame:CGRectMake(70, 30, 180, 110)];
        else
            vsSubtitle = [[UILabel alloc] initWithFrame:CGRectMake(70*SCALEUP, 10*SCALEUP, 180*SCALEUP, 240*SCALEUP)];
        
        vsSubtitle.text = @"Articles are ranked using the BriefMe Score, which shows you how popular an article is right now";
        vsSubtitle.font = [UIFont fontWithName:@"BrandonGrotesque-Bold" size:16*SCALEUP];
        vsSubtitle.textAlignment = NSTextAlignmentCenter;
        vsSubtitle.numberOfLines = 6;
        vsSubtitle.textColor = [UIColor whiteColor];
        
        UIImageView *xout = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"x-out.png"]];
        [xout setFrame:CGRectMake(MAIN_WIDTH-50, 10, 40, 40)];
        
        [self addSubview:vsSubtitle];
        [self addSubview:_hideButton];
        [self addSubview:xout];
        self.hidden = YES;
    }
    return self;
}

- (void)hideExplanation
{
    self.hidden = YES;

    for (UIView *subview in self.subviews) {
        if ([subview isKindOfClass:[ScoreBannerView class]])
            [subview removeFromSuperview];
    }
    
    [_hideButton removeFromSuperview];
}

- (void)displayExplanation:(id)sender
{
    // article number is hidden in the button text
    int artNum = (int)((UIButton *)sender).tag;
    self.hidden = NO;
    
    //// scoreView (top banner + breakdown)
    ScoreBannerView *scoreView = nil;
    if (MAIN_HEIGHT == IPHONE_4_HEIGHT)
        scoreView = [[ScoreBannerView alloc] initWithFrame:CGRectMake(0, 140, MAIN_WIDTH, 280) andWithArticle:(NewsArticle *)[_arts objectAtIndex:artNum]];
    else
        scoreView = [[ScoreBannerView alloc] initWithFrame:CGRectMake(0, 200*SCALEUP, MAIN_WIDTH*SCALEUP, 280*SCALEUP) andWithArticle:(NewsArticle *)[_arts objectAtIndex:artNum]];
    
    scoreView.breakDownView.hidden = NO;
    scoreView.breakDownView.frame = CGRectMake(0, 100*SCALEUP, MAIN_WIDTH, scoreView.breakDownView.frame.size.height);
    scoreView.breakDownView.backgroundColor = [UIColor clearColor];
    //	scoreView.redBlock.hidden = YES;
    [self addSubview:scoreView];
    [self addSubview:_hideButton];
    
    // localytics: tag event
    NSMutableDictionary *attrs = [NSMutableDictionary dictionaryWithDictionary:[[_arts objectAtIndex:artNum] getAttributes]];
    [Localytics tagEvent:@"ExplanationView Viewed" attributes:attrs];
}

@end
