//
//  NewsArticleInnerCellView.m
//  BriefMe
//
//  Created by Charlie Vrettos on 6/22/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import "NewsArticleInnerCellView.h"
#import "UICircleView.h"

@implementation NewsArticleInnerCellView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // render cardinal number (tens and ones), article title, article source
 
        // 10px wider to fit number "10"
        _rank = [[UILabel alloc]initWithFrame:CGRectMake(30*SCALEUP, 20*SCALEUP, 50*SCALEUP, 45)];
        [_rank setFont: [UIFont fontWithName:@"BrandonGrotesque-Bold" size:45]];
        [_rank setBackgroundColor:[UIColor clearColor]];
        _rank.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_rank];
        
        // title height must also be changed in constrain function below!
        self.title = [[UILabel alloc]initWithFrame:CGRectMake(100*SCALEUP, 20*SCALEUP, 185*SCALEUP, 85*SCALEUP)];
        [self.title setFont: [UIFont fontWithName:@"BrandonGrotesque-Regular" size:16]];
        [self.title setBackgroundColor:[UIColor clearColor]];
        self.title.lineBreakMode = NSLineBreakByClipping;
        [self addSubview:self.title];
        
        // thin ring behind main circle
        _vcircleBackground = [[UICircleView alloc] initWithFrame:CGRectMake(35*SCALEUP, 79*SCALEUP, 40*SCALEUP, 40*SCALEUP) andColor:[UIColor clearColor]];
        _vcircleBackground.layer.borderWidth = 0.5f;
        _vcircleBackground.layer.cornerRadius = 20*SCALEUP;
        
        // Set up the shape of the circle
        float radius = 20*SCALEUP;
        
        // Make a circular shape
        _vcircle = [[UICircleView alloc] initWithFrame:_vcircleBackground.frame andColor:[UIColor clearColor]];
        _vcircle.circle.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 2.0*radius, 2.0*radius)
                                                          cornerRadius:radius].CGPath;
        // Center the shape in self.view
        _vcircle.circle.position = CGPointMake(_vcircle.frame.origin.x,_vcircle.frame.origin.y);
        
        // Configure the appearance of the circle
        _vcircle.circle.fillColor = [UIColor clearColor].CGColor;
        _vcircle.circle.borderColor = [UIColor clearColor].CGColor;
        _vcircle.circle.lineWidth = 2;
        
        _vcircle.glowCircle.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 2.0*radius, 2.0*radius)
                                                              cornerRadius:radius].CGPath;
        _vcircle.glowCircle.position = _vcircle.circle.position;
        _vcircle.glowCircle.fillColor = [UIColor clearColor].CGColor;
        _vcircle.glowCircle.borderColor = [UIColor clearColor].CGColor;
        
        _vcircle.glowCircle.lineWidth = 3.5;
        
        // Add to parent layer
        [self.layer addSublayer:_vcircleBackground.layer];
        [self.layer addSublayer:_vcircle.circle];
        [self.layer addSublayer:_vcircle.glowCircle];
        
        // virality
        _score = [[UILabel alloc]initWithFrame:CGRectMake(35*SCALEUP, 80*SCALEUP, 40*SCALEUP, 40*SCALEUP)];
        [_score setFont: [UIFont fontWithName:@"BrandonGrotesque-Bold" size:13]];
        [_score setBackgroundColor:[UIColor clearColor]];
        _score.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_score];
        
        self.source = [[UILabel alloc]initWithFrame:CGRectMake(100*SCALEUP, (120*SCALEUP-15), 185*SCALEUP, 15)];
        [self.source setFont: [UIFont fontWithName:@"BrandonGrotesque-Bold" size:10]];
        self.source.textColor = [UIColor grayColor];
        [self.source setBackgroundColor:[UIColor clearColor]];
        [self addSubview:self.source];
        
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

@end
