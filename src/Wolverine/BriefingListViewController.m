//
//  BriefingListViewController.m
//  BriefMe
//
//  Created by Hari Ganesan on 3/18/15.
//  Copyright (c) 2015 Campion Designs, LLC. All rights reserved.
//

#import "BriefingListViewController.h"
#import "Delegate.h"
#import "NavController.h"
#import "SettingsViewController.h"
#import "WinningTableViewController.h"
#import "Parser.h"
#import "Localytics.h"
#import "BedtimeView.h"
#import "TutorialView.h"

@implementation BriefingListViewController

- (id)initWithSection:(SectionType)section andTitle:(NSString *)title
{
    self = [super initWithSection:section andTitle:title];
    if (self) {
        if ([Delegate timeOfDay] == TIME_OF_DAY_EVENING) {
            // winning banner
            _banner = [[UIButton alloc] initWithFrame:CGRectMake(MAIN_WIDTH - 100, 50, 100, 25)];
            [_banner addTarget:self action:@selector(displayWinningView) forControlEvents:UIControlEventTouchUpInside];
            [self updateBanner];
//            [self.view insertSubview:_banner belowSubview:self.explanationView];
            
            UILabel *winners = [[UILabel alloc] initWithFrame:CGRectMake(18, 0, _banner.frame.size.width, _banner.frame.size.height)];
            NSAttributedString *winnerText = [[NSAttributedString alloc] initWithString:@"DAILY THREE" attributes:@{NSKernAttributeName: @1.8f}];
            [winners setAttributedText:winnerText];
            [winners setTextColor:[UIColor whiteColor]];
            [winners setFont:[UIFont fontWithName:@"BrandonGrotesque-Black" size:9]];
            [winners setTextAlignment:NSTextAlignmentLeft];
            
            [_banner addSubview:winners];
        }
        
        // time such that evening for the first time after a day, if user didn't switch to night mode, briefingview appears
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"hasShownBedtimeView"] && ![[NSUserDefaults standardUserDefaults] boolForKey:@"hasShownBedtimeView"] && [Delegate autoTheme] == AUTO_THEME_NIGHT && [Delegate sharedDel].settingsvc.control.selectedSegmentIndex != 1 && [[NSUserDefaults standardUserDefaults] objectForKey:@"launchCount"] && [[NSUserDefaults standardUserDefaults] integerForKey:@"launchCount"] > 1) {
            
            [[Delegate sharedDel].settingsvc.control setSelectedSegmentIndex:2]; //use enum?
            [[Delegate sharedDel].settingsvc toggleDisplay]; // initialLoad will be true so hasShownBedtimeView will be set to YES
            
            UIView *bedtimeView = [[BedtimeView alloc] initWithFrame:self.view.frame];
//            [self.view addSubview:bedtimeView];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hasShownBedtimeView"];
        }
        
        if ([Delegate sharedDel].firstLaunch) {
            _scrollTutorial = [[TutorialView alloc] initWithFrame:CGRectMake(50*SCALEUP, MAIN_HEIGHT-205*SCALEUP-50, MAIN_WIDTH-100*SCALEUP, 50) andType:TutorialTypeBottom];
            _scrollTutorial.tutorialLabel.text = @"Swipe up to see more articles";
            _scrollTutorial.hidden = YES;
//            [self.view addSubview:_scrollTutorial];
        } else {
            _scrollTutorial = nil;
        }
    }
    
    return self;
}

- (void)updateBanner {
    // set display on or off
    if ([Delegate sharedDel].wc) {
        _banner.hidden = NO;
    } else {
        _banner.hidden = YES;
    }
    
    // set theme and visited
    BOOL day = NO, visited = NO;
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"theme"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"theme"] intValue] == 0 || ([[[NSUserDefaults standardUserDefaults] objectForKey:@"theme"] intValue] == 2 && [Delegate autoTheme] != AUTO_THEME_NIGHT)) {
        day = YES;
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"visitedBanner"] && -[[[NSUserDefaults standardUserDefaults] objectForKey:@"visitedBanner"] timeIntervalSinceNow] < 60*60*12) {
        visited = YES;
    }
    
    if (visited && day) {
        [_banner setBackgroundImage:[UIImage imageNamed:@"winner-visited.png"] forState:UIControlStateNormal];
    } else if (visited) {
        [_banner setBackgroundImage:[UIImage imageNamed:@"winnernight-visited.png"] forState:UIControlStateNormal];
    } else if (day) {
        [_banner setBackgroundImage:[UIImage imageNamed:@"winner.png"] forState:UIControlStateNormal];
    } else {
        [_banner setBackgroundImage:[UIImage imageNamed:@"winnernight.png"] forState:UIControlStateNormal];
    }
}

- (void)refresh
{
    [super refresh];
    
    // Make winners banner appear if between 6pm and midnight
//    [[Delegate sharedDel].parser parseWinners];
}

- (void)displayWinningView
{
    if ([Delegate sharedDel].nc.topViewController == [Delegate sharedDel].wc) {
        return;
    }
    
    [Delegate sharedDel].nc.currentViewController = [Delegate sharedDel].wc;
    [[Delegate sharedDel].nc pushViewController:[Delegate sharedDel].wc animated:YES];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"visitedBanner"];
    [self updateBanner];
    
    // TODO(hari): differentiate in localytics between notification swipe enter and banner tap enter
    [Localytics tagEvent:@"Winners Opened" attributes:@{@"time":[NSString stringWithFormat:@"%ld", (long)[Delegate timeOfDay]]}];
}

- (void)scrollViewWillBeginDragging:(nonnull UIScrollView *)scrollView
{
    if (!_viewHasScrolled) {
        _viewHasScrolled = YES;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [super scrollViewDidScroll:scrollView];
    
    if (_scrollTutorial != nil && !_scrollTutorial.hidden) {
        _scrollTutorial.hidden = YES;
    }
    
    [self animateBannerWithOffset:scrollView.contentOffset.y];
}

- (void)animateBannerWithOffset:(float)contentOffset
{
    // banner move offscreen
    float speed = 0.6;
    if (contentOffset+100*SCALEUP <= 0)
        _banner.frame = CGRectMake(MAIN_WIDTH - _banner.frame.size.width, _banner.frame.origin.y, _banner.frame.size.width, _banner.frame.size.height);
    else if (contentOffset+100*SCALEUP > 0)
        _banner.frame = CGRectMake(MAIN_WIDTH - _banner.frame.size.width + (contentOffset+100*SCALEUP)*speed, _banner.frame.origin.y, _banner.frame.size.width, _banner.frame.size.height);
    
    // 50 is original local y coordinate of _banner
    float movedDown = -1*(contentOffset + 100*SCALEUP);
    if (movedDown > 0)
        _banner.frame = CGRectMake(_banner.frame.origin.x, 50 + movedDown, _banner.frame.size.width, _banner.frame.size.height);
    else
        _banner.frame = CGRectMake(_banner.frame.origin.x, 50, _banner.frame.size.width, _banner.frame.size.height);
}

@end
