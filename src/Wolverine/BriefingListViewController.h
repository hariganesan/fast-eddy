//
//  BriefingListViewController.h
//  BriefMe
//
//  Created by Hari Ganesan on 3/18/15.
//  Copyright (c) 2015 Campion Designs, LLC. All rights reserved.
//

#import "NewsArticleListViewController.h"

@interface BriefingListViewController : NewsArticleListViewController

/** @discussion The semi-fixed banner at the top right which
    moves away on scroll and opens a <code>WinningTableViewController</code>. */
@property (nonatomic, strong) UIButton *banner;
@property (nonatomic) BOOL viewHasScrolled;
@property (nonatomic, strong) TutorialView *scrollTutorial;

/** @brief Moves the banner offscreen. */
- (void)animateBannerWithOffset:(float)contentOffset;
- (void)updateBanner;
- (void)displayWinningView;

@end
