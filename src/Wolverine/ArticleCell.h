//
//  ArticleCell.h
//  MOST
//
//  Created by Hari Ganesan on 6/20/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Article;

/** @brief  Base cell class for all articles within an articlelistviewcontroller. */
@interface ArticleCell : UITableViewCell

// subviews
/** @brief The outer bubble of the cell. */
@property (nonatomic, strong) UIView *bubble;

/** @brief The news article's summary/description. */
@property (nonatomic, strong) UILabel *snippet;

// properties TODO(hari): remove these -- do not conform to MVC
/** @brief The number corresponding to the row in the table. */
@property (nonatomic) NSInteger row;
/** @brief True if the article cell has been visited. */
@property (nonatomic, assign) BOOL visited;

/** @brief Initialize an article cell. */
- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;
- (void)setVisited:(BOOL)visited animated:(BOOL)animated;

@end