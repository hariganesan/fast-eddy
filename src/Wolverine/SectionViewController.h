//
//  SectionViewController.h
//  MOST
//
//  Created by Hari Ganesan on 6/18/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @discussion Full screen view that shows navigation to all extra sections
    (politics...entertainment) in grid style */
@interface SectionViewController : UIViewController

@property (nonatomic, strong) UIViewController *sectChild;

/** @discussion Resets child to section view with big icons.
    (called on any main tab bar selection) */
- (void)resetView;

@end
