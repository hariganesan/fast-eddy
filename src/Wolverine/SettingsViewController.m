//
//  SettingsViewController.m
//  MOST
//
//  Created by Hari Ganesan on 6/19/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import "SettingsViewController.h"
#import "HeaderView.h"
#import "Delegate.h"
#import "SectionViewController.h"
#import "ArticleListViewController.h"
#import "TBKTabBarController.h"
#import "TBKTabBar.h"
#import "NavController.h"
#import "ODRefreshControl.h"
#import "Localytics.h"
#import "NewsArticleListViewController.h"
#import "BriefingListViewController.h"
#import "WinningTableViewController.h"
#import "Parser.h"
#import "InfoViewController.h"
#import "BottomNavigationBarView.h"
#import <Parse/Parse.h>


@implementation SettingsViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"S E T T I N G S";
        self.view.backgroundColor = [Delegate sharedDel].sectionBackgroundColor;
        self.tableView.backgroundColor = [Delegate sharedDel].sectionBackgroundColor;
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        self.tableView.bounces = NO;
        _tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAIN_WIDTH, 165*SCALEUP)];
        self.tableView.tableHeaderView = _tableHeaderView;
        self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAIN_WIDTH, NAVIGATION_BAR_HEIGHT)];
        
        HeaderView *hv = [[HeaderView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
        hv.title = self.title;
        [_tableHeaderView addSubview:hv];
        
        // display
        UILabel *displayLabel = [self addLabelWithText:@"Display" andFrame:CGRectMake(15*SCALEUP, 120*SCALEUP, 100*SCALEUP, 40*SCALEUP)];
        
        _control = [[UISegmentedControl alloc] initWithItems:@[@"DAY", @"NIGHT", @"AUTO"]];
        _control.frame = CGRectMake(126*SCALEUP, displayLabel.frame.origin.y+5*SCALEUP, 180*SCALEUP, 30*SCALEUP);
        _control.tintColor = [UIColor whiteColor];
        
        _autoExplanation = [self addLabelWithText:@"Night theme automatically switches on from 10pm-6am" andFrame:CGRectMake(15*SCALEUP, 170*SCALEUP, MAIN_WIDTH-30*SCALEUP, 60*SCALEUP)];
        _autoExplanation.numberOfLines = 2;
        _autoExplanation.font = [UIFont fontWithName:@"BrandonGrotesque-Bold" size:16];
        _autoExplanation.hidden = YES;
        
        // notifications
        UILabel *notificationLabel = [self addLabelWithText:@"Daily Three Notifications" andFrame:CGRectMake(15*SCALEUP, 70*SCALEUP, 240*SCALEUP, 40*SCALEUP)];
        _notificationSwitch = [[UISwitch alloc] init];
        _notificationSwitch.frame = CGRectMake(_control.frame.origin.x + _control.frame.size.width - _notificationSwitch.intrinsicContentSize.width, notificationLabel.frame.origin.y + notificationLabel.frame.size.height/2 - _notificationSwitch.intrinsicContentSize.height/2, _notificationSwitch.intrinsicContentSize.width, _notificationSwitch.intrinsicContentSize.height);
        _notificationSwitch.onTintColor = [UIColor whiteColor];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"winners"]) {
            _notificationSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"winners"];
        } else {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"winners"];
            _notificationSwitch.on = YES;
        }
        
        // if first launch, default to day theme so bedtimeview doesn't interfere w/tutorial
        // will be changed to auto on 2nd launch, then switched by bedtime view prompt)
        // will be set in nsuserdefaults in toggledisplay
        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"theme"]) {
            _control.selectedSegmentIndex = THEME_DAY;
            // toggleDisplay will set @"theme":THEME_DAY obj
        } else {
            _control.selectedSegmentIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"theme"];
        }
        // if user manually switched to night (even on an old version) or to auto (during 1st launch with new version), never display bedtimeview
        if (_control.selectedSegmentIndex == THEME_NIGHT || _control.selectedSegmentIndex == THEME_AUTO) {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hasShownBedtimeView"];
        }
                
        [[NSUserDefaults standardUserDefaults] setInteger:BRIEFME_VERSION_INT forKey:@"version"];
        
        _initialLoad = YES; // so that toggleDisplay doesn't send "light theme" to analytics on first load
        [self toggleDisplay];
        [_control addTarget:self action:@selector(toggleDisplay) forControlEvents:UIControlEventValueChanged];
        [_tableHeaderView addSubview:_control];
        
        PFInstallation *currentInstallation = [PFInstallation currentInstallation];
        
        if (![currentInstallation deviceToken] && [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]) {
            [currentInstallation setDeviceToken:[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]];
        }

        [_notificationSwitch addTarget:self action:@selector(toggleNotifications) forControlEvents:UIControlEventValueChanged];
        [_tableHeaderView addSubview:_notificationSwitch];
    }
    return self;
}

- (UILabel *)addLabelWithText:(NSString *)text andFrame:(CGRect)frame
{
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.text = text;
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:@"BrandonGrotesque-Regular" size:16];
    [_tableHeaderView addSubview:label];
    return label;
}

- (void)toggleDisplay
{
    [[NSUserDefaults standardUserDefaults] setInteger:_control.selectedSegmentIndex forKey:@"theme"];
    
    if (_control.selectedSegmentIndex == THEME_DAY || (_control.selectedSegmentIndex == THEME_AUTO && [Delegate autoTheme] != AUTO_THEME_NIGHT)) {
        // light theme
        _control.tintColor = [UIColor whiteColor];
        [Delegate sharedDel].cellBackgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.94];
        [Delegate sharedDel].cellBackgroundVisitedColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.7];
        [Delegate sharedDel].cellBackgroundHighlightColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.4];
        [Delegate sharedDel].cellTitleColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
        [Delegate sharedDel].cellNumberColor = [UIColor darkGrayColor];
        [Delegate sharedDel].cellHighlightColor = VISITED_COLOR;
        [Delegate sharedDel].sectionBackgroundColor = LIPSTICK_COLOR;
        [Delegate sharedDel].cellAlternateColor = LIPSTICK_COLOR;
        [Delegate sharedDel].cellAlternateTransparentColor = LIPSTICK_TRANSCOLOR;
        [Delegate sharedDel].tabsectvc.tabBar.backgroundColor = LIPSTICK_COLOR;
        [Delegate sharedDel].tabsectvc.tabBar.arrowLayer.fillColor = LIPSTICK_COLOR.CGColor;
        [Delegate sharedDel].articleBackgroundColor = [UIColor whiteColor];
        [Delegate sharedDel].articleForegroundColor = [UIColor blackColor];
        [Delegate sharedDel].articleScrollBarColor = LIPSTICK_COLOR;
        [Delegate sharedDel].articleScrollBarFinishColor = [UIColor blackColor];
        [Delegate sharedDel].articleAlternateColor = LIPSTICK_COLOR;
        [Delegate sharedDel].navigationButtonForegroundColor = [UIColor whiteColor];
        [Delegate sharedDel].navigationButtonBackgroundColor = LIPSTICK_COLOR;
        
        if (MAIN_WIDTH == IPHONE_6_WIDTH) {
            [((UITabBarController *)[[Delegate sharedDel].nc.viewControllers objectAtIndex:0]).tabBar setSelectionIndicatorImage:[UIImage imageNamed:@"tabbar-6.png"]];
        } else if (MAIN_WIDTH == IPHONE_6PLUS_WIDTH) {
            [((UITabBarController *)[[Delegate sharedDel].nc.viewControllers objectAtIndex:0]).tabBar setSelectionIndicatorImage:[UIImage imageNamed:@"tabbar-6p.png"]];
        } else { // == 320
            [((UITabBarController *)[[Delegate sharedDel].nc.viewControllers objectAtIndex:0]).tabBar setSelectionIndicatorImage:[UIImage imageNamed:@"tabbarselection.png"]];
        }
        
        [Delegate sharedDel].sectvc.sectChild.view.backgroundColor = LIPSTICK_COLOR;
        self.view.backgroundColor = LIPSTICK_COLOR;
        
        for (UITableViewCell* cell in [self.tableView visibleCells]) {
            cell.backgroundColor = LIPSTICK_COLOR;
            cell.textLabel.highlightedTextColor = LIPSTICK_COLOR;
        }
        
        if ([Delegate sharedDel].wc) {
            [[Delegate sharedDel].wc.explanationView setBackgroundColor:LIPSTICK_TRANSCOLOR];
            [Delegate sharedDel].wc.tableViewV2.backgroundColor = LIPSTICK_COLOR;
            [Delegate sharedDel].wc.bar.backgroundColor = LIPSTICK_COLOR;
        }
        
        for (UIViewController* vc in [Delegate sharedDel].nc.sectionViewControllers) {
            if ([vc isKindOfClass:[NewsArticleListViewController class]]) {
                [((NewsArticleListViewController *)vc).explanationView setBackgroundColor:LIPSTICK_TRANSCOLOR];
                
                if ([vc isKindOfClass:[BriefingListViewController class]]) {
                    [((BriefingListViewController *)vc) updateBanner];
                }
            }
        }
    } else if (_control.selectedSegmentIndex == THEME_NIGHT || _control.selectedSegmentIndex == THEME_AUTO) {
        // dark theme
        _control.tintColor = [UIColor blackColor];
        [Delegate sharedDel].cellBackgroundColor = [UIColor colorWithRed:30/255.0 green:30/255.0 blue:30/255.0 alpha:0.94];
        [Delegate sharedDel].cellBackgroundVisitedColor = [UIColor colorWithRed:30/255.0 green:30/255.0 blue:30/255.0 alpha:0.6];
        [Delegate sharedDel].cellBackgroundHighlightColor = [UIColor colorWithRed:30/255.0 green:30/255.0 blue:30/255.0 alpha:0.4];
        [Delegate sharedDel].cellTitleColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
        [Delegate sharedDel].cellNumberColor = [UIColor colorWithRed:206/255.0 green:206/255.0 blue:206/255.0 alpha:1];
        [Delegate sharedDel].cellHighlightColor = VISITED_COLOR_DARK;
        [Delegate sharedDel].sectionBackgroundColor = NIGHT_GOGGLES_DARK_COLOR;
        [Delegate sharedDel].cellAlternateColor = NIGHT_GOGGLES_COLOR;
        [Delegate sharedDel].cellAlternateTransparentColor = NIGHT_GOGGLES_TRANSCOLOR;
        [Delegate sharedDel].tabsectvc.tabBar.backgroundColor = NIGHT_GOGGLES_DARK_COLOR;
        [Delegate sharedDel].tabsectvc.tabBar.arrowLayer.fillColor = NIGHT_GOGGLES_DARK_COLOR.CGColor;
        [Delegate sharedDel].articleBackgroundColor = CHARCOAL_COLOR;
        [Delegate sharedDel].articleForegroundColor = [UIColor whiteColor];
        [Delegate sharedDel].articleAlternateColor = MUTED_LIME_COLOR;
        [Delegate sharedDel].articleScrollBarColor = MUTED_LIME_COLOR;
        [Delegate sharedDel].articleScrollBarFinishColor = NIGHT_GOGGLES_DARK_COLOR;
        [Delegate sharedDel].navigationButtonForegroundColor = MUTED_LIME_COLOR;
        [Delegate sharedDel].navigationButtonBackgroundColor = NIGHT_GOGGLES_DARK_COLOR;
        
        if (MAIN_WIDTH == IPHONE_6_WIDTH) {
            [((UITabBarController *)[[Delegate sharedDel].nc.viewControllers objectAtIndex:0]).tabBar setSelectionIndicatorImage:[UIImage imageNamed:@"tabbarnight-6.png"]];
        } else if (MAIN_WIDTH == IPHONE_6PLUS_WIDTH) {
            [((UITabBarController *)[[Delegate sharedDel].nc.viewControllers objectAtIndex:0]).tabBar setSelectionIndicatorImage:[UIImage imageNamed:@"tabbarnight-6p.png"]];
        } else { // == 320
            [((UITabBarController *)[[Delegate sharedDel].nc.viewControllers objectAtIndex:0]).tabBar setSelectionIndicatorImage:[UIImage imageNamed:@"tabbarselectionnight.png"]];
        }
        
        [Delegate sharedDel].sectvc.sectChild.view.backgroundColor = NIGHT_GOGGLES_DARK_COLOR;
        self.view.backgroundColor = NIGHT_GOGGLES_DARK_COLOR;
 
        for (UITableViewCell* cell in [self.tableView visibleCells]) {
            cell.backgroundColor = NIGHT_GOGGLES_DARK_COLOR;
            cell.textLabel.highlightedTextColor = NIGHT_GOGGLES_DARK_COLOR;
        }
        
        if ([Delegate sharedDel].wc) {
            [((WinningTableViewController *)[Delegate sharedDel].wc).explanationView setBackgroundColor:NIGHT_GOGGLES_TRANSCOLOR];
            [Delegate sharedDel].wc.tableViewV2.backgroundColor = NIGHT_GOGGLES_DARK_COLOR;
            [Delegate sharedDel].wc.bar.backgroundColor = NIGHT_GOGGLES_DARK_COLOR;
        }
        
        for (UIViewController* vc in [Delegate sharedDel].nc.sectionViewControllers) {
            if ([vc isKindOfClass:[NewsArticleListViewController class]]) {
                [((NewsArticleListViewController *)vc).explanationView setBackgroundColor:NIGHT_GOGGLES_TRANSCOLOR];
                
                if ([vc isKindOfClass:[BriefingListViewController class]]) {
                    [((BriefingListViewController *)vc) updateBanner];
                }
            }
        }
    }
    
    // theme was changed by user
    if (!_initialLoad) {
        NSString *theme;
        switch (_control.selectedSegmentIndex) {
            case THEME_DAY: theme = @"light"; break;
            case THEME_NIGHT: theme = @"dark"; break;
            default: theme = @"auto";
        }
        
        [Localytics tagEvent:@"Theme Changed" attributes:@{@"theme": theme}];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hasShownBedtimeView"];
    } else {
        _initialLoad = NO;
    }

    // show auto explanation
    CGFloat explanationPadding = 70*SCALEUP;
    if (_control.selectedSegmentIndex == THEME_AUTO && _autoExplanation.hidden) {
        _autoExplanation.hidden = NO;
        
        [UIView animateWithDuration:0.2 animations:^{
            _tableHeaderView.frame = CGRectMake(0, 0, MAIN_WIDTH, 165*SCALEUP+explanationPadding);
            self.tableView.tableHeaderView = _tableHeaderView;
        }];
        
    } else if (!_autoExplanation.hidden){
        _autoExplanation.hidden = YES;
        
        [UIView animateWithDuration:0.2 animations:^{
            _tableHeaderView.frame = CGRectMake(0, 0, MAIN_WIDTH, 165*SCALEUP);
            self.tableView.tableHeaderView = _tableHeaderView;
        }];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"resetCells" object:nil];
}

- (void)toggleNotifications
{    
    [[NSUserDefaults standardUserDefaults] setBool:_notificationSwitch.on forKey:@"winners"];
    
//     update channels within Parse
    [Delegate updateParseWithToggle:_notificationSwitch.on];
    
    if (_notificationSwitch.on) {
        [Delegate registerNotifications];
        [Localytics tagEvent:@"Winner Notifications Toggled" attributes:@{@"OnOff": @"On"}];
    } else {
        [Localytics tagEvent:@"Winner Notifications Toggled" attributes:@{@"OnOff": @"Off"}];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50*SCALEUP;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.textLabel.font = [UIFont fontWithName:@"BrandonGrotesque-Regular" size:16];
        cell.backgroundColor = [Delegate sharedDel].sectionBackgroundColor;
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        UIView *selectionColor = [[UIView alloc] init];
        selectionColor.backgroundColor = [UIColor whiteColor];
        [cell setSelectedBackgroundView:selectionColor];
    }
    
    cell.textLabel.highlightedTextColor = [Delegate sharedDel].sectionBackgroundColor;
    
    if (indexPath.row == SettingsRowPrivacyPolicy) {
        cell.textLabel.text = @"Privacy Policy & Terms of Service";
    } else if (indexPath.row == SettingsRowHelpAndFeedback) {
        cell.textLabel.text = @"Help and Feedback";
    } else if (indexPath.row == SettingsRowShareThisApp) {
        cell.textLabel.text = @"Share this app";
        cell.textLabel.font = [UIFont fontWithName:@"BrandonGrotesque-Bold" size:16];
    } else if (indexPath.row == SettingsRowFAQ) {
        cell.textLabel.text = @"FAQ";
    }
    
    return cell;
}

- (void)addContent:(NSArray *)content toView:(UIScrollView *)view
{
    CGFloat innerPadding = 10, outerPadding = 40;
    CGFloat y = 100;
    NSString *title, *body;
    for (NSArray *pair in content) {
        title = pair[0];
        body = pair[1];
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10*SCALEUP, y, MAIN_WIDTH-20*SCALEUP, 40)];
        titleLabel.font = [UIFont fontWithName:@"BrandonGrotesque-Bold" size:16];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.text = title;
        titleLabel.numberOfLines = 0;
        [titleLabel sizeToFit];
        [view addSubview:titleLabel];
        
        UILabel *bodyLabel = [[UILabel alloc] initWithFrame:CGRectMake(10*SCALEUP, y+titleLabel.frame.size.height+innerPadding, MAIN_WIDTH-20*SCALEUP, 100)];
        bodyLabel.font = [UIFont fontWithName:@"BrandonGrotesque-Regular" size:16];
        bodyLabel.textColor = [UIColor blackColor];
        bodyLabel.text = body;
        bodyLabel.numberOfLines = 0;
        [bodyLabel sizeToFit];
        [view addSubview:bodyLabel];
        
        y = y + titleLabel.frame.size.height + bodyLabel.frame.size.height + outerPadding;
    }
    
    view.contentSize = CGSizeMake(MAIN_WIDTH, y+NAVIGATION_BAR_HEIGHT);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == SettingsRowPrivacyPolicy) {
        InfoViewController *privacyController = [[InfoViewController alloc] init];
        [privacyController.hv setTitle:@"Privacy & Terms"];
        [self addContent:[NSJSONSerialization JSONObjectWithData:[[NSData alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"terms" ofType:@"json"]] options:NSJSONReadingMutableContainers error:nil] toView:privacyController.sv];
        [Delegate sharedDel].nc.currentViewController = privacyController;
        [[Delegate sharedDel].nc pushViewController:privacyController animated:YES];
        [Localytics tagEvent:@"Privacy & Terms Selected"];
    } else if (indexPath.row == SettingsRowHelpAndFeedback) {
        NSString *body = [NSString stringWithFormat:@"Enter your comment here: <br><br><br><br><br><br>iOS Version %@<br>BriefMe Version %@<br>Build %@<br>", IOS_VERSION, BRIEFME_VERSION, BUILD];
        [[Delegate sharedDel].nc shareOnEmail:self withSubject:@"Feedback on BriefMe" body:body andRecipients:@[@"support@getbriefme.com"]];
        [Localytics tagEvent:@"Help and Feedback Selected"];
    } else if (indexPath.row == SettingsRowShareThisApp) {
        UIActivityViewController *activityvc = [[UIActivityViewController alloc] initWithActivityItems:@[[NSString stringWithFormat:@"Cool news app I just found:\n%@", APP_URL_IOS]] applicationActivities:nil];
        [self presentViewController:activityvc animated:YES completion:nil];
        [Localytics tagEvent:@"Share This App Selected"];
    } else if (indexPath.row == SettingsRowFAQ) {
        InfoViewController *faqController = [[InfoViewController alloc] init];
        [faqController.hv setTitle:@"FAQ"];
        if ([Delegate sharedDel].faq) {
            [self addContent:[Delegate sharedDel].faq toView:faqController.sv];
        } else {
            [self addContent:[NSJSONSerialization JSONObjectWithData:[[NSData alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"faq" ofType:@"json"]] options:NSJSONReadingMutableContainers error:nil] toView:faqController.sv];
        }
        [Delegate sharedDel].nc.currentViewController = faqController;
        [[Delegate sharedDel].nc pushViewController:faqController animated:YES];
        [Localytics tagEvent:@"FAQ Selected"];
    }
    
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:YES];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
