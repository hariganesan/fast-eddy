//
//  StreamArticleInnerCellView.h
//  BriefMe
//
//  Created by Charlie Vrettos on 6/22/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleInnerCellView.h"

@interface StreamArticleInnerCellView : ArticleInnerCellView

/** @discussion the number unit that represents the age of the article given
 what is put in the <#timeAgo#> param. */
@property (nonatomic, strong) UILabel *age;
/** @brief text that includes the unit of time for <#age#>, such as "one hour ago". */
@property (nonatomic, strong) UILabel *timeAgo;
/** @brief Set and reduce size of title depending on text length. */

@end
