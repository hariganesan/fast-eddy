//
//  NavController.h
//  MOST
//
//  Created by Kat Zhou on 1/17/13.
//  Copyright (c) 2013 Campion Designs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@class Article, BottomNavigationBarView;

/** @discussion  Navigation controller that houses the tab bar at the 
    bottom of the screen with the back and share buttons.
    Includes share functionality and some app init stuff.
    If an article is shared, the model of that article should be attached
    to this controller before sharing. */
@interface NavController : UINavigationController <UITabBarControllerDelegate, UIActivityItemSource, MFMailComposeViewControllerDelegate, UIGestureRecognizerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) NSString *notificationName;

/** @discussion the black bar at the bottom of the screen that shows the 
    back button and share button. */
@property (nonatomic, strong) BottomNavigationBarView *bar;
@property (nonatomic, assign) BOOL barHidden;

@property (nonatomic, strong) NSArray *sectionViewControllers;

@property (nonatomic, strong) UIViewController *currentViewController;

/** @brief queue for blur functions. */
@property (atomic, strong) NSMutableArray *blurQueue;
/** @brief Set to YES if blur processing is taking place. */
@property (atomic) BOOL blurring;

- (void)createInitialController:(id)sender;
- (void)toggleArticleNavigation;

/* navigation */
/** @discussion The navigation bar shows when you scroll up a page.
    This function is called from the controller on top of the stack. */
- (void)showNavigation;
/** @discussion The navigation bar hides when you scroll down a page.
 This function is called from the controller on top of the stack. */
- (void)hideNavigation;

- (void)pushWebBrowser:(id)url;

/** @brief Share an article; manages _sharingArticle */
- (void)shareButtonPressedWithArticle:(Article *)art;
/** @brief Share a url; manages _sharingArticle */
- (void)shareButtonPressedWithURL:(NSURL *)url;

- (void)resetOrientation;
- (void)deviceOrientationDidChange;

/* sharing */
- (id)activityViewController:(UIActivityViewController *)activityViewController itemForActivityType:(NSString *)activityType;
- (id)activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController;
- (id)activityViewController:(UIActivityViewController *)activityViewController subjectForActivityType:(NSString *)activityType;

- (void)shareOnEmail:(id)sender withSubject:(NSString *)subject body:(NSString *)body andRecipients:(NSArray *)toRecipients;
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error;

- (void)addToBlurQueue:(NSNotification *)sender;

@end
