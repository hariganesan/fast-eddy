//
//  UICircleView.m
//  MOST
//
//  Created by Hari Ganesan on 2/20/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import "UICircleView.h"

@implementation UICircleView

- (id)initWithFrame:(CGRect)frame andColor:(UIColor *)color
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _circle = [CAShapeLayer layer];
        _circle.backgroundColor = [UIColor clearColor].CGColor;
        _glowCircle = [CAShapeLayer layer];
        _glowCircle.backgroundColor = [UIColor clearColor].CGColor;
        _color = color;
        self.backgroundColor = [UIColor clearColor];
        
//        self.layer.cornerRadius = 5;
//        self.layer.masksToBounds = YES;
    }
    
    return self;
}

- (void)animateCircleTo:(NSNumber *)score withDuration:(NSNumber *)duration
{
    float animFrom = 0;
    float animTo = [score floatValue] / 100;
    // don't animate at all; just display the circle
    if (duration == 0)
        animFrom = animTo;
    
    // Configure animation
    CABasicAnimation *drawAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    drawAnimation.duration            = [duration floatValue];
    drawAnimation.repeatCount         = 1.0;  // Animate only once..
    // Animate from no part of the stroke being drawn to the entire stroke being drawn
    drawAnimation.fromValue = [NSNumber numberWithFloat:animFrom];
    drawAnimation.toValue = [NSNumber numberWithFloat:animTo];
    // Experiment with timing to get the appearence to look the way you want
    drawAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    drawAnimation.fillMode = kCAFillModeForwards;
    drawAnimation.removedOnCompletion = NO;
    
    // stop slightly before the end of the circle so the glow edge isnt rough
    float glowFrom = 0;
    float glowTo = [score floatValue] / 100 - .002;
    if (glowTo < 0)
        glowTo = 0;
    // don't animate at all; just display the circle
    if (duration == 0)
        glowFrom = glowTo;
    
    CABasicAnimation *drawGlowAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    drawGlowAnimation.duration            = [duration floatValue];
    drawGlowAnimation.repeatCount         = 1.0;  // Animate only once..
    drawGlowAnimation.fromValue = [NSNumber numberWithFloat:glowFrom];
    drawGlowAnimation.toValue = [NSNumber numberWithFloat:glowTo];
    drawGlowAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    drawGlowAnimation.fillMode = kCAFillModeForwards;
    drawGlowAnimation.removedOnCompletion = NO;

    // Add the animation to the circle
    [self.circle addAnimation:drawAnimation forKey:@"StrokeEndAnimation"];
    [self.glowCircle addAnimation:drawGlowAnimation forKey:@"StrokeEndAnimation"];
    [self.circle setNeedsDisplay];
    [self.glowCircle setNeedsDisplay];
}

@end
