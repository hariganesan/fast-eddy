//
//  ArticleViewController.h
//  MOST
//
//  Created by Todd Lubin on 7/17/12.
//  Copyright (c) 2012 Campion Designs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "Delegate.h"

@class Article, ArticleHeaderView, BottomNavigationBarView, WebArticleView, BriefMeArticleView, ZoomableImageView;

/** @discussion Base class for all third-party content within a controller.
    The class is used and not subclassed for viewing articles in the stream */
// UIScrollViewDelegate for ZoomableImageView
@interface ArticleViewController : UIViewController<WKUIDelegate, WKScriptMessageHandler, WKNavigationDelegate, UIScrollViewDelegate>

/** @brief The article object whose data is displayed in the view. */
@property (nonatomic, strong) Article* article;
/** @brief A view that indicates the view is loading. */
@property (nonatomic, strong) UIActivityIndicatorView *aView;
/** @discussion The date when the view was first loaded.
    This property is used to calculate the total time a user
    spends on an article and then logged in analytics. */
@property (nonatomic, strong) NSDate *viewStart;
///** @discussion The header above the webview. This is either
//    a NewsArticleInnerCellView or a StreamArticleInnerCellView. */
//@property (nonatomic, strong) ArticleInnerCellView *articleHeader;


@property (nonatomic, strong) ArticleHeaderView *articleHeader;

/** @discussion Set to YES when the webarticleview has loaded according to
    WKNavigationDelegate. */
@property (nonatomic) BOOL webViewDidFinishNavigation;

/** @discussion Set to YES when a user has tapped the briefme view segment at least
    once in the article. */
@property (nonatomic) BOOL briefmeViewTapped;

/** @brief Set to YES when a new web browser controller is pushed on the stack. */
@property (nonatomic, assign) BOOL webBrowserIsOpen;

/** @brief The navigation bar with a button to swap article views */
@property (nonatomic, strong) BottomNavigationBarView *bar;
/** @brief Article view (WKWebView base) that loads source website */
@property (nonatomic, strong) WebArticleView *webArticleView;
/** @brief Article view that uses BriefMe View content */
@property (nonatomic, strong) BriefMeArticleView *briefMeArticleView;
/** @brief Set to the current visible view, briefmeview or webview */
@property (nonatomic) ArticleViewType visibleView;
/** @brief Whether a view swap animation is currently happening */
@property (nonatomic) BOOL swapping;
/** @brief A view with an image that allows pinch-to-zoom */
@property (nonatomic, strong) ZoomableImageView *zoomableImageView;

/** @brief Initializes the controller. */
- (id)initWithArticle:(Article *)art;

@end
