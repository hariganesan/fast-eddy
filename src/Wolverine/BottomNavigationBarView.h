//
//  BottomNavigationBarView.h
//  BriefMe
//
//  Created by Charlie Vrettos on 6/24/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#define DISABLED_BUTTON_ALPHA 0.4f

@interface BottomNavigationBarView : UIView

/** @brief Takes you back to the list view */
@property (nonatomic, strong) UIImageView *backButtonImageView;
@property (nonatomic, strong) UIButton *shareButton;
@property (nonatomic, strong) UIImageView *shareButtonImageView;
/** @brief Image in center of navbar */
@property (nonatomic, strong) UIImageView *briefMeImageView;
/** @brief Switches between Web view and BriefMe view. */
@property (nonatomic, strong) UISegmentedControl *control;
/** @discussion An invisible button over the control. Used so that
    tutorial (where the button flashes) allows interaction. */
@property (nonatomic, strong) UIButton *controlButtonLeft;
/** @discussion An invisible button over the control. Used so that
    tutorial (where the button flashes) allows interaction. */
@property (nonatomic, strong) UIButton *controlButtonRight;

/** @discussion Label that appears in place of segmented control
    when there's no briefme view available. */
@property (nonatomic, strong) UILabel *noBriefMeViewLabel;

/** @brief Image for navigation within a WKWebview. */
@property (nonatomic, strong) UIImageView *backTriangleImageView;
/** @brief Image for navigation within a WKWebview. */
@property (nonatomic, strong) UIImageView *forwardTriangleImageView;
/** @brief Image for navigation within a WKWebview. */
@property (nonatomic, strong) UIImageView *refreshImageView;

/** @brief An invisible button for navigation within a WKWebview. */
@property (nonatomic, strong) UIButton *webBackButton;
/** @brief An invisible button for navigation within a WKWebview. */
@property (nonatomic, strong) UIButton *webForwardButton;
/** @brief An invisible button for navigation within a WKWebview. */
@property (nonatomic, strong) UIButton *refreshButton;

@property (nonatomic) BOOL animating;

typedef enum {
    BottomNavigationBarTypeLogo,
    BottomNavigationBarTypeControl,
    BottomNavigationBarTypeWebBrowser
} BottomNavigationBarType;

/** @discussion Initiate a navigation bar on the bottom of the screen with a
    specified `BottomNavigationBarType`. This affects the style of the navigation
    bar and the controller should dictate the events for all actionable views. */
- (instancetype)initWithType:(BottomNavigationBarType)bottomNavigationBarType;

- (void)show;
- (void)hide;

@end