//
//  ArticleHeaderView.h
//  BriefMe
//
//  Created by Charlie Vrettos on 6/30/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import "ArticleInnerCellView.h"

@interface ArticleHeaderView : ArticleInnerCellView

/** @brief The score or x mins ago */
@property (nonatomic, strong) UILabel *leftAttribute;

-(void)resizeHeightToFitSubviews;

@end
