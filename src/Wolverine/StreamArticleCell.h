//
//  StreamArticleCell.h
//  MOST
//
//  Created by Hari Ganesan on 6/20/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleCell.h"
#import "Delegate.h"

@class StreamArticleInnerCellView;

/** @brief  View for a cell within the stream section */
@interface StreamArticleCell : ArticleCell

/** @brief A view containing title, score, source. */
@property (nonatomic, strong) StreamArticleInnerCellView *streamArticleInnerCellView;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;

@end
