//
//  StreamArticleCell.m
//  MOST
//
//  Created by Hari Ganesan on 6/20/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import "Article.h"
#import "StreamArticleCell.h"
#import "Delegate.h"
#import "StreamArticleInnerCellView.h"

@implementation StreamArticleCell

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:(NSString *)reuseIdentifier];
    if (self) {
        _streamArticleInnerCellView = [[StreamArticleInnerCellView alloc] initWithFrame:self.frame];
        [self addSubview:_streamArticleInnerCellView];
    }
    return self;
}

// touch followed by a drag gesture (not selected)
- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    
    if (highlighted) {
        [_streamArticleInnerCellView.title setTextColor:[Delegate sharedDel].cellHighlightColor];
        [_streamArticleInnerCellView.age setTextColor:[Delegate sharedDel].cellHighlightColor];
        [_streamArticleInnerCellView.timeAgo setTextColor:[Delegate sharedDel].cellHighlightColor];
    } else {
        [self setVisited:self.visited animated:NO];
        [_streamArticleInnerCellView.title setTextColor:[Delegate sharedDel].cellTitleColor];
        [_streamArticleInnerCellView.age setTextColor:[Delegate sharedDel].cellNumberColor];
        [_streamArticleInnerCellView.timeAgo setTextColor:[Delegate sharedDel].cellNumberColor];
    }
}

@end
