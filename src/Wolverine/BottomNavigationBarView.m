//
//  BottomNavigationBarView.m
//  BriefMe
//
//  Created by Charlie Vrettos on 6/24/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import "BottomNavigationBarView.h"
#import "Delegate.h"
#import "NavController.h"

@implementation BottomNavigationBarView

- (instancetype)initWithType:(BottomNavigationBarType)bottomNavigationBarType
{
    CGRect frame = CGRectMake(0, MAIN_HEIGHT-NAVIGATION_BAR_HEIGHT, MAIN_WIDTH, NAVIGATION_BAR_HEIGHT);
    self = [super initWithFrame:frame];
    if (self) {
        _animating = NO;
        
        CGFloat backX = 26*SCALEUP;
        CGFloat imageYOffset = 1.0f;
        
        // back button image
        UIImage *backButtonImage = [[UIImage imageNamed:@"back.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        _backButtonImageView = [[UIImageView alloc] initWithImage:backButtonImage];
        _backButtonImageView.frame = CGRectMake(backX-backButtonImage.size.width/4, NAVIGATION_BAR_HEIGHT/2-backButtonImage.size.height/4+imageYOffset, backButtonImage.size.width/2, backButtonImage.size.height/2);
        [self addSubview:_backButtonImageView];
        [_backButtonImageView setTintColor:[Delegate sharedDel].navigationButtonForegroundColor];
        
        // clear back button above image
        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, backButtonImage.size.width/2+12*SCALEUP, NAVIGATION_BAR_HEIGHT)];
        [backButton addTarget:self action:@selector(goBackWithTap) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:backButton];
        
        // share button image
        UIImage *shareButtonImage = [[UIImage imageNamed:@"share.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        _shareButtonImageView = [[UIImageView alloc] initWithImage:shareButtonImage];
        _shareButtonImageView.frame = CGRectMake(MAIN_WIDTH-backX-shareButtonImage.size.width/4, NAVIGATION_BAR_HEIGHT/2-shareButtonImage.size.height/4+imageYOffset, shareButtonImage.size.width/2, shareButtonImage.size.height/2);
        [self addSubview:_shareButtonImageView];
        [_shareButtonImageView setTintColor:[Delegate sharedDel].navigationButtonForegroundColor];
        
        // clear share button above image
        _shareButton = [[UIButton alloc] initWithFrame:CGRectMake(MAIN_WIDTH-backButtonImage.size.width/2-12*SCALEUP, 0, backButtonImage.size.width/2+12*SCALEUP, NAVIGATION_BAR_HEIGHT)];
        [self addSubview:_shareButton];
        // parent navigationController must set shareButton target and handle it
        
        if (bottomNavigationBarType == BottomNavigationBarTypeLogo) {
            // briefme name
            UIImage *briefMeImage = [UIImage imageNamed:@"briefme.png"];
            _briefMeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(MAIN_WIDTH/2-briefMeImage.size.width/4, NAVIGATION_BAR_HEIGHT/2-briefMeImage.size.height/4, briefMeImage.size.width/2, briefMeImage.size.height/2)];
            [_briefMeImageView setImage:briefMeImage];
            [self addSubview:_briefMeImageView];
        } else if (bottomNavigationBarType == BottomNavigationBarTypeControl) {
            // control switches between mobile webview and briefme view
            _control = [[UISegmentedControl alloc] initWithItems:@[@"WEB", @"BRIEFME VIEW"]];
            CGSize controlSize = CGSizeMake(190*SCALEUP, 30);
            _control.frame = CGRectMake(self.frame.size.width/2-controlSize.width/2, self.frame.size.height/2-controlSize.height/2, controlSize.width, controlSize.height);
            _control.tintColor = [Delegate sharedDel].navigationButtonForegroundColor;
            _control.selectedSegmentIndex = 0;
            [self addSubview:_control];
            
            // make two invisible buttons over control so that we can interact during tutorial
            // check flashMainButton in ArticleViewController
            CGSize buttonSize = CGSizeMake(controlSize.width/2, NAVIGATION_BAR_HEIGHT);
            _controlButtonLeft = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width/2-buttonSize.width, self.frame.size.height/2-buttonSize.height/2, buttonSize.width, buttonSize.height)];
            _controlButtonRight = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width/2, self.frame.size.height/2-buttonSize.height/2, buttonSize.width, buttonSize.height)];
            _controlButtonLeft.tag = 0;
            _controlButtonRight.tag = 1;
            [_controlButtonLeft addTarget:self action:@selector(toggleSegmentedControl:) forControlEvents:UIControlEventTouchUpInside];
            [_controlButtonRight addTarget:self action:@selector(toggleSegmentedControl:) forControlEvents:UIControlEventTouchUpInside];
            
            [self addSubview:_controlButtonLeft];
            [self addSubview:_controlButtonRight];
            
            if (MAIN_WIDTH <= IPHONE_5_WIDTH) {
                [_control setTitleTextAttributes:@{NSKernAttributeName: @1.4f, NSFontAttributeName: [UIFont fontWithName:@"BrandonGrotesque-Bold" size:9]} forState:UIControlStateNormal];
            } else {
                [_control setTitleTextAttributes:@{NSKernAttributeName: @1.4f, NSFontAttributeName: [UIFont fontWithName:@"BrandonGrotesque-Bold" size:10]} forState:UIControlStateNormal];
            }
            
            // label for when no article content exists
            _noBriefMeViewLabel = [[UILabel alloc] init];
            _noBriefMeViewLabel.text = @"NO BRIEFME VIEW";
            _noBriefMeViewLabel.font = [UIFont fontWithName:@"BrandonGrotesque-Bold" size:14.0f];
            [_noBriefMeViewLabel sizeToFit];
            _noBriefMeViewLabel.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2+1);
            _noBriefMeViewLabel.textColor = [UIColor whiteColor];
            _noBriefMeViewLabel.alpha = DISABLED_BUTTON_ALPHA;
            _noBriefMeViewLabel.hidden = YES;
            [self addSubview:_noBriefMeViewLabel];
        } else if (bottomNavigationBarType == BottomNavigationBarTypeWebBrowser) {
            // all X values are for the center of the images.
            CGFloat backTriangleX = 90*SCALEUP;
            CGFloat forwardTriangleX = 160*SCALEUP;
            CGFloat refreshX = 230*SCALEUP;
            
            // back button image
            UIImage *backTriangleImage = [[UIImage imageNamed:@"webback.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            _backTriangleImageView = [[UIImageView alloc] initWithImage:backTriangleImage];
            _backTriangleImageView.frame = CGRectMake(backTriangleX-backTriangleImage.size.width/4, NAVIGATION_BAR_HEIGHT/2-backTriangleImage.size.height/4+imageYOffset, backTriangleImage.size.width/2, backTriangleImage.size.height/2);
            _backTriangleImageView.alpha = DISABLED_BUTTON_ALPHA;
            _backTriangleImageView.tintColor = [Delegate sharedDel].navigationButtonForegroundColor;
            [self addSubview:_backTriangleImageView];
            
            // invis button over back button img
            _webBackButton = [[UIButton alloc] initWithFrame:CGRectMake(backTriangleX-backTriangleImage.size.width/4, 0, backTriangleImage.size.width/2, NAVIGATION_BAR_HEIGHT)];
            _webBackButton.backgroundColor = [UIColor clearColor];
            [self addSubview:_webBackButton];
            
            // forward button image
            UIImage *forwardTriangleImage = [[UIImage imageNamed:@"webforward.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            _forwardTriangleImageView = [[UIImageView alloc] initWithImage:forwardTriangleImage];
            _forwardTriangleImageView.frame = CGRectMake(forwardTriangleX-forwardTriangleImage.size.width/4, NAVIGATION_BAR_HEIGHT/2-forwardTriangleImage.size.height/4+imageYOffset, forwardTriangleImage.size.width/2, forwardTriangleImage.size.height/2);
            _forwardTriangleImageView.alpha = DISABLED_BUTTON_ALPHA;
            _forwardTriangleImageView.tintColor = [Delegate sharedDel].navigationButtonForegroundColor;
            [self addSubview:_forwardTriangleImageView];
            
            // invis button over forward button img
            _webForwardButton = [[UIButton alloc] initWithFrame:CGRectMake(forwardTriangleX-forwardTriangleImage.size.width/4, 0, forwardTriangleImage.size.width/2, NAVIGATION_BAR_HEIGHT)];
            _webForwardButton.backgroundColor = [UIColor clearColor];
            [self addSubview:_webForwardButton];
            
            // refresh button image
            UIImage *refreshImage = [[UIImage imageNamed:@"refresh.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            _refreshImageView = [[UIImageView alloc] initWithImage:refreshImage];
            _refreshImageView.frame = CGRectMake(refreshX-refreshImage.size.width/4, NAVIGATION_BAR_HEIGHT/2-refreshImage.size.height/4+imageYOffset, refreshImage.size.width/2, refreshImage.size.height/2);
            _refreshImageView.tintColor = [Delegate sharedDel].navigationButtonForegroundColor;
            [self addSubview:_refreshImageView];
            
            // invis button over refresh button img
            _refreshButton = [[UIButton alloc] initWithFrame:CGRectMake(refreshX-refreshImage.size.width/4, 0, refreshImage.size.width/2, NAVIGATION_BAR_HEIGHT)];
            _refreshButton.backgroundColor = [UIColor clearColor];
            [self addSubview:_refreshButton];
        }
        
    }
    return self;
}

- (void)goBackWithTap
{
    [[Delegate sharedDel].nc popViewControllerAnimated:YES];
}

- (void)toggleSegmentedControl:(UIButton *)sender
{
    if (_control.selectedSegmentIndex != sender.tag) {
        [_control setSelectedSegmentIndex:sender.tag];
        [_control sendActionsForControlEvents:UIControlEventValueChanged];
    }
}

- (void)show
{
    if (_animating) {
        return;
    }
    _animating = YES;
    [UIView animateWithDuration:0.2f animations:^{
        self.frame = CGRectMake(self.frame.origin.x, MAIN_HEIGHT-NAVIGATION_BAR_HEIGHT, self.frame.size.width, self.frame.size.height);
    } completion:^(BOOL finished) {
        _animating = NO;
    }];
}

- (void)hide
{
    if (_animating) {
        return;
    }
    _animating = YES;
    [UIView animateWithDuration:0.2f animations:^{
        self.frame = CGRectMake(self.frame.origin.x, MAIN_HEIGHT, self.frame.size.width, self.frame.size.height);
    } completion:^(BOOL finished) {
        _animating = NO;
    }];
}

@end
