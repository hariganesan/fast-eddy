//
//  InfoViewController.h
//  BriefMe
//
//  Created by Charlie Vrettos on 3/30/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HeaderView, BottomNavigationBarView;

/** @brief Used to turn off landscape mode for FAQ and PRIVACY views */
@interface InfoViewController : UIViewController<UIScrollViewDelegate>

/** @brief The scroll view which has all the content. */
@property UIScrollView *sv;
/** @brief title and white line at top of section. */
@property HeaderView *hv;
/** @brief The navigation bar at the bottom. */
@property BottomNavigationBarView *bar;

@property (nonatomic, assign) CGFloat lastContentOffset;

@end
