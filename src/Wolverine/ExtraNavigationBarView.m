//
//  WebNavigationBarView.m
//  BriefMe
//
//  Created by Charlie Vrettos on 7/23/15.
//  Copyright © 2015 BriefMe Media, LLC. All rights reserved.
//

#import "ExtraNavigationBarView.h"
#import "Delegate.h"

@interface ExtraNavigationBarView ()
/** @discussion Whether or not animateWithDuration is currently
    occuring to move the view up and down */
@property (nonatomic) float animating;
/** @brief Frame of the view when it is above the navigation bar. */
@property (nonatomic) CGRect upFrame;
/** @brief Frame of the view when the navigation bar is hidden */
@property (nonatomic) CGRect downFrame;

@end


@implementation ExtraNavigationBarView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _upFrame = frame;
        _downFrame = CGRectMake(frame.origin.x, frame.origin.y+frame.size.height, frame.size.width, frame.size.height);
        _visible = NO;
        self.alpha = 0.0f;
        
        CGFloat backX = 26*SCALEUP;
        CGFloat forwardX = 86*SCALEUP;
        CGFloat imageYOffset = 1.0f;
        
        UIView *thinBlackLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 0.5f)];
        thinBlackLine.backgroundColor = [UIColor blackColor];
        thinBlackLine.alpha = 0.5f;
        [self addSubview:thinBlackLine];
        
        // back button image
        UIImage *backTriangleImage = [[UIImage imageNamed:@"webback.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        _backTriangleImageView = [[UIImageView alloc] initWithImage:backTriangleImage];
        _backTriangleImageView.frame = CGRectMake(backX-backTriangleImage.size.width/4, NAVIGATION_BAR_HEIGHT/2-backTriangleImage.size.height/4+imageYOffset, backTriangleImage.size.width/2, backTriangleImage.size.height/2);
        _backTriangleImageView.tintColor = [Delegate sharedDel].navigationButtonBackgroundColor;
        [self addSubview:_backTriangleImageView];
        
        // forward button image
        UIImage *forwardTriangleImage = [[UIImage imageNamed:@"webforward.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        _forwardTriangleImageView = [[UIImageView alloc] initWithImage:forwardTriangleImage];
        _forwardTriangleImageView.frame = CGRectMake(forwardX-forwardTriangleImage.size.width/4, NAVIGATION_BAR_HEIGHT/2-forwardTriangleImage.size.height/4+imageYOffset, forwardTriangleImage.size.width/2, forwardTriangleImage.size.height/2);
        _forwardTriangleImageView.tintColor = [Delegate sharedDel].navigationButtonBackgroundColor;
        [self addSubview:_forwardTriangleImageView];
        
        // refresh button image
        UIImage *refreshButtonImage = [[UIImage imageNamed:@"refresh.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        UIImageView *refreshButtonImageView = [[UIImageView alloc] initWithImage:refreshButtonImage];
        refreshButtonImageView.frame = CGRectMake(MAIN_WIDTH-backX-refreshButtonImage.size.width/4,NAVIGATION_BAR_HEIGHT/2-refreshButtonImage.size.height/4+imageYOffset, refreshButtonImage.size.width/2, refreshButtonImage.size.height/2);
        refreshButtonImageView.tintColor = [Delegate sharedDel].navigationButtonBackgroundColor;
        [self addSubview:refreshButtonImageView];
        
        CGFloat invisibleButtonWidth = 60*SCALEUP;
        
//        // invis button over back button img
        _backButton = [[UIButton alloc] initWithFrame:CGRectMake(backX-invisibleButtonWidth/2, 0, invisibleButtonWidth, NAVIGATION_BAR_HEIGHT)];
        _backButton.backgroundColor = [UIColor clearColor];
        [self addSubview:_backButton];

//        // invis button over forward button img
        _forwardButton = [[UIButton alloc] initWithFrame:CGRectMake(forwardX-invisibleButtonWidth/2, 0, invisibleButtonWidth, NAVIGATION_BAR_HEIGHT)];
        _forwardButton.backgroundColor = [UIColor clearColor];
        [self addSubview:_forwardButton];

        _refreshButton = [[UIButton alloc] initWithFrame:CGRectMake(MAIN_WIDTH-backX-invisibleButtonWidth/2, 0, invisibleButtonWidth, NAVIGATION_BAR_HEIGHT)];
        _refreshButton.backgroundColor = [UIColor clearColor];
        [self addSubview:_refreshButton];

        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)moveUp
{
    if (_animating) {
        return;
    }
    _animating = YES;
    [UIView animateWithDuration:0.2f animations:^{
        self.frame = _upFrame;
    } completion:^(BOOL finished) {
        _animating = NO;
    }];
}

- (void)moveDown
{
    if (_animating) {
        return;
    }
    _animating = YES;
    [UIView animateWithDuration:0.2f animations:^{
        self.frame = _downFrame;
    } completion:^(BOOL finished) {
            _animating = NO;
    }];
}

- (void)animateAppear
{
    if (_visible)
        return;
    _visible = YES;
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 1.0;
    }];
}

@end
