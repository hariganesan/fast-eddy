//
//  TutorialView.m
//  MOST
//
//  Created by Charlie Vrettos on 7/28/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import "TutorialView.h"
#import "Delegate.h"

@implementation TutorialView

- (id)initWithFrame:(CGRect)frame andType:(TutorialType)tutType
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [Delegate sharedDel].cellAlternateColor;
        self.layer.cornerRadius = 10;
        self.tutType = tutType;
        
        _tutorialLabel = [[UILabel alloc] initWithFrame:CGRectMake(12*SCALEUP, 0, self.frame.size.width - 2*12*SCALEUP, self.frame.size.height)];
        _tutorialLabel.textColor = [UIColor whiteColor];
        _tutorialLabel.textAlignment = NSTextAlignmentCenter;
        _tutorialLabel.font = [UIFont fontWithName:@"BrandonGrotesque-Medium" size:16];
        [self addSubview:_tutorialLabel];
        
        if (tutType == TutorialTypeTop) {
            _triangle = [[UIView alloc] initWithFrame:CGRectMake((self.frame.size.width/2 - 40*SCALEUP/2), -20*SCALEUP, 40*SCALEUP, 22*SCALEUP)];
        } else if (tutType == TutorialTypeLeft) {
            _triangle = [[UIView alloc] initWithFrame:CGRectMake(-22*SCALEUP, self.frame.size.height/2-40*SCALEUP/2+30*SCALEUP, 40*SCALEUP, 22*SCALEUP)];
            _triangle.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(-90));
        } else if (tutType == TutorialTypeBottom) {
            _triangle = [[UIView alloc] initWithFrame:CGRectMake((self.frame.size.width/2 - 40*SCALEUP/2), self.frame.size.height-5*SCALEUP, 40*SCALEUP, 22*SCALEUP)];
            _triangle.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(-180));
        } else if (tutType == TutorialTypeRight) {
            _triangle = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width-18*SCALEUP, self.frame.size.height/2-22*SCALEUP/2, 40*SCALEUP, 22*SCALEUP)];
            _triangle.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
        }
        
        _triangle.backgroundColor = [UIColor clearColor];

        _triangleShape = [[CAShapeLayer alloc]initWithLayer:self];
        _triangleShape.bounds = CGRectMake(0, 0, 40*SCALEUP, 22*SCALEUP);
        _triangleShape.anchorPoint = CGPointMake(0,0);

        UIBezierPath *bottomTrianglePath = [UIBezierPath bezierPath];
        [bottomTrianglePath moveToPoint:CGPointMake(CGRectGetMinX(_triangleShape.frame), CGRectGetMaxY(_triangleShape.frame) - 1.5)];
        [bottomTrianglePath addLineToPoint:CGPointMake(CGRectGetMidX(_triangleShape.frame) - 0.25, CGRectGetMinY(_triangleShape.frame))];
        [bottomTrianglePath addLineToPoint:CGPointMake(CGRectGetMidX(_triangleShape.frame) + 0.25, CGRectGetMinY(_triangleShape.frame))];
        [bottomTrianglePath addLineToPoint:CGPointMake(CGRectGetMaxX(_triangleShape.frame), CGRectGetMaxY(_triangleShape.frame) - 1.5)];
        _triangleShape.path = bottomTrianglePath.CGPath;
        _triangleShape.fillColor = [Delegate sharedDel].cellAlternateColor.CGColor;
        [_triangle.layer addSublayer:_triangleShape];
        
        [self addSubview:_triangle];
    }
    
    return self;
}

- (void)bobAfterDelay:(CGFloat)delay
{
    CGFloat duration = 0.1;
    CGFloat pixels = 10;
    
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionTransitionNone animations:^{
        [self move:pixels];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:duration animations:^{
            [self move:-pixels];
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:duration animations:^{
                [self move:pixels];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:duration animations:^{
                    [self move:-pixels];
                } completion:nil];
            }];
        }];
    }];
}

- (void)move:(CGFloat)pixels
{
    switch (self.tutType) {
        case TutorialTypeTop: self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y+pixels, self.frame.size.width, self.frame.size.height);
            break;
        case TutorialTypeBottom: self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y-pixels, self.frame.size.width, self.frame.size.height);
            break;
        case TutorialTypeRight: self.frame = CGRectMake(self.frame.origin.x+pixels, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
            break;
        case TutorialTypeLeft: self.frame = CGRectMake(self.frame.origin.x-pixels, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
            break;
    }
}

@end
