//
//  NewsArticleListViewController.h
//  MOST
//
//  Created by Hari Ganesan on 9/16/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import "ArticleListViewController.h"
#import "ExplanationView.h"

/** @brief Briefing and sections (politics...entertainment) article lists */
@interface NewsArticleListViewController : ArticleListViewController

@end


