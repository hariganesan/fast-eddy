//
//  ArticleHeaderView.m
//  BriefMe
//
//  Created by Charlie Vrettos on 6/30/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import "ArticleHeaderView.h"

@implementation ArticleHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        [self setUserInteractionEnabled:NO];
        
        self.title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, MAIN_WIDTH-60*SCALEUP, 100)];
        [self addSubview: self.title];
        [self.title setTextAlignment:NSTextAlignmentCenter];
        
    }
    return self;
}

- (void)resizeHeightToFitSubviews
{
    float h = 0;
    for (UIView *v in [self subviews]) {
        float fh = v.frame.origin.y + v.frame.size.height;
        h = MAX(fh, h);
    }
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, h)];
}


@end
