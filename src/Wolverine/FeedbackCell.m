//
//  FeedbackCell.m
//  BriefMe
//
//  Created by Hari Ganesan on 12/10/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import "FeedbackCell.h"
#import "Delegate.h"
#import "NavController.h"
#import "StreamListViewController.h"
#import "Localytics.h"

@implementation FeedbackCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];

        _bubble = [[UIView alloc] initWithFrame:CGRectMake(15*SCALEUP, 5*SCALEUP, (self.frame.size.width-30)*SCALEUP, 130*SCALEUP)]; // reset in viewcontroller due to expanding cell
        _bubble.layer.cornerRadius = 12;
        [self.contentView addSubview:_bubble];
        
        _title = [[UILabel alloc] initWithFrame:CGRectMake(30*SCALEUP, 25*SCALEUP, (self.frame.size.width-60)*SCALEUP, 30*SCALEUP)];
        _title.font = [UIFont fontWithName:@"BrandonGrotesque-Medium" size:18];
        _title.textColor = [UIColor whiteColor];
        _title.text = @"Are you liking BriefMe so far?";
        _title.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_title];
        
        _negative = [[UIButton alloc] initWithFrame:CGRectMake(45*SCALEUP, 80*SCALEUP, 100*SCALEUP, 40*SCALEUP)];
        [_negative setTitle:@"Well..." forState:UIControlStateNormal];
        [_negative addTarget:self action:@selector(negativeResponse:) forControlEvents:UIControlEventTouchUpInside];
        _negativeSelected = NO;
        
        _positive = [[UIButton alloc] initWithFrame:CGRectMake(175*SCALEUP, 80*SCALEUP, 100*SCALEUP, 40*SCALEUP)];
        [_positive setTitle:@"Yeah!" forState:UIControlStateNormal];
        [_positive addTarget:self action:@selector(positiveResponse:) forControlEvents:UIControlEventTouchUpInside];
        _positiveSelected = NO;
        
        _twitterPrompted = NO;
        
        for (UIButton *choice in @[_negative, _positive]) {
            choice.titleLabel.font = [UIFont fontWithName:@"BrandonGrotesque-Medium" size:18];
            [choice setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [choice setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
            choice.layer.borderWidth = 2.0f;
            choice.layer.cornerRadius = 12;
            choice.layer.borderColor = [UIColor whiteColor].CGColor;
            choice.backgroundColor = [UIColor clearColor];
            choice.titleLabel.textAlignment = NSTextAlignmentCenter;
            
            [self.contentView addSubview:choice];
        }
    }
    
    return self;
}

- (void)negativeResponse:(id)sender
{
    if (_twitterPrompted) {
        [Localytics tagEvent:@"FeedbackTwitterResponse" attributes:@{@"type": @"negative"}];
        [self removeWithPositiveTone:NO];
    } else if (!_positiveSelected && !_negativeSelected) {
        [UIView animateWithDuration:0.5 animations:^{
            _title.alpha = _negative.alpha = _positive.alpha = 0;
            [UIView animateWithDuration:0.5 animations:^{
                _title.text = @":( Tell us how we can improve!";
                [_positive setTitle:@"Okay" forState:UIControlStateNormal];
                [_negative setTitle:@"Meh" forState:UIControlStateNormal];
                _title.alpha = _negative.alpha = _positive.alpha = 1;
            }];
        }];
        
        _negativeSelected = YES;
        [Localytics tagEvent:@"FeedbackResponse" attributes:@{@"type": @"negative"}];
    } else {
        // remove cell
        [self removeWithPositiveTone:NO];
    }
}

- (void)positiveResponse:(id)sender
{
    if (_twitterPrompted) {
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:TWITTER_DEEP_URL]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:TWITTER_DEEP_URL]];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:TWITTER_URL]];
        }
        
        [Localytics tagEvent:@"FeedbackTwitterResponse" attributes:@{@"type": @"positive"}];
        [self removeWithPositiveTone:YES];
    } else if (!_positiveSelected && !_negativeSelected) {
        [UIView animateWithDuration:0.5 animations:^{
            _title.alpha = _negative.alpha = _positive.alpha = 0;
            [UIView animateWithDuration:0.5 animations:^{
                _title.text = @"Great! Rate us on the app store?";
                [_positive setTitle:@"Sure!" forState:UIControlStateNormal];
                [_negative setTitle:@"Nah" forState:UIControlStateNormal];
                _title.alpha = _negative.alpha = _positive.alpha = 1;
            }];
        }];
        
        _positiveSelected = YES;
        [Localytics tagEvent:@"FeedbackResponse" attributes:@{@"type": @"positive"}];
    } else if (_positiveSelected) {
        // go to app store
        NSString *appStoreLink = [NSString stringWithFormat:@"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%d&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8", APP_ID];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appStoreLink]];
        [Localytics tagEvent:@"FeedbackToAppStore"];
        
        // prompt twitter
        _title.font = [UIFont fontWithName:@"BrandonGrotesque-Medium" size:15];
        _twitterPrompted = YES;
        [UIView animateWithDuration:0.5 animations:^{
            _title.alpha = _negative.alpha = _positive.alpha = 0;
            [_positive setTitle:@"Why Not?" forState:UIControlStateNormal];
            [_negative setTitle:@"Nah" forState:UIControlStateNormal];
            [UIView animateWithDuration:0.5 animations:^{
                _title.text = @"Want more BriefMe? Follow us on Twitter!";
                _title.alpha = _negative.alpha = _positive.alpha = 1;
            }];
        }];
    } else if (_negativeSelected) {
        NSString *body = [NSString stringWithFormat:@"Enter your comment here: <br><br><br><br><br><br>iOS Version %@<br>BriefMe Version %@<br>Build %@<br>", IOS_VERSION, BRIEFME_VERSION, BUILD];
        [[Delegate sharedDel].nc shareOnEmail:self withSubject:@"Feedback on BriefMe" body:body andRecipients:@[@"support@getbriefme.com"]];
        [self removeWithPositiveTone:NO];

        [Localytics tagEvent:@"FeedbackToSupport"];
    }
}

- (void)removeWithPositiveTone:(BOOL)positiveTone
{
    if (positiveTone) {
        [UIView animateWithDuration:0.5 animations:^{
            _title.alpha = _negative.alpha = _positive.alpha = 0;
            [UIView animateWithDuration:0.5 animations:^{
                _title.text = @"Thanks!";
                [_positive setHidden:YES];
                [_negative setHidden:YES];
                _title.alpha = 1;
            }];
        }];
    } else {
        [UIView animateWithDuration:0.5 animations:^{
            _title.alpha = _negative.alpha = _positive.alpha = 0;
            [UIView animateWithDuration:0.5 animations:^{
                _title.text = @"Cool -- we won't bother you again.";
                [_positive setHidden:YES];
                [_negative setHidden:YES];
                _title.alpha = 1;
            }];
        }];
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"feedbackTapped"];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    // resets on reload (when switching themes)
    _bubble.backgroundColor = [Delegate sharedDel].sectionBackgroundColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    // do nothing
}

@end
