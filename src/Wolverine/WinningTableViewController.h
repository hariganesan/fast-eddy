//
//  WinningTableViewController.h
//  MOST
//
//  Created by Charlie Vrettos on 10/31/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExplanationView.h"

@class BottomNavigationBarView;

/** @brief Controller for view containing the Daily Three . */
@interface WinningTableViewController : UITableViewController

/** @brief The three articles that are displayed in the tableview. */
@property (nonatomic, strong) NSMutableArray *articles;
/** @discussion The explanation view which holds additonal article information
    accessible by tapping on the scores. */
@property (nonatomic, strong) ExplanationView *explanationView;
/** @brief TableView replacement for this subclass of <code>UITableViewController</code>. */
@property (nonatomic, strong) UITableView *tableViewV2;
/** @brief Navigation bar. Settings controller changes theme. */
@property (nonatomic, strong) BottomNavigationBarView *bar;

/** @discussion Initializes the controller and passes data from the JSON object
    to the <code>articles</code> property. */
- (id)initWithJson:(id)JSON;

@end
