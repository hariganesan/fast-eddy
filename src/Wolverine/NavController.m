//
//  NavController.m
//  MOST
//
//  Created by Kat Zhou on 1/17/13.
//  Copyright (c) 2013 Campion Designs, LLC. All rights reserved.
//

#import "NavController.h"
#import "Delegate.h"
#import "Parser.h"
#import "ArticleListViewController.h"
#import "NewsArticleListViewController.h"
#import "BriefingListViewController.h"
#import "StreamListViewController.h"
#import "SectionViewController.h"
#import "SettingsViewController.h"
#import "TBKTabBarController.h"
#import "Article.h"
#import "WinningTableViewController.h"
#import "Localytics.h"
#import "TUSafariActivity.h"
#import "BottomNavigationBarView.h"
#import "ArticleViewController.h"
#import "WebBrowserViewController.h"
#import "NSMutableArray+Queue.h"
#import "BlurrableImageView.h"

@interface NavController ()

@property (nonatomic) CGRect originalFrame;
/** @brief Article to be shared; Accessed by activityViewController */
@property (nonatomic, strong) Article *article;
/** @brief URL to be shared; Accessed by activityViewController */
@property (nonatomic, strong) NSURL *url;

@end

@implementation NavController

- (id)init
{
    self = [super init];
    if (self) {
        self.navigationBarHidden = YES;
        
        // black navigation bar on bottom of screen
        _bar = [[BottomNavigationBarView alloc] initWithType:BottomNavigationBarTypeLogo];
        _bar.backgroundColor = [UIColor blackColor];
        _bar.backgroundColor = [Delegate sharedDel].sectionBackgroundColor;
        _barHidden = YES;
        
        _blurQueue = [[NSMutableArray alloc] init];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addToBlurQueue:) name:@"addToBlurQueue" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(performBlur) name:@"performedBlur" object:nil];
        
        // create controllers back in delegate
    }
    return self;
}

- (void) createInitialController:(id)sender
{
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    [[UIView appearanceWhenContainedIn:[UITabBar class], nil] setTintColor:[UIColor whiteColor]];
    [[UITabBar appearance] setBarTintColor:[UIColor blackColor]];
    [[UITabBarItem appearance] setTitleTextAttributes: @{NSFontAttributeName: [UIFont fontWithName:@"BrandonGrotesque-Black" size:9], NSForegroundColorAttributeName: [UIColor whiteColor], NSKernAttributeName: @(5.0f)} forState:UIControlStateNormal];
    
    // push first controller onto nav stack
    UITabBarController *tc = [[UITabBarController alloc] init];
    tc.delegate = self;
    [tc.tabBar setClipsToBounds:YES];
//    [tc.tabBar setSelectionIndicatorImage: [UIImage imageNamed:@"tabbarselection.png"]];
    self.viewControllers = @[tc];
    
    // settings (done first for alvc colors)
    [Delegate sharedDel].sectionBackgroundColor = LIPSTICK_COLOR;
    SettingsViewController *settingsvc = [[SettingsViewController alloc] init];
    UIImage *settingsImage = [UIImage imageNamed:@"settings.png"];
    [settingsvc.tabBarItem setImage:[[Delegate imageWithImage:settingsImage scaledToSize:CGSizeMake(settingsImage.size.width/2, settingsImage.size.height/2)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [Delegate sharedDel].settingsvc = settingsvc;
    
    // main briefing section
    ArticleListViewController *bvc;
    
    // TODO(hari): use normal titles and use kerning in headerview
    bvc = [[BriefingListViewController alloc] initWithSection:SECTION_BRIEFING andTitle:@"B R I E F I N G"];
    _currentViewController = bvc;
    
    UIImage *briefingImage = [UIImage imageNamed:@"briefing.png"];
    [bvc.tabBarItem setImage:[[Delegate imageWithImage:briefingImage scaledToSize:CGSizeMake(briefingImage.size.width/2, briefingImage.size.height/2)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    // other sections
    TBKTabBarController *tabsectvc = [[TBKTabBarController alloc] init];
    
    ArticleListViewController *sectvc1 = [[NewsArticleListViewController alloc] initWithSection:SECTION_POLITICS andTitle:@"P O L I T I C S"];
    ArticleListViewController *sectvc2 = [[NewsArticleListViewController alloc] initWithSection:SECTION_WORLD andTitle:@"W O R L D"];
    ArticleListViewController *sectvc3 = [[NewsArticleListViewController alloc] initWithSection:SECTION_BUSINESS andTitle:@"B U S I N E S S"];
    ArticleListViewController *sectvc4 = [[NewsArticleListViewController alloc] initWithSection:SECTION_SPORTS andTitle:@"S P O R T S"];
    ArticleListViewController *sectvc5 = [[NewsArticleListViewController alloc] initWithSection:SECTION_TECHNOLOGY andTitle:@"T E C H N O L O G Y"];
    ArticleListViewController *sectvc6 = [[NewsArticleListViewController alloc] initWithSection:SECTION_ENTERTAINMENT andTitle:@"E N T E R T A I N M E N T"];
    
    UIImage *politicsImage = [UIImage imageNamed:@"politics-black-big.png"];
    UIImage *worldImage = [UIImage imageNamed:@"world-black-big.png"];
    UIImage *businessImage = [UIImage imageNamed:@"business-black-big.png"];
    UIImage *sportsImage = [UIImage imageNamed:@"sports-black-big.png"];
    UIImage *technologyImage = [UIImage imageNamed:@"technology-black-big.png"];
    UIImage *entertainmentImage = [UIImage imageNamed:@"entertainment-black-big.png"];

    UIImage *selectedPoliticsImage = [UIImage imageNamed:@"politics.png"];
    UIImage *selectedWorldImage = [UIImage imageNamed:@"world.png"];
    UIImage *selectedBusinessImage = [UIImage imageNamed:@"business.png"];
    UIImage *selectedSportsImage = [UIImage imageNamed:@"sports.png"];
    UIImage *selectedTechnologyImage = [UIImage imageNamed:@"technology.png"];
    UIImage *selectedEntertainmentImage = [UIImage imageNamed:@"entertainment.png"];
    
    [sectvc1.tabBarItem setImage:[[Delegate imageWithImage:politicsImage scaledToSize:CGSizeMake(politicsImage.size.width/4, politicsImage.size.height/4)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [sectvc2.tabBarItem setImage:[[Delegate imageWithImage:worldImage scaledToSize:CGSizeMake(worldImage.size.width/4, worldImage.size.height/4)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [sectvc3.tabBarItem setImage:[[Delegate imageWithImage:businessImage scaledToSize:CGSizeMake(businessImage.size.width/4, businessImage.size.height/4)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [sectvc4.tabBarItem setImage:[[Delegate imageWithImage:sportsImage scaledToSize:CGSizeMake(sportsImage.size.width/4, sportsImage.size.height/4)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [sectvc5.tabBarItem setImage:[[Delegate imageWithImage:technologyImage scaledToSize:CGSizeMake(technologyImage.size.width/4, technologyImage.size.height/4)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [sectvc6.tabBarItem setImage:[[Delegate imageWithImage:entertainmentImage scaledToSize:CGSizeMake(entertainmentImage.size.width/4, entertainmentImage.size.height/4)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [sectvc1.tabBarItem setSelectedImage:[[Delegate imageWithImage:selectedPoliticsImage scaledToSize:CGSizeMake(politicsImage.size.width/4, politicsImage.size.height/4)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [sectvc2.tabBarItem setSelectedImage:[[Delegate imageWithImage:selectedWorldImage scaledToSize:CGSizeMake(worldImage.size.width/4, worldImage.size.height/4)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [sectvc3.tabBarItem setSelectedImage:[[Delegate imageWithImage:selectedBusinessImage scaledToSize:CGSizeMake(businessImage.size.width/4, businessImage.size.height/4)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [sectvc4.tabBarItem setSelectedImage:[[Delegate imageWithImage:selectedSportsImage scaledToSize:CGSizeMake(sportsImage.size.width/4, sportsImage.size.height/4)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [sectvc5.tabBarItem setSelectedImage:[[Delegate imageWithImage:selectedTechnologyImage scaledToSize:CGSizeMake(technologyImage.size.width/4, technologyImage.size.height/4)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [sectvc6.tabBarItem setSelectedImage:[[Delegate imageWithImage:selectedEntertainmentImage scaledToSize:CGSizeMake(entertainmentImage.size.width/4, entertainmentImage.size.height/4)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];

    SectionViewController *sectvc = [[SectionViewController alloc] init];
    tabsectvc.title = @"S E C T I O N S";
    sectvc.title = @"S E C T I O N S";
    UIImage *sectionsImage = [UIImage imageNamed:@"sections.png"];
    [sectvc.tabBarItem setImage:[[Delegate imageWithImage:sectionsImage scaledToSize:CGSizeMake(sectionsImage.size.width/2, sectionsImage.size.height/2)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabsectvc.tabBarItem setImage:[[Delegate imageWithImage:sectionsImage scaledToSize:CGSizeMake(sectionsImage.size.width/2, sectionsImage.size.height/2)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    tabsectvc.tabBar.frame = CGRectMake(0, MAIN_HEIGHT-tc.tabBar.frame.size.height-50, MAIN_WIDTH, 50);
    tabsectvc.viewControllers = @[sectvc1, sectvc2, sectvc3, sectvc4, sectvc5, sectvc6];

    // Stream
    StreamListViewController *streamvc = [[StreamListViewController alloc] initWithSection:SECTION_STREAM andTitle:@"S T R E A M"];
    UIImage *streamImage = [UIImage imageNamed:@"stream.png"];
    [streamvc.tabBarItem setImage:[[Delegate imageWithImage:streamImage scaledToSize:CGSizeMake(streamImage.size.width/2, streamImage.size.height/2)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [self parseObjects];

    [tc setViewControllers:@[bvc, sectvc, streamvc, settingsvc] animated:NO];
    tc.selectedViewController = tc.viewControllers[0];
    self.sectionViewControllers = @[bvc, sectvc1, sectvc2, sectvc3, sectvc4, sectvc5, sectvc6, streamvc];
    
    [Delegate sharedDel].briefingvc = (BriefingListViewController *)bvc;
    [Delegate sharedDel].sectvc = sectvc;
    [Delegate sharedDel].tabsectvc = tabsectvc;
    [Delegate sharedDel].streamvc = streamvc;
}

- (void)parseObjects
{
    // init parser
    [Delegate sharedDel].parsedNews = [[NSMutableDictionary alloc] init];
    [Delegate sharedDel].parser = [[Parser alloc] init];
    
    // parse objects and add references to delegate
    for (NSInteger i = 0; i < [BM_ROUTES count]; i++) {
        [[Delegate sharedDel].parser parseObjectsWithSection:i];
    }
    
//    [[Delegate sharedDel].parser parseWinners];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // add back swipe gesture to all secondary views
    // http://stackoverflow.com/questions/19054625/changing-back-button-in-ios-7-disables-swipe-to-navigate-back/20330647#20330647
    // http://keighl.com/post/ios7-interactive-pop-gesture-custom-back-button/
    
    __weak NavController *weakSelf = self;
    
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.interactivePopGestureRecognizer.delegate = weakSelf;
        self.delegate = weakSelf;
    }
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    // Hijack the push method to disable the gesture
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)])
        self.interactivePopGestureRecognizer.enabled = NO;
    
    [super pushViewController:viewController animated:animated];
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    // Enable the gesture again once the new controller is shown
    self.interactivePopGestureRecognizer.enabled = ([self respondsToSelector:@selector(interactivePopGestureRecognizer)] && [self.viewControllers count] > 1);
}

- (void)toggleArticleNavigation
{
    if (![self.topViewController isKindOfClass:[ArticleListViewController class]]) {
        [self.topViewController.view addSubview:_bar];
        [self resetOrientation];
        [self showNavigation];
    }
}

- (void)showNavigation
{
    if (_barHidden) {
        _barHidden = NO;
        [self.topViewController.view addSubview:_bar];
        [UIView animateWithDuration:0.2 animations:^{
            _bar.frame = CGRectMake(_bar.frame.origin.x, MAIN_HEIGHT-NAVIGATION_BAR_HEIGHT, _bar.frame.size.width, NAVIGATION_BAR_HEIGHT);
        } completion:nil];
    }
}

- (void)hideNavigation
{
    if (!_barHidden) {
        _barHidden = YES;
        [UIView animateWithDuration:0.2 animations:^{
            _bar.frame = CGRectMake(_bar.frame.origin.x, MAIN_HEIGHT, _bar.frame.size.width, 0);
        } completion:^(BOOL finished){
            if (finished) {
                [_bar removeFromSuperview];
            }
        }];
    }
}

- (void)resetOrientation
{
    CGRect newFrame = _bar.frame;
    CGRect newBookFrame = _bar.briefMeImageView.frame;
    CGRect newShareFrame = _bar.shareButtonImageView.frame;

    newFrame.origin.y = MAIN_HEIGHT-newFrame.size.height;
    newFrame.size.width = MAIN_WIDTH;
    newBookFrame.origin.x = MAIN_WIDTH/2-newBookFrame.size.width/2;
    newShareFrame.origin.x = MAIN_WIDTH-newShareFrame.size.width-8*SCALEUP;
    
    _bar.frame = newFrame;
    _bar.briefMeImageView.frame = newBookFrame;
    _bar.shareButtonImageView.frame = newShareFrame;
}

- (void)deviceOrientationDidChange
{
    // rotate the rotatable image
    [UIView animateWithDuration:0.3 animations:^{
        [self resetOrientation];
    } completion:^(BOOL finished) {
        _barHidden = NO;
    }];
}

- (void)shareButtonPressedWithURL:(NSURL *)url
{
    // nil so that (id)activityViewController:... knows we are sharing a URL
    _article = nil;
    _url = url;
    
    NSMutableDictionary *attrs = [NSMutableDictionary dictionaryWithDictionary:@{@"url": [url absoluteString]}];
    [self shareWithAttrs:attrs];

}

- (void)shareButtonPressedWithArticle:(Article *)art
{
    _article = art;
    // nil so that (id)activityViewController:... knows we are sharing an article
    _url = nil;
    
    NSMutableDictionary *attrs = [NSMutableDictionary dictionaryWithDictionary:[self.article getAttributes]];
    
    [self shareWithAttrs:attrs];
}

- (void)shareWithAttrs:(NSMutableDictionary *)attrs
{
    UIActivityViewController *activityvc;
    TUSafariActivity *sa = [[TUSafariActivity alloc] init];
    
    activityvc = [[UIActivityViewController alloc] initWithActivityItems:@[self] applicationActivities:@[sa]];
    
//    UIActivityViewController
    [activityvc setCompletionWithItemsHandler:^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
        if (completed) {
            [attrs addEntriesFromDictionary:@{@"type": activityType}];
            
            [Localytics tagScreen:@"Share Completed"];
            [Localytics tagEvent:@"Share Completed" attributes:attrs];
        } else {
            [Localytics tagScreen:@"Share Dismissed"];
        }
    }];
    
    [self presentViewController:activityvc animated:YES completion:^{
        [Localytics tagScreen:@"Share Opened"];
    }];
}

- (id)activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController
{
    if (_url)
        return _url;
    return [NSURL URLWithString:self.article.url];
}

- (id)activityViewController:(UIActivityViewController *)activityViewController itemForActivityType:(NSString *)activityType
{
    // sharing just a URL; only not nil when shareButtonPressedWithURL is called
    if (_url)
        return _url;

    [Localytics tagScreen:@"Share Selected"];
    [Localytics tagEvent:@"Share Selected" attributes:@{@"type": activityType}];

    if ([activityType isEqualToString:UIActivityTypePostToFacebook] && self.article.url)
        return [NSURL URLWithString:self.article.url];
    else if ([activityType isEqualToString:UIActivityTypePostToTwitter] && self.article.title && self.article.shareUrl)
        return [NSString stringWithFormat:@"%@: %@ Discovered on #BriefMe", self.article.title, self.article.shareUrl];
    else if ([activityType isEqualToString:UIActivityTypeMail] && self.article.shareUrl && self.article.title)
        return [NSString stringWithFormat:@"<html><body><a href='%@'>%@</a><br /><br />Discovered on <a href='http://www.getbriefme.com/'>BriefMe</a></body></html>", [NSURL URLWithString:self.article.shareUrl], self.article.title];
    else if ([activityType isEqualToString:UIActivityTypeMessage] && self.article.shareUrl)
        return self.article.shareUrl;
    else if ([activityType isEqualToString:UIActivityTypeCopyToPasteboard] && self.article.shareUrl)
        return [NSURL URLWithString:self.article.shareUrl];
    else if (self.article.url) // add to reading list, pinterest, etc.
        return [NSURL URLWithString:self.article.url];
    else return nil;
}

- (id)activityViewController:(UIActivityViewController *)activityViewController subjectForActivityType:(NSString *)activityType
{
    if ([activityType isEqualToString:UIActivityTypeMail] && _article && _article.title) {
        return self.article.title;
    } else {
        return nil;
    }
}

- (void) tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    if (viewController == [Delegate sharedDel].sectvc) {
        [[Delegate sharedDel].sectvc resetView];
    }
    
    // if "top" or "stream" selected, and not already the current view controller, send analytics
    if (viewController != _currentViewController) {
        if ([viewController isKindOfClass:[ArticleListViewController class]]) {
            ArticleListViewController *alvc = (ArticleListViewController *)viewController;
            // log 'top' & 'stream' taps; doesn't include the first view of the briefing section
            [Localytics tagEvent:@"Section Selected" attributes:@{@"section": BM_ROUTES[alvc.section]}];
        } else if ([viewController isKindOfClass:[SettingsViewController class]]) {
            [Localytics tagEvent:@"Section Selected" attributes:@{@"section": @"settings"}];
        } else if ([viewController isKindOfClass:[SectionViewController class]]) {
            [Localytics tagEvent:@"Section Selected" attributes:@{@"section": @"sections"}];
        }
    }
    
    _currentViewController = viewController;
}

- (void)shareOnEmail:(id)sender withSubject:(NSString *)subject body:(NSString *)body andRecipients:(NSArray *)toRecipients
{
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        
        [mailer setToRecipients:toRecipients];
        [mailer setSubject:subject];
        [mailer setMessageBody:body isHTML:YES];
        
        [[Delegate sharedDel].window.rootViewController presentViewController:mailer animated:YES completion:nil];
    } else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Failure" message:@"Your device does not have any email accounts set up. Please contact us at support@getbriefme.com" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        { case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break; }
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    
    // Remove the mail view
    [self dismissViewControllerAnimated:YES completion:nil];
}

+ (UIViewController*)topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

- (BOOL)shouldAutorotate
{
    return self.topViewController.shouldAutorotate;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return self.topViewController.supportedInterfaceOrientations;
}

- (BOOL)prefersStatusBarHidden
{
    return self.topViewController.prefersStatusBarHidden;
}

/** @brief handles when an article is passed into BriefMe. */
- (void)restoreUserActivityState:(NSUserActivity *)activity
{    
    if ([activity.userInfo objectForKey:@"articleDict"]) {
        NSMutableDictionary *articleDict = [activity.userInfo objectForKey:@"articleDict"];
        
        // check if we have the article 'content' stored in briefingContentsDict from when /top loaded on
        // the phone, and if so, use it to allow briefmeview; if not, no briefmeview. this is required
        // because having content in the useractivity made it too large to transfer via handoff consistently
        if ([Delegate sharedDel].briefingContentsDict) {
            NSString *articleUrl = [articleDict objectForKey:@"url"];
            NSString *articleContent = [[Delegate sharedDel].briefingContentsDict objectForKey:articleUrl];
            if (articleContent) {
                [articleDict setObject:articleContent forKey:@"content"];
            }
        }
        
        NewsArticle *nArt = [[NewsArticle alloc] initWithDictionary:articleDict];
        nArt.rank = [articleDict objectForKey:@"rank"];
        self.article = nArt;
    
        ArticleViewController *avc = [[ArticleViewController alloc] initWithArticle:self.article];
        
        if ([[self topViewController] isKindOfClass:[ArticleViewController class]]) {
            [self popViewControllerAnimated:NO];
        }
        
        [Delegate sharedDel].nc.currentViewController = avc;
        [self pushViewController:avc animated:YES];
    }
    else {
        NSLog(@"no articleDict");
    }
}

- (void)pushWebBrowser:(id)url
{
    // need to perform on main thread for ios 9 exception
    [self performSelectorOnMainThread:@selector(innerPushWebBrowser:) withObject:url waitUntilDone:NO];
}

- (void)innerPushWebBrowser:(id)url
{
    WebBrowserViewController *wbvc = [[WebBrowserViewController alloc] initWithUrl:[NSURL URLWithString:url]];
    [Delegate sharedDel].nc.currentViewController = wbvc;
    [[Delegate sharedDel].nc pushViewController:wbvc animated:YES];
}

- (void)addToBlurQueue:(NSNotification *)sender
{
    if (sender.object != nil && [sender.object isMemberOfClass:[BlurrableImageView class]]) {
        [_blurQueue enqueue:sender.object];
        
        [self performBlur];
    }
}

- (void)performBlur
{
    if ([_blurQueue count] > 0 && !_blurring) {
        _blurring = YES;
        BlurrableImageView *imageView = [_blurQueue dequeue];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            // _blurring is set to NO after blur is completed in BlurrableImageView
            [imageView blurAndSetImages];
        });
    }
}

@end
