//
//  FeedbackCell.h
//  BriefMe
//
//  Created by Hari Ganesan on 12/10/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @discussion  Cell used to display a feedback message between normal cells such as "Are you enjoying BriefMe?"
    check https://medium.com/circa/the-right-way-to-ask-users-to-review-your-app-9a32fd604fca
    http://stackoverflow.com/questions/18905686/itunes-review-url-and-ios-7-ask-user-to-rate-our-app-appstore-show-a-blank-pag
    only shown in IOS 8 */
@interface FeedbackCell : UITableViewCell

/** @brief The outer (red) bubble of the cell. */
@property (nonatomic, strong) UIView *bubble;
/** @brief The main question/statement to the user. */
@property (nonatomic, strong) UILabel *title;
/** @brief The button that follows any "yes/accept" dialogue. */
@property (nonatomic, strong) UIButton *positive;
/** @brief The button that follows any "no/deny" dialogue. */
@property (nonatomic, strong) UIButton *negative;

/** @brief YES if user selected positive response first. */
@property (nonatomic) BOOL positiveSelected;
/** @brief YES if user selected negative response first. */
@property (nonatomic) BOOL negativeSelected;
/** @brief YES if twitter has been prompted. */
@property (nonatomic) BOOL twitterPrompted;

@end
