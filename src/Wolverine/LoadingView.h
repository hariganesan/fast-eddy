//
//  LoadingView.h
//  MOST
//
//  Created by Charlie Vrettos on 10/9/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Delegate.h"

/** @discussion  View that is displayed on every app init. 
    Shows BriefMe book flipping while request is made to /top in the background. */
@interface LoadingView : UIView

@property (nonatomic, strong) UIImageView *book;
@property (nonatomic, strong) UIView *bookHolder;
@property (nonatomic, strong) UILabel *BriefMe;
/** @brief "What the world is reading now" subtitle */
@property (nonatomic, strong) UILabel *WhatTheWorld;

/** @brief The number of sections of the app (top, politics, etc.) loaded. */
@property (nonatomic) NSInteger loadCount;
/** @brief Set to YES if app is already loaded before pages start flipping. */
@property (nonatomic) BOOL shouldHideAnim;
/** @brief Set to YES if pages have been flipping and app is loaded */
@property (nonatomic) BOOL animFinished;

/** @brief Animates the flipping pages. */
- (void)animate;

@end
