//
//  ZoomableImageView.m
//  BriefMe
//
//  Created by Charlie Vrettos on 6/17/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import "ZoomableImageView.h"
#import "Delegate.h"
#import "Localytics.h"
#import "NavController.h"

@implementation ZoomableImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.minimumZoomScale=1.0;
        self.maximumZoomScale=6.0;
        self.alpha = 1.0;
        self.backgroundColor = CHARCOAL_COLOR;

        UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close)];
        [self addGestureRecognizer:gr];
        
        _currentImageUrl = nil;
        _activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        _activityView.frame = CGRectMake(self.frame.size.width/2-20/2, self.frame.size.height/2-20/2, 20, 20);
        [self addSubview:_activityView];
        
        [self setShowsHorizontalScrollIndicator:NO];
        [self setShowsVerticalScrollIndicator:NO];
    }
    return self;
}

- (void)close
{
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 0.0f;
        self.zoomScale += 0.1;
    } completion:^(BOOL finished) {
        self.zoomScale = 1.0;
        // don't destroy _imageView in case user re-opens the same image as the last fullscreen img
        self.hidden = YES;
    }];
    
    [_activityView stopAnimating];
    [self lockZoom];
}

- (void)loadImage:(NSURL *)url
{
    self.alpha = 0.0f;
    self.hidden = NO;
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 1.0f;
    } completion:^(BOOL finished) {

    }];
    
    [_activityView startAnimating];
    
    if ([_currentImageUrl isEqual:url]) {
        [_activityView stopAnimating];
        [self unlockZoom];
        return;
    }
    _imageView.image = nil;
    
    [self downloadImageWithURL:url completionBlock:^(BOOL succeeded, UIImage *image) {
        if (succeeded) {
            // change the image in the cell
            _currentImageUrl = url;
            
            if (!_imageView) {
                _imageView = [[UIImageView alloc] initWithImage:image];
                _imageView.frame = CGRectMake(10*SCALEUP, 10*SCALEUP, MAIN_WIDTH-20*SCALEUP, MAIN_HEIGHT-20*SCALEUP);
                _imageView.contentMode = UIViewContentModeScaleAspectFit;
                [self addSubview:_imageView];
            } else {
                self.zoomScale = 1.0;
                _imageView.image = image;
            }
            [_activityView stopAnimating];
            [self unlockZoom];
            return;
        } else { // failed to download            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Image Load Failure" message:@"" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self close];
            }]];
            [[Delegate sharedDel].nc presentViewController:alert animated:YES completion:nil];
            return;
        }
    }];
}

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if (!error) {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   if (image)
                                       completionBlock(YES,image);
                                   else
                                       completionBlock(NO,nil);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}

-(void)lockZoom
{
    self.maximumZoomScale = 1.0;
    self.minimumZoomScale = 1.0;
}

-(void)unlockZoom
{
    self.minimumZoomScale=1.0;
    self.maximumZoomScale=6.0;
}

@end
