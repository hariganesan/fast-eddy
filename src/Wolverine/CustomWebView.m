//
//  CustomWebView.m
//  BriefMe
//
//  Created by Charlie Vrettos on 6/26/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import "CustomWebView.h"
#import "Delegate.h"
#import "BottomNavigationBarView.h"
#import "SettingsViewController.h"

@interface CustomWebView ()
@property (nonatomic) BOOL needWebBlocks;
@end

@implementation CustomWebView

- (id)initWithFrame:(CGRect)frame andWithNavBar:(BottomNavigationBarView *)navBar andWithWebConfig:(WKWebViewConfiguration *)webConfig
{
    self = [super initWithFrame:frame configuration:webConfig];
    if (self) {
        _navBar = navBar;
        self.scrollView.bounces = YES;
        _lastContentOffset = 0.0f;
        _needWebBlocks = YES;
        
        _statusBarScreenshot = [[UIView alloc] init];
        _statusBarBackground = [[UIView alloc] initWithFrame:CGRectMake(0, -1*STATUS_BAR_HEIGHT, MAIN_WIDTH, STATUS_BAR_HEIGHT)];
        [_statusBarBackground setBackgroundColor:[Delegate sharedDel].articleBackgroundColor];
        [self addSubview:_statusBarBackground];

        _activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _activityView.frame = CGRectMake(MAIN_WIDTH / 2 - 10, MAIN_HEIGHT / 2 - 10, 20.0f, 20.0f);
        [self addSubview:_activityView];
        
        [self.scrollView setContentInset:UIEdgeInsetsMake(0, 0, NAVIGATION_BAR_HEIGHT, 0)];
    }
    return self;
}

- (void)loadFromString:(NSString *)html
{
    _needWebBlocks = NO;
    [self loadHTMLString:html baseURL:nil];
    [_activityView stopAnimating];
}

- (void)loadFromURL:(NSURL *)url
{
    _needWebBlocks = YES;
    [_activityView startAnimating];
    [self loadRequest:[NSURLRequest requestWithURL:url]];
}

+ (WKWebViewConfiguration *)createConfigWithScriptsForUrl:(NSURL *)url scroll:(BOOL)scroll scrollKey:(NSString *)scrollKey openLink:(BOOL)openLink openImage:(BOOL)openImage loadedDOM:(BOOL)loadedDOM scale:(BOOL)scale nightMode:(BOOL)nightMode webBlocks:(BOOL)webBlocks
{
    WKWebViewConfiguration *webConfig = [[WKWebViewConfiguration alloc] init];
    WKUserContentController* userController = [[WKUserContentController alloc] init];
    
    NSString *js = @"";
    WKUserScript* userScript = nil;
    
    if (scroll) {
        js = [NSString stringWithFormat:@"window.onscroll = function() {window.webkit.messageHandlers.%@.postMessage(window.scrollY);};", scrollKey];
        userScript = [[WKUserScript alloc]initWithSource:js injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];
        [userController addUserScript:userScript];
    }
    if (openLink) {
        js = [NSString stringWithFormat:@"for(var i = 0, l=document.links.length; i<l; i++) { link = document.links[i]; var oldOnClick = link.getAttribute('onclick'); if (link.getAttribute('briefMeReadMore')) { link.setAttribute(\"onclick\", oldOnClick + \"; window.webkit.messageHandlers.%@.postMessage('');\"); } else { link.setAttribute(\"onclick\", oldOnClick + \"; window.webkit.messageHandlers.%@.postMessage('\" + link.href + \"');\"); }link.href = '#!'; };", READ_MORE_KEY, OPEN_LINK_KEY];
        userScript = [[WKUserScript alloc]initWithSource:js injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];
        [userController addUserScript:userScript];
    }
    if (openImage) {
        js = [NSString stringWithFormat:@"for(var i = 0, l=document.images.length; i<l; i++) { image = document.images[i]; var oldOnClick = image.getAttribute('onclick'); var fullSizeImageUrl = image.src; if (image.getAttribute('briefMeFullSrc')) { fullSizeImageUrl = image.getAttribute('briefMeFullSrc'); }; if (image.getAttribute('briefMeGif')) { image.setAttribute(\"onclick\", oldOnClick + \"; window.webkit.messageHandlers.%@.postMessage('\" + fullSizeImageUrl + \"');\"); } else { image.setAttribute(\"onclick\", oldOnClick + \"; window.webkit.messageHandlers.%@.postMessage('\" + fullSizeImageUrl + \"');\"); } };", OPEN_LINK_KEY, OPEN_IMAGE_KEY];
        userScript = [[WKUserScript alloc]initWithSource:js injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];
        [userController addUserScript:userScript];
    }
    if (scale) {
        js = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
        userScript = [[WKUserScript alloc]initWithSource:js injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];
        [userController addUserScript:userScript];
    }
    if (loadedDOM) {
        js = [NSString stringWithFormat:@"window.webkit.messageHandlers.%@.postMessage('hi');", LOADED_DOM_KEY];
        // note that this uses forMainFrameOnly:YES
        userScript = [[WKUserScript alloc]initWithSource:js injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
        [userController addUserScript:userScript];
    }
    if (nightMode) {
        if ([Delegate sharedDel].settingsvc.control.selectedSegmentIndex == THEME_NIGHT || ([Delegate sharedDel].settingsvc.control.selectedSegmentIndex == THEME_AUTO && [Delegate autoTheme] == AUTO_THEME_NIGHT)) {
            js = @"var body=document.getElementsByTagName('body')[0]; if (body.className.indexOf('night') == -1) { body.className += ' night';}";
            userScript = [[WKUserScript alloc]initWithSource:js injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];
            [userController addUserScript:userScript];
        }
    }
    if (webBlocks && [Delegate sharedDel].webBlocks && [[Delegate sharedDel].webBlocks objectForKey:@"blocks"]) {
        js = [self buildWebBlockJSForUrl:url];
        // This is the first execution of the webBlocks. They are executed again using injectWebBlocks in case these execute too early
        //  or the host detected from the original URL was inaccurate so no webblocks were found.
        userScript = [[WKUserScript alloc]initWithSource:js injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];
        [userController addUserScript:userScript];
    }
    
    webConfig.userContentController = userController;
    
    return webConfig;
}

+ (NSString *)buildWebBlockJSForUrl:(NSURL *)url
{
    NSURLComponents *urlComponents = [[NSURLComponents alloc] initWithURL:url resolvingAgainstBaseURL:YES];
    NSString *host = [urlComponents.host stringByReplacingOccurrencesOfString:@"www." withString:@""];
    
    // use webblocks loaded from webBlocks in config from /all
    NSDictionary *hostDict = [[[Delegate sharedDel].webBlocks objectForKey:@"blocks"] objectForKey:host];
    if (!hostDict) {
        return @"";
    }

    // build a chunk of js commands for each action used (obj.display='none', obj.visibility='hidden', or obj.click()), and concatenate them all
    // execute the js with {frame} = document (like document.getElementById...)
    NSString *jsCommand = @"//execute js on document\n frame = document; function f(){";
    for (NSString *action in @[@"display-none", @"visibility-hidden", @"click", @"custom"])
        jsCommand = [NSString stringWithFormat:@"%@%@", jsCommand, [self buildWebBlockJSChunkForAction:action withDict:hostDict]];
    
    // execute the js in all iframes
    jsCommand = [NSString stringWithFormat:@"%@}; f(); \n\n //execute js on each frame\n iframes = document.getElementsByTagName('iframe'); for (i = 0; i < iframes; i++) { frame = iframes[i].contentDocument || iframes[i].contentWindow.document; f();};", jsCommand];
    
    return jsCommand;
}

+ (NSString *)buildWebBlockJSChunkForAction:(NSString *)action withDict:(NSDictionary *)dict
{
    NSString *newCommands = @"";
    
    if ([action isEqualToString:@"custom"])
    {
        NSArray *lines = [dict objectForKey:@"custom"];
        if (lines) {
            for (id line in lines) {
                newCommands = [NSString stringWithFormat:@"%@%@;", newCommands, line];
            }
        }
        return newCommands;
    }
    
    NSDictionary *actionDict = [dict objectForKey:action];
    if (!actionDict)
        return @"";
    
    NSString *jsAction = @"";
    
    // translate the action names into the actual js commands as strings
    if ([action isEqualToString:@"display-none"])
        jsAction = @".style.display='none'";
    else if ([action isEqualToString:@"visibility-hidden"])
        jsAction = @".style.visibility='hidden'";
    else if ([action isEqualToString:@"click"])
        jsAction = @".click()";
    else {
        NSLog(@"ERROR: passed a bad action");
        return @"";
    }
    
    // if there is a list of ids to call this action on in the json, build the corresponding js
    // this is used when we want to do "{frame}.getElementById('id').click() or .style.display='none' or .style.visibility='hidden'
    NSArray *idArray = [actionDict objectForKey:@"ids"];
    if (idArray) {
        for (id idName in idArray) {
            newCommands = [NSString stringWithFormat:@"%@ if (frame.getElementById('%@')){frame.getElementById('%@')%@;};", newCommands, idName, idName, jsAction];
        }
    }
    
    // if there is a list of classes with indices to call this action on in the json, build the corresponding js
    // this is used when we want to do "{frame}.getElementsByClassName('classname')[index].click() or .style.display='none' or .style.visibility='hidden'
    NSArray *classArray = [actionDict objectForKey:@"classes"];
    if (classArray) {
        for (id obj in classArray) {
            id className = [obj objectForKey:@"name"];
            id classIndex = [obj objectForKey:@"index"];
            
            newCommands = [NSString stringWithFormat:@"%@ if (frame.getElementsByClassName('%@')[%@]){frame.getElementsByClassName('%@')[%@]%@;};", newCommands, className, classIndex, className, classIndex, jsAction];
        }
    }
    
    // if there is a list of classes with to call this action on all the children of, build the corresponding js
    // this is used when we want to do "{frame}.getElementsByClassName('classname')[index].click() or .style.display='none' or .style.visibility='hidden' on ALL indices
    NSArray *wholeClassArray = [actionDict objectForKey:@"class-arrays"];
    if (wholeClassArray) {
        for (id className in wholeClassArray) {
            newCommands = [NSString stringWithFormat:@"%@ for (i=0; obj=frame.getElementsByClassName('%@')[i]; i++){obj%@;};", newCommands, className, jsAction];
        }
    }
    return newCommands;
}

- (void)finishedNavigation
{
    if (_needWebBlocks && [Delegate sharedDel].webBlocks && [[Delegate sharedDel].webBlocks objectForKey:@"blocks"]) {
        [self injectWebBlocks];
    }
    [self.activityView stopAnimating];
}

- (void)injectWebBlocks
{
    // This is the second execution of the webblocks. It acts as a backup in case:
    //  1. The original URL (used in the first webblocks execution, the userscript execution) was un-redirected, like 'feedburner.com/nytimes/123', meaning
    //      the host detected would have been feedburner, which isn't in the webBlocks dict
    //  2. The first execution of the web blocks was too early. An advertisement may load after the DOM loads, and the first execution occurs when DOM loads.
    NSString *jsCommand = [[self class] buildWebBlockJSForUrl:self.URL];
    if (jsCommand) {
        [self evaluateJavaScript:jsCommand completionHandler:nil];
    }
}

- (BOOL)draggingOrBouncing
{
    CGFloat maxScroll = self.scrollView.contentSize.height;
    CGFloat scroll = self.scrollView.contentOffset.y + self.scrollView.frame.size.height;

    if (self.scrollView.dragging || self.scrollView.dragging || scroll > maxScroll || scroll < 0)
        return YES;
    return NO;
}

@end
