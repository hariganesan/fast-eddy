//
//  NewsArticleListViewController.m
//  MOST
//
//  Created by Hari Ganesan on 9/16/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import "NewsArticleListViewController.h"
#import "NewsArticleCell.h"
#import "NewsArticle.h"
#import "UICircleView.h"
#import "NewsArticleInnerCellView.h"

@implementation NewsArticleListViewController

- (id)initWithSection:(SectionType)section andTitle:(NSString *)title
{
    self = [super initWithSection:section andTitle:title];
    if (self) {        
        // EXPLANATION VIEW
        self.explanationView = [[ExplanationView alloc] initWithFrame:CGRectMake(0, 0, MAIN_WIDTH, MAIN_HEIGHT)];
        [self.view addSubview:self.explanationView];
    }
    return self;
}

// populate and display table cells
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{    
    NewsArticleCell *ncell = (NewsArticleCell *)[super tableView:tableView cellForRowAtIndexPath:indexPath];
    NewsArticle *art = (NewsArticle *)[self.objects objectAtIndex:indexPath.row];

    [ncell.newsArticleInnerCellView setStyledTitle:art.title withMinFontSize:14.0f andMaxFontSize:17.0f andFontName:@"BrandonGrotesque-Regular"];
    
    [ncell.newsArticleInnerCellView.source setText:art.source];
    // sort out number 10
    [ncell.newsArticleInnerCellView.rank setText:[NSString stringWithFormat:@"%d", [art.rank intValue]]];
    ncell.explanationViewOpenButton.tag = [art.rank intValue]-1;
    [ncell.newsArticleInnerCellView.vcircle animateCircleTo:art.score withDuration:0];
    [ncell.newsArticleInnerCellView.score setText:[NSString stringWithFormat:@"%.1f", [art.score floatValue]]];
    
    return ncell;
}

- (void)replaceData
{
    [super replaceData];
    
    // set arts array for when/if the explanationview is opened
    self.explanationView.arts = [NSMutableArray arrayWithArray:self.objects];
}

@end
