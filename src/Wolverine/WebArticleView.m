//
//  WebArticleView.m
//  BriefMe
//
//  Created by Charlie Vrettos on 6/23/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import "WebArticleView.h"
#import "Delegate.h"
#import "NavController.h"
#import "Localytics.h"
#import "ExtraNavigationBarView.h"
#import "BottomNavigationBarView.h"

@implementation WebArticleView

- (id)initWithFrame:(CGRect)frame andWithURL:(NSString *)url andWithNavBar:(BottomNavigationBarView *)navBar andWebConfig:(WKWebViewConfiguration *)webConfig
{
    self = [super initWithFrame:frame andWithNavBar:navBar andWithWebConfig:webConfig];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        _staticNav = NO;
        _goingToScrollToTop = NO;
        
        // check if need static navigation bar for this site
        NSURLComponents *urlComponents = [[NSURLComponents alloc] initWithString:url];
        NSString *host = [urlComponents.host stringByReplacingOccurrencesOfString:@"www." withString:@""];
        if ([Delegate sharedDel].staticNavSources && [[Delegate sharedDel].staticNavSources objectForKey:host]) {
            _staticNav = YES;
        }
        
        // add web navigation bar above regular navbar
        _webNavBar = [[ExtraNavigationBarView alloc] initWithFrame:CGRectMake(0, MAIN_HEIGHT-STATUS_BAR_HEIGHT-2*NAVIGATION_BAR_HEIGHT, MAIN_WIDTH, NAVIGATION_BAR_HEIGHT)];
        [self addSubview:_webNavBar];
    }
    return self;
}

+ (WKWebViewConfiguration *)createConfigWithScriptsForUrl:(NSURL *)url
{
    NSString *scrollKey = [NSString stringWithFormat:@"%@%d", SCROLL_KEY, WEB_ARTICLE_VIEW];
    return [self createConfigWithScriptsForUrl:url scroll:YES scrollKey:scrollKey openLink:NO openImage:NO loadedDOM:YES scale:YES nightMode:NO webBlocks:YES];
}

@end
