//
//  SectionViewController.m
//  MOST
//
//  Created by Hari Ganesan on 6/18/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import "SectionViewController.h"
#import "Delegate.h"
#import "HeaderView.h"
#import "TBKTabBarController.h"
#import "SettingsViewController.h"
#import "Localytics.h"

@implementation SectionViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"S E C T I O N S";
        
        // Create child controller for initial view so that we can navigate to secondary tab bar.
        _sectChild = [[UIViewController alloc] init];
        [self addChildViewController:_sectChild];
        [self.view addSubview:_sectChild.view];
        
        _sectChild.view.backgroundColor = [Delegate sharedDel].sectionBackgroundColor;

        HeaderView *hv = [[HeaderView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
        hv.title = [self.title uppercaseString];
        [_sectChild.view addSubview:hv];
             
        CGFloat boxHeight = 100*SCALEUP;
        CGFloat boxesPaddingTop = 70*SCALEUP;

        int base = 0;
        if (MAIN_HEIGHT > IPHONE_4_HEIGHT)
            base = 19*SCALEUP;
        
        [self addButtonAndLabelWithTitle:@"Politics" image:[UIImage imageNamed:@"politics.png"] highlightedImage:[UIImage imageNamed:@"politics-black-big.png"] frame:CGRectMake(0, base+boxesPaddingTop, MAIN_WIDTH/2, boxHeight) andTag:0];
        [self addButtonAndLabelWithTitle:@"World" image:[UIImage imageNamed:@"world.png"] highlightedImage:[UIImage imageNamed:@"world-black-big.png"] frame:CGRectMake(MAIN_WIDTH/2, base+boxesPaddingTop, MAIN_WIDTH/2, boxHeight) andTag:1];
        [self addButtonAndLabelWithTitle:@"Business" image:[UIImage imageNamed:@"business.png"] highlightedImage:[UIImage imageNamed:@"business-black-big.png"] frame:CGRectMake(0, base + MAIN_HEIGHT/4+boxesPaddingTop, MAIN_WIDTH/2, boxHeight) andTag:2];
        [self addButtonAndLabelWithTitle:@"Sports" image:[UIImage imageNamed:@"sports.png"] highlightedImage:[UIImage imageNamed:@"sports-black-big.png"] frame:CGRectMake(MAIN_WIDTH/2, base + MAIN_HEIGHT/4+boxesPaddingTop, MAIN_WIDTH/2, boxHeight) andTag:3];
        [self addButtonAndLabelWithTitle:@"Technology" image:[UIImage imageNamed:@"technology.png"] highlightedImage:[UIImage imageNamed:@"technology-black-big.png"] frame:CGRectMake(0, base + MAIN_HEIGHT/2+boxesPaddingTop, MAIN_WIDTH/2, boxHeight) andTag:4];
        [self addButtonAndLabelWithTitle:@"Entertainment" image:[UIImage imageNamed:@"entertainment.png"] highlightedImage:[UIImage imageNamed:@"entertainment-black-big.png"] frame:CGRectMake(MAIN_WIDTH/2, base + MAIN_HEIGHT/2+boxesPaddingTop, MAIN_WIDTH/2, boxHeight) andTag:5];
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)addButtonAndLabelWithTitle:(NSString *)title image:(UIImage *)image highlightedImage:(UIImage *)highlightedImage frame:(CGRect)frame andTag:(NSInteger)tag
{
    UIButton *boxView = [UIButton buttonWithType:UIButtonTypeCustom];
    boxView.frame = frame;
    boxView.userInteractionEnabled = YES;
    UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loadSecondaryView:)];
    boxView.tag = tag;
    [boxView addGestureRecognizer:gr];

    [boxView setImage:[Delegate imageWithImage:image scaledToSize:CGSizeMake(image.size.width/2, image.size.height/2)] forState:UIControlStateNormal];
    [boxView setImage:[Delegate imageWithImage:highlightedImage scaledToSize:CGSizeMake(highlightedImage.size.width/2, highlightedImage.size.height/2)] forState:UIControlStateHighlighted];
    boxView.titleLabel.font = [UIFont fontWithName:@"BrandonGrotesque-Regular" size:16];
    [boxView setTitle:title forState:UIControlStateNormal];
    [boxView setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [boxView setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];

    CGFloat spacing = 10.0;
    // lower the text and push it left so it appears centered
    //  below the image
    CGSize imageSize = boxView.imageView.frame.size;
    boxView.titleEdgeInsets = UIEdgeInsetsMake(0.0, - imageSize.width, - (imageSize.height + spacing), 0.0);
    
    // raise the image and push it right so it appears centered
    //  above the text
    CGSize titleSize = boxView.titleLabel.frame.size;
    boxView.imageEdgeInsets = UIEdgeInsetsMake(- (titleSize.height + spacing), 0.0, 0.0, - titleSize.width);

    [_sectChild.view addSubview:boxView];
}

- (void)loadSecondaryView:(UITapGestureRecognizer *)gr
{
    [Delegate sharedDel].tabsectvc.initialSectionSelected = (int)gr.view.tag;

    // add secondary view as a child and transition from other child
    [self addChildViewController:[Delegate sharedDel].tabsectvc];
    [self transitionFromViewController:_sectChild toViewController:[Delegate sharedDel].tabsectvc duration:0 options:UIViewAnimationOptionCurveLinear animations:nil completion:nil];
    
    // select specific section
    [[Delegate sharedDel].tabsectvc.tabBar didSelectTabBarItem:[Delegate sharedDel].tabsectvc.tabBar.items[gr.view.tag]];
    [[Delegate sharedDel].tabsectvc.tabBar.delegate tabBar:[Delegate sharedDel].tabsectvc.tabBar didSelectTabAtIndex:gr.view.tag];
}

- (void)resetView
{
    if ([[self childViewControllers] containsObject:[Delegate sharedDel].tabsectvc]) {
        [self transitionFromViewController:[Delegate sharedDel].tabsectvc toViewController:_sectChild duration:0 options:UIViewAnimationOptionCurveLinear animations:nil completion:nil];
        [[Delegate sharedDel].tabsectvc removeFromParentViewController];
    }
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
