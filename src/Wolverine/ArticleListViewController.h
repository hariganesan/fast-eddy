//
//  ArticleListViewController.h
//  MOST
//
//  Created by Hari Ganesan on 12/17/13.
//  Copyright (c) 2013 Campion Designs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Accelerate/Accelerate.h>
#import "Delegate.h"

@class UICircleView, ODRefreshControl, BlurrableImageView, TBKTabBarController, TutorialView, HeaderView, ExplanationView;

/** @brief Base class for all lists of articles. Always a bit of a mess. */
@interface ArticleListViewController : UITableViewController <UIGestureRecognizerDelegate>

/** @discussion The articles objects attached to the controller that are
    displayed within the tableview. */
@property (nonatomic, strong) NSMutableArray *objects;
@property (nonatomic, strong) NSMutableDictionary *expandedIndices;

/** @brief The section associated with the viewcontroller. */
@property (nonatomic, assign) SectionType section;
@property (nonatomic, assign) CGFloat lastContentOffset;
/** @brief Set to YES if the UITabBarController is in the view. */
@property (nonatomic, assign) BOOL navigationInView;

@property (nonatomic, strong) ODRefreshControl *customRefreshControl;
@property (nonatomic, strong) NSDate *refreshStart;
@property (nonatomic) BOOL initialParse;
@property (nonatomic) BOOL fakeScroll;
@property (nonatomic) BOOL animatingCell;
@property (nonatomic, strong) NSTimer *refreshTimer;
/** @description Set to YES once articles are parsed and reloadData is called. Used
    for waiting for both /top and /stream to load before injecting feedbackcell */
@property (nonatomic) BOOL articlesLoaded;

// subviews
/** @discussion The title and underline at the top of the view.
    Fades away on scroll. */
@property (nonatomic, strong) HeaderView *hv;
/** @discussion The UIImageView that contains the background image.
    The imageView holds one of a stack of blurred images that
    depends on the scroll offset. */
@property (nonatomic, strong) BlurrableImageView *bgImageView;

/** @brief A gradient that allows the title of the section to be visible. */
@property (nonatomic, strong) UIImageView *gradientView;
/** @brief A native replacement of the default tableview. */
@property (nonatomic, strong) UITableView *tableViewV2;
/** @discussion The explanation view which holds additonal article information
    accessible by tapping on the scores. Accessible by tapping on an invisible
    button within the cell view of the tableview. */
@property (nonatomic, strong) ExplanationView *explanationView;

- (id)initWithSection: (SectionType)section andTitle: (NSString *)title;
/** @brief A very, very complicated method. Probably not worth messing with. */
- (void)toggleCellExpansion:(UILongPressGestureRecognizer *)gr;
- (void)refresh;
- (void)replaceData;

/** @discussion The navigation bar shows when you scroll up a page.
 This function is called from the controller on top of the stack. */
- (void)showNavigation;
/** @discussion The navigation bar hides when you scroll down a page.
 This function is called from the controller on top of the stack. */
- (void)hideNavigation;

- (void)refreshTimes;

/** @brief Sets all the visible cells' view properties during a view update. */
- (void)resetCells;

@end
