//
//  NewsArticleCell.m
//  MOST
//
//  Created by Hari Ganesan on 6/20/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import "NewsArticle.h"
#import "NewsArticleCell.h"
#import "UICircleView.h"
#import "Delegate.h"
#import "NewsArticleInnerCellView.h"

@implementation NewsArticleCell

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:(NSString *)reuseIdentifier];
    if (self) {

        // dimView used for tutorial
        _dimView = [[UIView alloc] initWithFrame:CGRectMake(15*SCALEUP, 5*SCALEUP, (self.frame.size.width-30)*SCALEUP, 130*SCALEUP)];
        _dimView.layer.cornerRadius = 12;
        _dimView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
        _dimView.hidden = YES;
        [self.contentView addSubview:_dimView];
        
        _newsArticleInnerCellView = [[NewsArticleInnerCellView alloc] initWithFrame:self.frame];
        [self addSubview:_newsArticleInnerCellView];

        // add invisible button over circle to toggle dimview
        _explanationViewOpenButton = [[UIButton alloc] initWithFrame:_newsArticleInnerCellView.vcircleBackground.frame];
        _explanationViewOpenButton.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_explanationViewOpenButton];
    }
    return self;
}

// touch followed by a drag gesture (not selected)
- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    
    if (highlighted) {
        [_newsArticleInnerCellView.title setTextColor:[Delegate sharedDel].cellHighlightColor];
        [_newsArticleInnerCellView.rank setTextColor:[Delegate sharedDel].cellHighlightColor];
        [_newsArticleInnerCellView.score setTextColor:[Delegate sharedDel].cellHighlightColor];
        [_newsArticleInnerCellView.vcircleBackground.layer setBorderColor:[Delegate sharedDel].cellHighlightColor.CGColor];
        [_newsArticleInnerCellView.vcircle.circle setStrokeColor:[Delegate sharedDel].cellHighlightColor.CGColor];
        _newsArticleInnerCellView.vcircle.glowCircle.hidden = YES;
    } else {
        [self setVisited:self.visited animated:NO];
        [_newsArticleInnerCellView.title setTextColor:[Delegate sharedDel].cellTitleColor];
        [_newsArticleInnerCellView.rank setTextColor:[Delegate sharedDel].cellNumberColor];
        [_newsArticleInnerCellView.score setTextColor:[Delegate sharedDel].cellNumberColor];
        [_newsArticleInnerCellView.vcircleBackground.layer setBorderColor:[Delegate sharedDel].cellNumberColor.CGColor];
        [_newsArticleInnerCellView.vcircle.circle setStrokeColor:[Delegate sharedDel].cellNumberColor.CGColor];
        _newsArticleInnerCellView.vcircle.glowCircle.hidden = NO;
    }
}

@end
