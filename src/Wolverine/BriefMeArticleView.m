//
//  BriefMeView.m
//  BriefMe
//
//  Created by Charlie Vrettos on 6/23/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import "BriefMeArticleView.h"
#import "Delegate.h"
#import "NavController.h"
#import "BottomNavigationBarView.h"
#import "SettingsViewController.h"
#import "ArticleInnerCellView.h"
#import "Localytics.h"

@interface BriefMeArticleView ()
@property (nonatomic) float pinchLevel;
@property (nonatomic) float zoom;
@end

@implementation BriefMeArticleView

- (id)initWithFrame:(CGRect)frame andWithHeader:(ArticleInnerCellView *)header andWithNavBar:(BottomNavigationBarView *)navBar andWebConfig:(WKWebViewConfiguration *)webConfig
{
    self = [super initWithFrame:frame andWithNavBar:navBar andWithWebConfig:webConfig];
    if (self) {
        _header = header;
        [self.scrollView addSubview:_header];
        [self.scrollView setShowsVerticalScrollIndicator:NO];
        _pinchLevel = 1.0f; // 1 because initial pgr.scale is 1
        _zoom = 1.0f;
        
        // find browser subview, move its origin.y down, and insert header above it
        // moving origin.y down instead of setting insets fixes all css/js that is top aligned (all sticky headers like nyt, national journal, ...)
        //  rather than having it appear (_header's height) pixels down the screen.. which would all have to be blocked with webBlocks
        CGRect browserCanvas = self.bounds;
        for(int i=0; i< [[self.scrollView subviews] count]; i++) {
            // find the biggest view which contains the browser
            UIView* subview = [[self.scrollView subviews] objectAtIndex:i];
            CGRect f = subview.frame;
            if(f.origin.x == browserCanvas.origin.x && f.origin.y == browserCanvas.origin.y && f.size.width == browserCanvas.size.width && f.size.height == browserCanvas.size.height) {
                f.origin.y = _header.frame.size.height;
                subview.frame = f;
            }
        }
        
        _horizontalScrollBar = [[UIView alloc] initWithFrame:CGRectMake(0, STATUS_BAR_HEIGHT-2, 0, 2)];
        [_horizontalScrollBar setBackgroundColor:[Delegate sharedDel].articleScrollBarColor];
        [self.statusBarBackground addSubview:_horizontalScrollBar];
        
        UIPinchGestureRecognizer *pgr = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinch:)];
        [self addGestureRecognizer:pgr];
    }
    return self;
}

- (void)pinch:(UIPinchGestureRecognizer *)pgr
{
    // round pgr.scale to 0.1
    float num = pgr.scale + 0.05;
    int intNum = num*10;
    float calculatedPinchLevel = intNum/10.0;
    
    // zoom briefmeview each time the pgr.scale increases by 0.1
    if (calculatedPinchLevel > _pinchLevel) {
        _pinchLevel = calculatedPinchLevel;
        [self increaseFontSize];
    } else if (calculatedPinchLevel < _pinchLevel) {
        _pinchLevel = calculatedPinchLevel;
        [self decreaseFontSize];
    }
    
    // reset pinchLevel on gesture end because pgr.scale resets
    if ([pgr state] == UIGestureRecognizerStateEnded) {
        _pinchLevel = 1.0; // 1 because initial pgr.scale is 1
    }
}

- (void)moveScrollBar
{
    // Animate custom scroll bar
    // TODO(charlie): handle when contentsize = main_height or just height of header
    // TODO(charlie): maybe calculate maxScroll elsewhere and set it? if for some reason
    //  the contentsize changed (maybe loading imgs?), the progress bar would be jumpy and buggy
    float maxScroll = self.scrollView.contentSize.height-MAIN_HEIGHT+self.scrollView.contentInset.bottom;
    float maxProgressWidth = MAIN_WIDTH-_horizontalScrollBar.frame.origin.x;
    float percentScrolled = self.scrollView.contentOffset.y / maxScroll;
    float scrollDistance = percentScrolled * maxProgressWidth;
    if (maxScroll > 0 && percentScrolled >= 0) {
        float new_width = scrollDistance - _horizontalScrollBar.frame.origin.x;
        _horizontalScrollBar.frame = CGRectMake(_horizontalScrollBar.frame.origin.x, _horizontalScrollBar.frame.origin.y, new_width, _horizontalScrollBar.frame.size.height);
        if (new_width >= maxProgressWidth) {
            [UIView animateWithDuration:0.5 animations:^{
                [_horizontalScrollBar setBackgroundColor:[Delegate sharedDel].articleScrollBarFinishColor];
            }];
        } else if (new_width < maxProgressWidth && [_horizontalScrollBar.backgroundColor isEqual:[Delegate sharedDel].articleScrollBarFinishColor]) {
            [UIView animateWithDuration:0.5 animations:^{
                [_horizontalScrollBar setBackgroundColor:[Delegate sharedDel].articleScrollBarColor];
            }];
        }
    } else {
        _horizontalScrollBar.frame = CGRectMake(_horizontalScrollBar.frame.origin.x, _horizontalScrollBar.frame.origin.y, 0, _horizontalScrollBar.frame.size.height);
    }
}

- (void)increaseFontSize
{
    _zoom = MIN(_zoom+0.05f, 2.0f);
    [self evaluateJavaScript:[NSString stringWithFormat:@"document.body.style.zoom = %f", _zoom] completionHandler:nil];
}

- (void)decreaseFontSize
{
    _zoom = MAX(_zoom-0.05f, 1.0f);
    [self evaluateJavaScript:[NSString stringWithFormat:@"document.body.style.zoom = %f", _zoom] completionHandler:nil];
}

+ (WKWebViewConfiguration *)createConfigWithScriptsForUrl:(NSURL *)url
{
    NSString *scrollKey = [NSString stringWithFormat:@"%@%d", SCROLL_KEY, BRIEFME_ARTICLE_VIEW];
    return [self createConfigWithScriptsForUrl:url scroll:YES scrollKey:scrollKey openLink:YES openImage:YES loadedDOM:NO scale:NO nightMode:YES webBlocks:NO];
}

@end
