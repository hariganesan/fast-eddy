//
//  InfoViewController.m
//  BriefMe
//
//  Created by Charlie Vrettos on 3/30/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import "InfoViewController.h"
#import "HeaderView.h"
#import "BottomNavigationBarView.h"
#import "Delegate.h"

@implementation InfoViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.view.backgroundColor = [UIColor whiteColor];
        _lastContentOffset = 0.0f;
        
        _sv = [[UIScrollView alloc] initWithFrame:self.view.frame];
        _sv.backgroundColor = [UIColor whiteColor];
        _sv.bounces = YES;
        [self.view addSubview:_sv];
        _sv.delegate = self;
        
        _hv = [[HeaderView alloc] initWithFrame:CGRectMake(0, 0, MAIN_WIDTH, 100)];
        _hv.headerLabel.textColor = [UIColor blackColor];
        _hv.whiteLine.backgroundColor = [UIColor blackColor];
        [_sv addSubview:_hv];
        
        self.bar = [[BottomNavigationBarView alloc] initWithType:BottomNavigationBarTypeLogo];
        _bar.backgroundColor = [Delegate sharedDel].sectionBackgroundColor;
        _bar.shareButtonImageView.hidden = YES;
        _bar.shareButton.hidden = YES;
        [self.view addSubview:_bar];
    }
    return self;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // Animate navbar
    ScrollDirection scrollDirection = ScrollDirectionNone;
    if (_lastContentOffset < scrollView.contentOffset.y) {
        scrollDirection = ScrollDirectionDown;
    } else if (_lastContentOffset > scrollView.contentOffset.y) {
        scrollDirection = ScrollDirectionUp;
    }
    
    _lastContentOffset = scrollView.contentOffset.y;

    // show/hide navigation items
    if (scrollDirection == ScrollDirectionUp || scrollView.contentOffset.y <= 0) {
        [_bar show];
    } else if (scrollDirection == ScrollDirectionDown) {
        [_bar hide];
    }
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
