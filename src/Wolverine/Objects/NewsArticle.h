//
//  NewsArticle.h
//  MOST
//
//  Created by Todd Lubin on 6/29/12.
//  Copyright (c) 2012 Campion Designs, LLC. All rights reserved.
//

#import "Article.h"

/** @brief Model object that represents articles in Briefing and Sections (not Stream) */
@interface NewsArticle : Article

/** @brief 1-based number that represents that article's rank. */
@property (nonatomic, strong) NSNumber *rank;

/** @brief property that pertain to the BriefMe stats of the article. */
@property (nonatomic, copy) NSNumber *score;
/** @brief property that pertain to the BriefMe stats of the article. */
@property (nonatomic, copy) NSNumber *facebook;
/** @brief property that pertain to the BriefMe stats of the article. */
@property (nonatomic, copy) NSNumber *twitter;
/** @brief property that pertain to the BriefMe stats of the article. */
@property (nonatomic, copy) NSNumber *virality;

/** @brief Initialize the article given a dictionary. */
- (id)initWithDictionary:(NSDictionary *)dict;

/** @brief Get all properties within the model to return to Localytics. */
- (NSDictionary *)getAttributes;

@end
