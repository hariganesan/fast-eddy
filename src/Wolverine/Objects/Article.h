//
//  Article.h
//  MOST
//
//  Created by Hari Ganesan on 6/10/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import "Delegate.h"

/** @brief Base class for all models that include data for an article. */
@interface Article : NSObject

@property (nonatomic) SectionType section;

/** @brief The age of the article in minutes. */
@property (nonatomic, strong) NSNumber *age;
/** @discussion The displayed age of the article. 
    May be 0-60 if in minutes, or 0-48 if in hours. */
@property (nonatomic, strong) NSNumber *displayedAge;

/** @brief Units for the displayed age (minutes/hours). */
@property (nonatomic, strong) NSString *ageUnits;
/** @brief Published date of the article. */
@property (nonatomic, strong) NSDate *published;

/** @discussion article info from RSS. Includes:
    url of the article
    title of the article (<= 170 chars)
    news source of the article e.g. CNN, New York Times
    a small description/summary of the article, taken either from the RSS directly or from Facebook. (> 10 chars)
*/
@property (nonatomic, strong) NSString *url;
/** @brief The news article's title. */
@property (nonatomic, strong) NSString *title;
/** @brief The news article's summary/description. */
@property (nonatomic, strong) NSString *snippet;
/** @brief The news source of the article. */
@property (nonatomic, strong) NSString *source;
/** @brief The normalized html of the article. */
@property (nonatomic, strong) NSString *content;
/** @discussion YES if _content contains all styling required ("<html><head>..." etc). This
    prevents use of styleBefore and styleAfter for the article */
@property (nonatomic) BOOL customStyling;

/** @brief Shortened URL that is used sometimes in sharing. For stream articles, it is set to the url. */
@property (nonatomic, strong) NSString *shareUrl;

/** @brief Set to YES if the article was expanded during the same session. */
@property (nonatomic, assign) BOOL expanded;
/** @brief Set to YES if BriefMe View was viewed during article view */
@property (nonatomic, assign) BOOL briefmeViewViewed;

/** @brief Initialize the article given a dictionary. */
- (id)initWithDictionary:(NSDictionary *)dict;

/** @brief Get all properties within the model to return to Localytics. */
- (NSDictionary *)getAttributes;

@end
