//
//  HeaderView.h
//  MOST
//
//  Created by Hari Ganesan on 6/13/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @brief  Text at the top of a section with the underline (Briefing, Politics, Stream) */
@interface HeaderView : UIView

// TODO(hari): remove model data from the header view
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UILabel *headerLabel;
@property (nonatomic, strong) UIView *whiteLine;

- (void)changeTitleFontSize:(int)fontSize;
- (void)animateDisappear:(float)offset;

@end
