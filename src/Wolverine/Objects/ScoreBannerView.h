//
//  ScoreBannerView.h
//  MOST
//
//  Created by Charlie Vrettos on 7/18/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICountingLabel.h"
#import "NewsArticle.h"

@interface ScoreBannerView : UIView

@property (nonatomic, strong) UIView *topBannerView;
@property (nonatomic, strong) UIView *breakDownView;
@property (nonatomic, strong) UIView *topBannerTriangleContainer;
@property (nonatomic, strong) UICountingLabel *countingLabel;
@property (nonatomic, strong) NewsArticle *newsArt;
@property (nonatomic, strong) NSArray *vscores;
@property (nonatomic) BOOL isAnimatingBreakdown;
@property (nonatomic, strong) UIButton *scoreButton;
@property (nonatomic, strong) UIView *triangle;

- (id)initWithFrame:(CGRect)frame andWithArticle:(NewsArticle *)art;
- (void)animateBreakDown;
- (void)drawCircles;

@end
