//
//  ScoreBannerView.m
//  MOST
//
//  Created by Charlie Vrettos on 7/18/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import "ScoreBannerView.h"
#import "UICircleView.h"
#import "UICountingLabel.h"
#import "Delegate.h"
#import "Localytics.h"

@implementation ScoreBannerView

- (id)initWithFrame:(CGRect)frame andWithArticle:(NewsArticle *)art
{
    self = [super initWithFrame:frame]; //frame of just the topBannerView for transitionView; top+breakdown for explanationview
    if (self) {
        _newsArt = art;
        _isAnimatingBreakdown = NO;
        _vscores = [[NSArray alloc] init];
        
        _topBannerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAIN_WIDTH, 100*SCALEUP)];
        _topBannerView.backgroundColor = [UIColor blackColor];
        
        _topBannerTriangleContainer = [[UIView alloc] initWithFrame:CGRectMake((160-18)*SCALEUP, -20*SCALEUP, 36*SCALEUP, 36*SCALEUP)];
        _topBannerTriangleContainer.backgroundColor = [UIColor clearColor];
        
        UIView *triangle = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 36*SCALEUP, 36*SCALEUP)];
        triangle.backgroundColor = [UIColor blackColor];
        triangle.transform = CGAffineTransformMakeRotation(M_PI*0.25);
        
        [_topBannerTriangleContainer addSubview:triangle];
        triangle.backgroundColor = [UIColor blackColor];
        
        // negative y coordinate: offscreen for animation
        _breakDownView = [[UIView alloc] initWithFrame:CGRectMake(0, -200*SCALEUP, MAIN_WIDTH, 180*SCALEUP)];
        _breakDownView.backgroundColor = [Delegate sharedDel].cellAlternateTransparentColor;
        
        [self addSubview:_breakDownView];
        [_breakDownView addSubview:_topBannerTriangleContainer];
        [self addSubview:_topBannerView];
        
        //TOP BANNER LABELS AND CONTENT
        // labels for virality top
        NSArray *vtoplabels = @[[[UILabel alloc] initWithFrame:CGRectMake(10*SCALEUP, 12*SCALEUP, 100*SCALEUP, 15*SCALEUP)],
                                [[UILabel alloc] initWithFrame:CGRectMake(110*SCALEUP, 12*SCALEUP, 100*SCALEUP, 15*SCALEUP)],
                                [[UILabel alloc] initWithFrame:CGRectMake(214*SCALEUP, 12*SCALEUP, 100*SCALEUP, 15*SCALEUP)]];
        [vtoplabels[0] setText:@"RANK"];
        [vtoplabels[1] setText:@"BRIEFME SCORE"];
        [vtoplabels[2] setText:@"TIME"];
        
        for (UILabel *item in vtoplabels) {
            [item setTextColor:[UIColor whiteColor]];
            [item setFont:[UIFont fontWithName:@"BrandonGrotesque-Bold" size:11*SCALEUP]];
            [item setTextAlignment: NSTextAlignmentCenter];
            [item setNumberOfLines:0];
            [_topBannerView addSubview:item];
        }
        
        // labels for mins/hours ago
        UILabel *timeAgo = [[UILabel alloc] initWithFrame:CGRectMake(214*SCALEUP, 64*SCALEUP, 100*SCALEUP, 30*SCALEUP)];
        timeAgo.text = art.ageUnits;
        [timeAgo setTextColor:[UIColor whiteColor]];
        [timeAgo setFont:[UIFont fontWithName:@"BrandonGrotesque-Bold" size:14*SCALEUP]];
        [timeAgo setTextAlignment: NSTextAlignmentCenter];
        [timeAgo setNumberOfLines:0];
        [_topBannerView addSubview:timeAgo];
        
        // labels for timenumber
        UILabel *timeNumber = [[UILabel alloc] initWithFrame:CGRectMake(234*SCALEUP, 20*SCALEUP, 60*SCALEUP, 60*SCALEUP)];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"EEE MMM dd HH:mm:ss ZZZ yyyy"];
        timeNumber.text = [NSString stringWithFormat:@"%li", (long)[art.displayedAge integerValue]];
        
        [timeNumber setTextColor:[UIColor whiteColor]];
        [timeNumber setFont:[UIFont fontWithName:@"BrandonGrotesque-Bold" size:28]];
        [timeNumber setTextAlignment: NSTextAlignmentCenter];
        [timeNumber setNumberOfLines:0];
        [_topBannerView addSubview:timeNumber];
        
        // labels for viralityrank
        UILabel *viralityRank = [[UILabel alloc] initWithFrame:CGRectMake(10*SCALEUP, 12*SCALEUP, 100*SCALEUP, 100*SCALEUP)];
        viralityRank.text = [NSString stringWithFormat:@"%@", art.rank];
        [viralityRank setTextColor:[UIColor whiteColor]];
        [viralityRank setFont:[UIFont fontWithName:@"BrandonGrotesque-Bold" size:62]];
        [viralityRank setTextAlignment: NSTextAlignmentCenter];
        [viralityRank setNumberOfLines:0];
        [_topBannerView addSubview:viralityRank];
        
        // circle for virality score
        UICircleView *viralityScore = [[UICircleView alloc] initWithFrame:CGRectMake(135*SCALEUP, 36*SCALEUP, 50*SCALEUP, 50*SCALEUP) andColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:1]];
        viralityScore.layer.borderColor = [UIColor whiteColor].CGColor;
        viralityScore.layer.borderWidth = 0.5f;
        viralityScore.layer.cornerRadius = 25*SCALEUP;
        //viralityScore.circle = [CAShapeLayer layer];
        viralityScore.circle.fillColor = [UIColor clearColor].CGColor;
        viralityScore.circle.borderColor = [UIColor clearColor].CGColor;
        viralityScore.circle.strokeColor = [UIColor whiteColor].CGColor;
        viralityScore.circle.lineWidth = 2;
        
        viralityScore.glowCircle.fillColor = [UIColor clearColor].CGColor;
        viralityScore.glowCircle.borderColor = [UIColor clearColor].CGColor;
        viralityScore.glowCircle.strokeColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:.2].CGColor;
        viralityScore.glowCircle.lineWidth = 5.5;

        // count up from 0 to the virality score
        UICountingLabel* attributedLabel = [[UICountingLabel alloc] initWithFrame:CGRectMake(0, 0, viralityScore.frame.size.width, viralityScore.frame.size.height)];
        [_topBannerView addSubview:attributedLabel];
        attributedLabel.attributedFormatBlock = ^NSAttributedString* (float value)
        {
            NSDictionary* highlight = @{ NSFontAttributeName: [UIFont fontWithName:@"BrandonGrotesque-Bold" size:14] };
            NSString* scoreText = [NSString stringWithFormat:@"%.1f", value];
            NSMutableAttributedString* scoreTextAttributed = [[NSMutableAttributedString alloc] initWithString: scoreText
                                                                                                    attributes: highlight];
            return scoreTextAttributed;
        };
        attributedLabel.textColor = [UIColor whiteColor];
        attributedLabel.textAlignment = NSTextAlignmentCenter;
        attributedLabel.frame = viralityScore.frame;
//        [viralityScore addSubview:attributedLabel];
        [_topBannerView.layer addSublayer:viralityScore.layer];

        [attributedLabel countFrom:0 to:[art.score floatValue] withDuration:2.5];

        _countingLabel.method = UILabelCountingMethodEaseInOut;
        _countingLabel.format = @"%.1f";
        __weak ScoreBannerView *blockSelf = self;
        _countingLabel.completionBlock = ^{
            blockSelf.countingLabel.textColor = [UIColor colorWithRed:0 green:0.5 blue:0 alpha:1];
        };
        [_countingLabel countFrom:0 to:[art.score floatValue]];

        // animate virality score circle
        viralityScore.circle.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 2.0*(viralityScore.layer.cornerRadius), 2.0*(viralityScore.layer.cornerRadius))
                                                        cornerRadius:(viralityScore.layer.cornerRadius)].CGPath;
        viralityScore.circle.position = CGPointMake(viralityScore.frame.origin.x, viralityScore.frame.origin.y);
        
        viralityScore.glowCircle.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 2.0*(viralityScore.layer.cornerRadius), 2.0*(viralityScore.layer.cornerRadius))
                                                              cornerRadius:(viralityScore.layer.cornerRadius)].CGPath;
        viralityScore.glowCircle.position = CGPointMake(viralityScore.frame.origin.x, viralityScore.frame.origin.y);
        viralityScore.glowCircle.fillColor = [UIColor clearColor].CGColor;
        viralityScore.glowCircle.borderColor = [UIColor clearColor].CGColor;
        
        [_topBannerView.layer addSublayer:viralityScore.circle];
        [_topBannerView.layer addSublayer:viralityScore.glowCircle];
        [viralityScore animateCircleTo:[NSNumber numberWithFloat:[art.score floatValue]] withDuration:[NSNumber numberWithFloat:2.5]];
        
        //BREAKDOWN LABELS AND CONTENT
        [self drawCircles];

        // labels for virality breakdown
        NSArray *vdescs = @[[[UILabel alloc] initWithFrame:CGRectMake(24*SCALEUP, 72*SCALEUP, 80*SCALEUP, 50*SCALEUP)],
                            [[UILabel alloc] initWithFrame:CGRectMake(122*SCALEUP, 72*SCALEUP, 80*SCALEUP, 50*SCALEUP)],
                            [[UILabel alloc] initWithFrame:CGRectMake(218*SCALEUP, 72*SCALEUP, 80*SCALEUP, 50*SCALEUP)]];


        [vdescs[0] setText:@"VIRALITY"];
        [vdescs[1] setText:@"FACEBOOK"];
        [vdescs[2] setText:@"TWITTER"];

        for (UILabel *vdesc in vdescs) {
            [vdesc setTextColor:[UIColor whiteColor]];
            [vdesc setFont:[UIFont fontWithName:@"BrandonGrotesque-Bold" size:14]];
            [vdesc setTextAlignment: NSTextAlignmentCenter];
            [vdesc setNumberOfLines:0];
            [_breakDownView addSubview:vdesc];
        }

        // labels for breakdown overview
        NSArray *voverview = @[[[UILabel alloc] initWithFrame:CGRectMake(20*SCALEUP, 100*SCALEUP, 90*SCALEUP, 60*SCALEUP)],
                            [[UILabel alloc] initWithFrame:CGRectMake(117*SCALEUP, 100*SCALEUP, 90*SCALEUP, 60*SCALEUP)],
                            [[UILabel alloc] initWithFrame:CGRectMake(212*SCALEUP, 100*SCALEUP, 90*SCALEUP, 60*SCALEUP)]];


        [voverview[0] setText:@"topic trending / time of publication"];
        [voverview[1] setText:@"likes, shares & comments"];
        [voverview[2] setText:@"tweets, retweets and starred"];

        for (UILabel *item in voverview) {
            [item setTextColor:[UIColor whiteColor]];
            [item setFont:[UIFont fontWithName:@"BrandonGrotesque-Bold" size:12*SCALEUP]];
            [item setTextAlignment: NSTextAlignmentCenter];
            [item setNumberOfLines:0];
            [self.breakDownView addSubview:item];
        }
        
        _scoreButton = [[UIButton alloc] initWithFrame:viralityScore.frame];
        [_scoreButton addTarget:self action:@selector(animateBreakDown) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_scoreButton];
    
        
        //initially hide breakdown
        _breakDownView.hidden = YES;
        //_topBannerTriangle.hidden = YES;
    }
    return self;
}

- (void)animateBreakDown
{
    if (_isAnimatingBreakdown)
        return;
    _isAnimatingBreakdown = YES;

    // GROW ANIMATION
    if (_breakDownView.hidden) {
        NSArray *vbNumbers = @[_newsArt.virality, _newsArt.facebook, _newsArt.twitter];

        int scoreIndex = 0;
        for (UICircleView *vscore in _vscores) {
            [vscore animateCircleTo:(NSNumber *)vbNumbers[scoreIndex] withDuration:[NSNumber numberWithFloat:2.5]];
            [vscore.attributedLabel countFrom:0 to:[vbNumbers[scoreIndex] floatValue] withDuration:2.5];
            scoreIndex++;
        }
        
        _breakDownView.hidden = NO;
        _topBannerTriangleContainer.hidden = NO;
        [UIView animateWithDuration:0.7 animations:^{
            _breakDownView.frame = CGRectMake(0, 100*SCALEUP, MAIN_WIDTH, 180*SCALEUP);
            //_scoreView.topBannerTriangleContainer.frame = CGRectMake(160-18, 80, 36, 36);
        }  completion:^(BOOL finished) {
            _isAnimatingBreakdown = NO;
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height + _breakDownView.frame.size.height);
            
            //localytics: log event; note: this will always occur once during tutorial
            [Localytics tagEvent:@"Breakdown Viewed"];
            _isAnimatingBreakdown = NO;
        }];
    // SHRINK ANIMATION
    } else {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height - _breakDownView.frame.size.height);
        [UIView animateWithDuration:0.7 animations:^{
            self.breakDownView.frame = CGRectMake(0, -200*SCALEUP, MAIN_WIDTH, 180*SCALEUP);
            //_scoreView.topBannerTriangleContainer.frame = CGRectMake(160-18, 50, 36, 36);
        }  completion:^(BOOL finished) {
            _isAnimatingBreakdown = NO;
            self.breakDownView.hidden = YES;
            self.topBannerTriangleContainer.hidden = YES;
            _isAnimatingBreakdown = NO;
        }];
    }
}

- (void)drawCircles
{
    // three virality circles
    _vscores = @[[[UICircleView alloc] initWithFrame:CGRectMake(40*SCALEUP, 32*SCALEUP, 44*SCALEUP, 44*SCALEUP) andColor:[UIColor clearColor]],
                         [[UICircleView alloc] initWithFrame:CGRectMake(138*SCALEUP, 32*SCALEUP, 44*SCALEUP, 44*SCALEUP) andColor:[UIColor clearColor]],
                         [[UICircleView alloc] initWithFrame:CGRectMake(236*SCALEUP, 32*SCALEUP, 44*SCALEUP, 44*SCALEUP) andColor:[UIColor clearColor]]];
    
    
    NSArray *vbNumbers = @[_newsArt.virality, _newsArt.facebook, _newsArt.twitter];
    
    [_vscores enumerateObjectsUsingBlock:^(UICircleView *vscore, NSUInteger index, BOOL *stop) {
        vscore.layer.borderColor = [UIColor whiteColor].CGColor;
        vscore.layer.borderWidth = 0.5f;
        vscore.layer.cornerRadius = 22*SCALEUP;
        
        // Set up the shape of the circle
        float radius = 22*SCALEUP;
        //vscore.circle = [CAShapeLayer layer];
        // Make a circular shape
        vscore.circle.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 2.0*radius, 2.0*radius)
                                                 cornerRadius:radius].CGPath;
        // Center the shape in self.view
        vscore.circle.position = CGPointMake(vscore.frame.origin.x, vscore.frame.origin.y);
        
        // Configure the appearence of the circle
        vscore.circle.fillColor = [UIColor clearColor].CGColor;
        vscore.circle.borderColor = [UIColor clearColor].CGColor;
        vscore.circle.strokeColor = [UIColor whiteColor].CGColor;
        vscore.circle.lineWidth = 2;
        
        
        vscore.glowCircle.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 2.0*radius, 2.0*radius)
                                                            cornerRadius:radius].CGPath;
        vscore.glowCircle.position = CGPointMake(vscore.frame.origin.x, vscore.frame.origin.y);
        vscore.glowCircle.fillColor = [UIColor clearColor].CGColor;
        vscore.glowCircle.borderColor = [UIColor clearColor].CGColor;
        vscore.glowCircle.strokeColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:.3].CGColor;
        vscore.glowCircle.lineWidth = 4;
        
        // Add to parent layer
        [_breakDownView.layer addSublayer:vscore.layer];
        [_breakDownView.layer addSublayer:vscore.circle];
        [_breakDownView.layer addSublayer:vscore.glowCircle];
        
        [vscore animateCircleTo:(NSNumber *)vbNumbers[index] withDuration:[NSNumber numberWithFloat:2.5]];
        
        // count up from 0 to the score
        vscore.attributedLabel = [[UICountingLabel alloc] initWithFrame:CGRectMake(0, 0, vscore.frame.size.width, vscore.frame.size.height)];
        [_breakDownView addSubview:vscore.attributedLabel];
        vscore.attributedLabel.attributedFormatBlock = ^NSAttributedString* (float value)
        {
            NSDictionary* highlight = @{ NSFontAttributeName: [UIFont fontWithName:@"BrandonGrotesque-Bold" size:14*(1 + (SCALEUP - 1)/2)] };
            NSString* scoreText = [NSString stringWithFormat:@"%.1f", value];
            NSMutableAttributedString* scoreTextAttributed = [[NSMutableAttributedString alloc] initWithString: scoreText
                                                                                                    attributes: highlight];
            return scoreTextAttributed;
        };
        
        vscore.attributedLabel.frame = vscore.frame;
        vscore.attributedLabel.textColor = [UIColor whiteColor];
        vscore.attributedLabel.textAlignment = NSTextAlignmentCenter;
        [_breakDownView.layer addSublayer:vscore.attributedLabel.layer];
//        [_breakDownView addSubview:vscore];
        
        [vscore.attributedLabel countFrom:0 to:[vbNumbers[index] floatValue] withDuration:2.5];
//
//        _countingLabel.method = UILabelCountingMethodEaseInOut;
//        _countingLabel.format = @"%.1f";
//        __weak ScoreBannerView* blockSelf = self;
//        _countingLabel.completionBlock = ^{
//            blockSelf.countingLabel.textColor = [UIColor colorWithRed:0 green:0.5 blue:0 alpha:1];
//        };
//        [_countingLabel countFrom:0 to:[vbNumbers[index] floatValue]];
        
        //[self performSelectorInBackground:@selector(smoothIncreaseNumber:) withObject:vscoreLabel];
    }];
}

@end
