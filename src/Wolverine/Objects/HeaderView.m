//
//  HeaderView.m
//  MOST
//
//  Created by Hari Ganesan on 6/13/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import "HeaderView.h"
#import "Delegate.h"

@implementation HeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 70)];
        _title = @"";
        _whiteLine = [[UIView alloc] initWithFrame:CGRectMake(0,0,0,0)]; // changed in setTitle
        _whiteLine.backgroundColor = [UIColor whiteColor];
        _headerLabel.textColor = [UIColor whiteColor];
        
        [self addSubview:_headerLabel];
        [self addSubview:_whiteLine];
    }
    return self;
}

// TODO(hari): best not to override getters and setters
// note that self.title = title would not work here
- (void) setTitle:(NSString *)title
{
    _title = title;
    [self changeTitleFontSize:13];
}

- (void)changeTitleFontSize:(int)fontSize
{
    NSAttributedString *attributedString =
    [[NSAttributedString alloc]
     initWithString: [_title uppercaseString]
     attributes:
     @{
       NSFontAttributeName : [UIFont fontWithName:@"BrandonGrotesque-Bold" size:fontSize],
       NSKernAttributeName : @(0.4f)
       }];
    
    [_headerLabel setAttributedText:attributedString];
    [_headerLabel setTextAlignment:NSTextAlignmentCenter];    
    
    self.userInteractionEnabled = NO;
    
    // Width of string + width of kerning * num characters
    float titleWidth = 0.5 * ([_title sizeWithAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"BrandonGrotesque-Bold" size:fontSize]}].width + (0.4 * _title.length));
    _whiteLine.frame = CGRectMake(MAIN_WIDTH/2-(titleWidth/2) - 1, 48, titleWidth, 1.5); // 1 px shift left for some reason
}

- (void)animateDisappear:(float)offset
{
    // fade headerview
    float fadeIndex = lround(offset / 7);
    float newOpacity;
    if (fadeIndex <= 0)
        newOpacity = 1;
    else {
        newOpacity = 1 - fadeIndex/30;
        if (newOpacity < 0)
            newOpacity = 0;
    }
    self.layer.opacity = newOpacity;
    
    // shift headerview down if user is pulling to refresh
    if (offset < 0) {
        self.frame = CGRectMake(self.frame.origin.x, 0 - offset, self.frame.size.width, self.frame.size.height);
    } else {
        // parallax headerview
        float parallaxIndex =  lround(offset / 14);
        self.frame = CGRectMake(self.frame.origin.x, 0 - parallaxIndex/2, self.frame.size.width, self.frame.size.height);
    }
}

@end
