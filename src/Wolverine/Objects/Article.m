//
//  Article.m
//  MOST
//
//  Created by Hari Ganesan on 6/10/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import "Article.h"

@implementation Article

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        _source = [dict objectForKey:@"source"];
        _title = [dict objectForKey:@"title"];
        _age = [dict objectForKey:@"age"];
        if ([_title length] >= 155)
            _title = [NSString stringWithFormat:@"%@...", [_title substringToIndex:155]];
        
        _url = [dict objectForKey:@"url"];
        _shareUrl = _url;
        _snippet = [dict objectForKey:@"description"];
        _source = [dict objectForKey:@"source"];
        if ([dict objectForKey:@"content"]) {
            _content = [dict objectForKey:@"content"];
        } else {
            _content = @"???";
        }
        if ([dict objectForKey:@"custom_styling"] && [(NSNumber *)[dict objectForKey:@"custom_styling"] isEqualToNumber:[NSNumber numberWithBool:YES]]) {
            _customStyling = YES;
        } else {
            _customStyling = NO;
        }
        
        if ([_snippet length] < 10) {
            _snippet = @"No summary available";
        }

        _expanded = NO;
        _briefmeViewViewed = NO;

        // TODO(hari): remove temporary fix
        if ([dict objectForKey:@"published"] == [NSNull null]) {
            self.published = [NSDate date];
        } else {
            // parse published date into an nsdate for _published
            NSString *publishedStr = [dict objectForKey:@"published"];
            
            // a good way to test these results is https://www.wolframalpha.com/input/?i=%28time+since+Tue%2C+22+Jul+2014+16%3A49%3A53+GMT%29+to+minutes
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [dateFormatter setLocale:locale];
            [dateFormatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss z"];
            
            // try most popular date format
            NSDate *pubDate = [dateFormatter dateFromString:publishedStr];
            if (pubDate == nil) {
                // try another format
                [dateFormatter setDateFormat:@"EEE, dd MMM yyyy HH:mm z"];
                pubDate = [dateFormatter dateFromString:publishedStr];
            }
            if (pubDate == nil) {
                // try another format
                [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
                pubDate = [dateFormatter dateFromString:publishedStr];
            }
            
            // if couldnt parse published date, use the articles 'age' from the json
            //  along with the date the json was updated to make an approximate nsdate
            if (pubDate == nil) {
                NSDate *routeUpdated = [dict objectForKey:@"routeUpdated"];
                self.published = [routeUpdated dateByAddingTimeInterval:-60*[[dict objectForKey:@"age"] integerValue]];
                NSLog(@"FAILURE: the date: %@ from '%@' could not be parsed; estimated it as %@", publishedStr, self.title, self.published);
            } else {
                //NSLog(@"SUCCESS: the date: %@ has been parsed; %f minutes since.", publishedStr, -1 * [pubDate timeIntervalSinceNow] / 60);
                self.published = pubDate;
            }
        }
    }
    return self;
}

- (NSDictionary *)getAttributes
{
    if (self.section == SECTION_WINNERS) {
        return @{
                 @"title": self.title,
                 @"source": self.source,
                 @"age": self.age,
                 @"section": @"winners"
        };
    } else {
        return @{
                 @"title": self.title,
                 @"source": self.source,
                 @"age": self.age,
                 @"expanded": (self.expanded ? @"YES" : @"NO"),
                 @"briefmeViewViewed": (self.briefmeViewViewed ? @"YES" : @"NO"),
#ifdef BM_ROUTES
                 @"section": BM_ROUTES[self.section]
#else
                 @"section": @"top"
#endif
        };
    }
}

@end
