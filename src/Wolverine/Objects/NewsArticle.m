//
//  NewsArticle.m
//  MOST
//
//  Created by Todd Lubin on 6/29/12.
//  Copyright (c) 2012 Campion Designs, LLC. All rights reserved.
//

#import "NewsArticle.h"

@implementation NewsArticle

-(id)initWithDictionary:(NSDictionary *)dict
{
    self = [super initWithDictionary:dict];
    if (self) {
        // articleNum property is set in Parser
        _rank = [NSNumber numberWithInt:-1];
        
        // shareUrl defaults to url (set in Article.h)
        if ([dict objectForKey:@"share_url"]) {
            self.shareUrl = [NSString stringWithFormat:@"http://%@", [dict objectForKey: @"share_url"]];
        }
        
        NSDictionary *stats = [dict objectForKey:@"stats"];
        _score = [stats objectForKey:@"score"];
        _facebook = [stats objectForKey:@"facebook"];
        _twitter = [stats objectForKey:@"twitter"];
        _virality = [stats objectForKey:@"virality"];
    }
    
    return self;
}

- (NSDictionary *)getAttributes
{
    NSMutableDictionary *d = [NSMutableDictionary dictionaryWithDictionary:[super getAttributes]];
    [d addEntriesFromDictionary:@{@"score": self.score, @"rank": self.rank}];
    return d;
}

@end
