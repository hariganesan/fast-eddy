//
//  ArticleInnerCellView.m
//  BriefMe
//
//  Created by Charlie Vrettos on 6/23/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import "ArticleInnerCellView.h"

@implementation ArticleInnerCellView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUserInteractionEnabled:NO];
    }
    return self;
}

- (void)constrainTitleForText:(NSString *)text withMinFontSize:(float)minFontSize andMaxFontSize:(float)maxFontSize andFontName:(NSString *)fontName
{
    if (MAIN_WIDTH == IPHONE_6PLUS_WIDTH || MAIN_WIDTH == IPHONE_6_WIDTH) {
        maxFontSize += 2;
    }
    
    UIFont* font = [UIFont fontWithName:fontName size:maxFontSize];
    
    CGSize constraintSize = CGSizeMake(_title.frame.size.width, MAXFLOAT);
    NSRange range = NSMakeRange(minFontSize, maxFontSize);
    
    float fontSize;
    for (NSInteger i = maxFontSize*2; i > minFontSize*2; i--) {
        fontSize = ((float)range.length + (float)range.location) / 2.0;
        
        font = [font fontWithSize:fontSize];
        CGRect rect = [text boundingRectWithSize:constraintSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
        if (rect.size.height <= 85*SCALEUP) {
            range.location = fontSize;
        } else {
            range.length = fontSize - 0.5;
        }
        
        if (range.length == range.location) {
            font = [font fontWithSize:range.location];
            break;
        }
    }
    
    _title.font = font;
}

- (void)setStyledTitle:(NSString *)title withMinFontSize:(float)minFontSize andMaxFontSize:(float)maxFontSize andFontName:(NSString *)fontName
{
    CGRect oldFrame = _title.frame;
    [_title setNumberOfLines:4];
    [_title setText: title];
    [self constrainTitleForText:title withMinFontSize:minFontSize andMaxFontSize:maxFontSize andFontName:fontName];
    [_title sizeToFit];
    _title.frame = CGRectMake(_title.frame.origin.x, _title.frame.origin.y, oldFrame.size.width, _title.frame.size.height);
}

@end
