//
//  ArticleInnerCellView.h
//  BriefMe
//
//  Created by Charlie Vrettos on 6/23/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleInnerCellView : UIView

/** @brief The news source of the article. */
@property (nonatomic, strong) UILabel *source;
/** @brief The news article's title. */
@property (nonatomic, strong) UILabel *title;

/** @brief Set and reduce size of title depending on text length. */
- (void)constrainTitleForText:(NSString *)text withMinFontSize:(float)minFontSize andMaxFontSize:(float)maxFontSize andFontName:(NSString *)fontName;
- (void)setStyledTitle:(NSString *)title withMinFontSize:(float)minFontSize andMaxFontSize:(float)maxFontSize andFontName:(NSString *)fontName;

@end
