//
//  WebArticleView.h
//  BriefMe
//
//  Created by Charlie Vrettos on 6/23/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import "CustomWebView.h"

@class ArticleInnerCellView, BottomNavigationBarView, ExtraNavigationBarView;

@interface WebArticleView : CustomWebView

/** @discussion YES if article source host is in staticNavs in json 'config';
    disables showing/hiding the navbar */
@property (nonatomic) BOOL staticNav;
/** @brief YES when statusBar has been tapped and before scroll completed, else NO */
@property (nonatomic) BOOL goingToScrollToTop; // YES when statusbar tapped, else NO
@property (nonatomic, strong) ExtraNavigationBarView *webNavBar;

- (id)initWithFrame:(CGRect)frame andWithURL:(NSString *)url andWithNavBar:(BottomNavigationBarView *)navBar andWebConfig:(WKWebViewConfiguration *)webConfig;
/** @description Builds a WKWebViewConfiguration with the appropriate
    javascript userscripts for webarticleview added, including scroll
    handling code, DOM loaded code, viewport scale code, and web block
    code. */
+ (WKWebViewConfiguration *)createConfigWithScriptsForUrl:(NSURL *)url;

@end
