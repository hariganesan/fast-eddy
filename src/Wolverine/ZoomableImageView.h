//
//  ZoomableImageView.h
//  BriefMe
//
//  Created by Charlie Vrettos on 6/17/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @discussion View containing a zoomable image loaded with LoadImage.
    View is hidden rather than destroyed when closed so that if a user
    re-opens the same image (with the same currentImageUrl) it will
    display immediately rather than having to DL the image again */
@interface ZoomableImageView : UIScrollView<UIScrollViewDelegate>

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) NSURL *currentImageUrl;
@property (nonatomic, strong) UIActivityIndicatorView *activityView;

- (void)loadImage:(NSURL *)url;

@end
