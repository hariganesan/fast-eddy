//
//  Delegate.m
//  MOST
//
//  Created by Hari Ganesan on 12/17/13.
//  Copyright (c) 2013 Campion Designs, LLC. All rights reserved.
//

#import "Delegate.h"
#import "Parser.h"
#import "NavController.h"
#import "LoadingView.h"
#import "OnboardingView.h"
#import "Localytics.h"
#import <Parse/Parse.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <AVFoundation/AVFoundation.h>
#import "DeprecationViewController.h"

static Delegate *sharedInstance;

@interface Delegate ()

/** @brief Date that initApp was last called */
@property (nonatomic, strong) NSDate* startInitApp;
/** @brief Seconds app must be in background for in order to refresh upon return */
@property (nonatomic) NSInteger initTimeout;
/** @brief Date that app was last put in the background */
@property (nonatomic, strong) NSDate *dateOnExit;

@end


@implementation Delegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSSetUncaughtExceptionHandler(&onUncaughtException);
    _app = application;
    _webBlocks = nil;
    _staticNavSources = nil;
    _feedbackCellNumber = 4;
    _launchesUntilFeedback = 50;
    _autoWinners = NO;
    self.initTimeout = 15*60; // timeout is 15 minutes, if longer then refresh app
    _app.applicationIconBadgeNumber = 0;
    _briefingContentsDict = [[NSMutableDictionary alloc] init];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    // fixes a bug for 6+/ipad landscape launch
    application.statusBarOrientation = UIInterfaceOrientationPortrait;
    
    // init window
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    _tableViewFrameOffset = 100*SCALEUP;
    
    if (!sharedInstance) {
        sharedInstance = self;
    }
    
    // allow volume override over ringer
    [[AVAudioSession sharedInstance]
     setCategory: AVAudioSessionCategoryPlayback
     error: nil];
    
    if (![[[UIDevice currentDevice] model] isEqualToString:@"iPhone Simulator"] && ![[[UIDevice currentDevice] model] isEqualToString:@"iPad Simulator"]) {
        [Localytics autoIntegrate:@"c26065654cb5bea89e0d25f-8cef5fc2-5f97-11e4-277b-004a77f8b47f" launchOptions:launchOptions];
    }

//    [Localytics setLoggingEnabled:YES];
    
    // start parse for notifications
    [Parse setApplicationId:@"v4AkaJkfwJEFO4Ujc47KTIevxxgrYd3CPN8UVLix" clientKey:@"6Mjrch61zj3zGr1AQnfs4FxcExts4z3KofoYmM3v"];
    
    // enable crashlytics and fabric for twitter answers
//    [Fabric with:@[CrashlyticsKit]];
    
    // initialize everything
    [self initApp];

    return YES;
}

- (BOOL)application:(UIApplication *)application willContinueUserActivityWithType:(NSString *)userActivityType
{
    return NO;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray *restorableObjects))restorationHandler
{
    if (self.nc) {        
        restorationHandler(@[self.nc]);
        return YES;
    }
    
    return NO;
}

- (void) application:(UIApplication *)application didFailToContinueUserActivityWithType:(NSString *)userActivityType error:(NSError *)error
{
    NSLog(@"did fail to continue user activity with type: %@, with error: %@", userActivityType, error);
}

// http://stackoverflow.com/questions/24100313/ask-for-user-permission-to-receive-uilocalnotifications-in-ios-8
// TODO(hari): test out notifications
+ (void)registerNotifications
{
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge |UIUserNotificationTypeSound |UIUserNotificationTypeAlert) categories:nil]];
    if (![[UIApplication sharedApplication] isRegisteredForRemoteNotifications]) {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

// response to registerUserNotificationSettings: called above
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    NSLog(@"registered for notification settings");
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSInteger timezone = [Delegate timeZoneHour];
    
    // store device token in nsuserdefaults
    NSString *newToken = [deviceToken description];
    newToken = [newToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    newToken = [newToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] || ![[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isEqualToString:newToken]) {
        [[NSUserDefaults standardUserDefaults] setObject:newToken forKey:@"deviceToken"];
    }
    
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    currentInstallation.channels = @[@"global", [NSString stringWithFormat:@"hour%ld", (long)timezone]];
    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"failed to register for remote notifications");
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Notifications May Not Be Configured" message:@"You can configure notifications by going to Settings => Notifications => BriefMe." preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:nil]];
    [self.nc presentViewController:alert animated:YES completion:nil];
}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))handler
{
    // let Parse handle info
    [PFPush handlePush:userInfo];
    
    [Delegate sharedDel].autoWinners = YES;
    
    // if app in background and under timeout (over timeout is handled by foreground case), re-init it
    if (application.applicationState != UIApplicationStateActive && -[self.dateOnExit timeIntervalSinceNow] <= self.initTimeout) {
        [self initApp];
    }
    handler(UIBackgroundFetchResultNewData);
}

// user put app in background
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    self.dateOnExit = [NSDate date];
}

// user re-opened app from background
- (void)applicationWillEnterForeground:(UIApplication *)application
{
    application.applicationIconBadgeNumber = 0;

    // if its been more than initial timeout, refresh all sections
    if (-[self.dateOnExit timeIntervalSinceNow] > self.initTimeout) {
        [self initApp];
    }
}

- (void)initApp
{
     self.startInitApp = [NSDate date];
    // create an empty dictionary of articleIDs if it doesnt exist (first launch)
    // flush entries from before 3 days
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"articleIDs"]) {
        [[NSUserDefaults standardUserDefaults] setObject:[[NSMutableDictionary alloc] init] forKey:@"articleIDs"];
    } else {
        NSMutableDictionary *flushedIDs = [[NSMutableDictionary alloc] init];
        
        [[[NSUserDefaults standardUserDefaults] objectForKey:@"articleIDs"] enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL* stop) {
            if (-[(NSDate *)value timeIntervalSinceNow] < 3*24*60*60) {
                [flushedIDs setObject:value forKey:key];
            }
        }];
        
        [[NSUserDefaults standardUserDefaults] setObject:flushedIDs forKey:@"articleIDs"];
    }
    
    
    // set to NO for new users and users coming from old version so that bedtimeview may appear
     if (![[NSUserDefaults standardUserDefaults] objectForKey:@"hasShownBedtimeView"]) {
         [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"hasShownBedtimeView"];
     }
    
    // Trigger tutorial if this is the first launch
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"hasPerformedFirstLaunch"]) {
        _firstLaunch = YES;
    
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hasPerformedFirstLaunch"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"firstLaunchDate"];
    } else {
        _firstLaunch = NO;
    }
    
    // up launch count
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"launchCount"]) {
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"launchCount"];
    } else {
        NSInteger launchCount = [[NSUserDefaults standardUserDefaults] integerForKey:@"launchCount"];
        [[NSUserDefaults standardUserDefaults] setInteger:(launchCount+1) forKey:@"launchCount"];
    }
    
    [Localytics tagEvent:@"Launch" attributes:@{@"launchCount": [NSString stringWithFormat:@"%ld", (long)[[NSUserDefaults standardUserDefaults] integerForKey:@"launchCount"]]}];

    // Initialize view controllers, parse objects, etc.
    _nc = [[NavController alloc] init];
    
    self.window.rootViewController = _nc;
    [self.window makeKeyAndVisible];
    
    // shut down final dialog after 12/15/16
    NSDate *currentDate = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setYear:2016];
    [components setMonth:12];
    [components setDay:15];
    NSDate *shutDownDate = [calendar dateFromComponents:components];
    
    BOOL shutDown = NO;
//    if ([currentDate timeIntervalSinceDate:shutDownDate] > 0) {
//        shutDown = YES;
//    }
    
    DeprecationViewController *dc = [[DeprecationViewController alloc] initWithShutDown:shutDown];
    dc.delegate = self;
    [_nc presentViewController:dc animated:NO completion:nil];
    
    if (!shutDown) {
        [_nc createInitialController:nil];
        
        _loadingView = [[LoadingView alloc] init];
        [_nc.view addSubview:_loadingView];
        [_loadingView animate];
        
        // remove !...
        if (_firstLaunch) {
            [Localytics tagScreen:@"Onboarding Started"];
            _onboardingView = [[OnboardingView alloc] initWithFrame:CGRectMake(0, 0, MAIN_WIDTH, MAIN_HEIGHT)];
            [self.nc.view addSubview:_onboardingView];
            [_onboardingView animate];
        }
        
        // if no local webBlocks (should only occur on first launch), copy webblocks.json into nsuserdefaults
        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"webBlocks"]){
            NSString *filePath = [[NSBundle mainBundle] pathForResource:@"webBlocks" ofType:@"json"];
            NSData *jsonData = [[NSData alloc] initWithContentsOfFile:filePath];
            NSError *error = nil;
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
            if (error) {
                // this should never ever happen
                NSLog(@"Error parsing webBlocks.json: %@", error);
            } else {
                [[NSUserDefaults standardUserDefaults] setObject:jsonDict forKey:@"webBlocks"];
                self.webBlocks = jsonDict;
            }
        } else {
            self.webBlocks = [[NSUserDefaults standardUserDefaults] objectForKey:@"webBlocks"];
        }
    }
}

// called when app has loaded enough and the loadingview (flipping book) has just been hidden
- (void)appLoaded
{
    NSTimeInterval initAppTime = [[NSDate date] timeIntervalSinceDate: self.startInitApp];
    [Localytics tagEvent:@"App Init" attributes:@{@"time":[NSString stringWithFormat:@"%d", (int)round(initAppTime*1000)]}];
    
    NSInteger currentTimezone = [Delegate timeZoneHour];
    // if no timezone stored yet (1st launch or user with old version, can't tell if timezone changed; store to try again future launches)
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"timeZoneHour"]) {
        // timeZoneHour should always be an accurate timezone, even if the user has push notifications off and it isn't stored in parse
        [[NSUserDefaults standardUserDefaults] setInteger:currentTimezone forKey:@"timeZoneHour"];
    } else {
        NSInteger storedTimezone = [[NSUserDefaults standardUserDefaults] integerForKey:@"timeZoneHour"];
        
        if (currentTimezone != storedTimezone) {
            // save new timezone
            [[NSUserDefaults standardUserDefaults] setInteger:currentTimezone forKey:@"timeZoneHour"];
            
            // if user has push notifications turned on, send their new timezone to parse
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"winners"] && [[NSUserDefaults standardUserDefaults] boolForKey:@"winners"]){
                // send new timezone to parse
                PFInstallation *currentInstallation = [PFInstallation currentInstallation];
                currentInstallation.channels = @[@"global", [NSString stringWithFormat:@"hour%ld", (long)currentTimezone]];
                [currentInstallation saveInBackground];
            }
        }
    }
}

- (void)dismissDC
{
    [_nc dismissViewControllerAnimated:YES completion:nil];
}

- (void)application:(UIApplication *)application handleWatchKitExtensionRequest:(NSDictionary *)userInfo reply:(void (^)(NSDictionary *))reply
{
    [Localytics handleWatchKitExtensionRequest:userInfo reply:reply];
}

+ (Delegate *)sharedDel
{
    return sharedInstance;
}

+ (NSInteger)timeOfDay
{
    NSInteger hour = [[[NSCalendar currentCalendar] components:NSCalendarUnitHour fromDate:[NSDate date]] hour];
    if (hour >= 6 && hour < 12) {
        return TIME_OF_DAY_MORNING;
    } else if (hour >= 12 && hour < 18) {
        return TIME_OF_DAY_AFTERNOON;
    } else if (hour >= 18 && hour < 24) {
        return TIME_OF_DAY_EVENING;
    }
    
    return TIME_OF_DAY_NIGHT;
}

+ (NSInteger)timeZoneHour
{
    NSCalendar *g = [NSCalendar currentCalendar];
    [g setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setTimeZone:[NSTimeZone defaultTimeZone]];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSInteger timezone = [[g components:NSCalendarUnitHour fromDate:[df dateFromString:@"1970-01-01 00:00:00"]] hour];
    return timezone;
}

+ (NSInteger)autoTheme
{
    NSInteger hour = [[[NSCalendar currentCalendar] components:NSCalendarUnitHour fromDate:[NSDate date]] hour];
    if (hour >= 6 && hour < 22) {
        return AUTO_THEME_DAY;
    }
    
    return AUTO_THEME_NIGHT;
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (void)updateParseWithToggle:(BOOL)toggle
{
    // Update stored timezone
    NSInteger timezone = [self timeZoneHour];
    [[NSUserDefaults standardUserDefaults] setInteger:timezone forKey:@"timeZoneHour"];
    
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    
    if (toggle) {
        currentInstallation.channels = @[@"global", [NSString stringWithFormat:@"hour%ld", (long)timezone]];
    } else {
        currentInstallation.channels = @[];
    }

    [currentInstallation saveInBackground];
}

void onUncaughtException(NSException* exception)
{
    NSLog(@"UNCAUGHT EXCEPTION: %@", exception.description);
    [Localytics tagEvent:@"UncaughtException" attributes:@{@"description": exception.description}];

}

+(CGFloat) AACStatusBarHeight
{
    CGSize statusBarSize = [[UIApplication sharedApplication] statusBarFrame].size;
    return MIN(statusBarSize.width, statusBarSize.height);
}

+(CGFloat) AACStatusBarWidth
{
    CGSize statusBarSize = [[UIApplication sharedApplication] statusBarFrame].size;
    return MAX(statusBarSize.width, statusBarSize.height);
}

@end
