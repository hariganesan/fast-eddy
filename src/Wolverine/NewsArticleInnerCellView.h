//
//  NewsArticleInnerCellView.h
//  BriefMe
//
//  Created by Charlie Vrettos on 6/22/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICircleView.h"
#import "ArticleInnerCellView.h"

@interface NewsArticleInnerCellView : ArticleInnerCellView

/** @brief Number shown in the top left corner of the cell. */
@property (nonatomic, strong) UILabel *rank;
/** @brief The BriefMe Score. */
@property (nonatomic, strong) UILabel *score;
/** @brief The glowing circle around the score that is filled in depending on the score. */
@property (nonatomic, strong) UICircleView *vcircle;
/** @brief A thin ring to show the background of the glow circle. */
@property (nonatomic, strong) UICircleView *vcircleBackground;
/** @brief An invisible button that shows the explanationView. */

- (id)initWithFrame:(CGRect)frame;

@end
