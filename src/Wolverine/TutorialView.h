//
//  TutorialView.h
//  MOST
//
//  Created by Charlie Vrettos on 7/28/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @discussion Little red tooltip bubble that has some text and an
    arrow pointing to what it's referring to. This is a bubble that
    shows up to provide some tips during the tutorial. It can point 
    in any direction. */
@interface TutorialView : UIView

typedef enum {
    TutorialTypeTop,
    TutorialTypeRight,
    TutorialTypeBottom,
    TutorialTypeLeft
} TutorialType;

/** @brief The triangle that is attached to the view. */
@property (nonatomic, strong) UIView *triangle;
/** @brief The layer (shape) of the triangle. */
@property (nonatomic, strong) CAShapeLayer *triangleShape;

/** @brief The label that displays this tutorial text. */
@property (nonatomic, strong) UILabel * tutorialLabel;
/** @brief The direction of the arrow for the tutorial. */
@property (nonatomic) TutorialType tutType;

- (id)initWithFrame:(CGRect)frame andType:(TutorialType)tutType;

/** @discussion Bobs the tutorial a few times to remind the user that
    it is awaiting an action. */
- (void)bobAfterDelay:(CGFloat)delay;

@end
