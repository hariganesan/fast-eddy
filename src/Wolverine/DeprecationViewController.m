//
//  DeprecationViewController.m
//  BriefMe
//
//  Created by Charlie Vrettos on 7/30/15.
//  Copyright © 2015 BriefMe Media, LLC. All rights reserved.
//

#import "DeprecationViewController.h"

@interface DeprecationViewController ()

@end

@implementation DeprecationViewController

- (instancetype)initWithShutDown:(BOOL)shutDown
{
    self = [super init];
    if (self) {
        self.view.backgroundColor = [UIColor blackColor];

        UIImageView *book = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blank-flip.png"]];
        book.frame = CGRectMake(MAIN_WIDTH/2-60/2, MAIN_HEIGHT/2-115, 60, 60);
        [self.view addSubview:book];
        
        NSString *topText;
        
        if (shutDown) {
            topText = @"BRIEFME has shut down.";
        } else {
            topText = @"BRIEFME is shutting down soon.";
        }
        
        // make 'BRIEF' bold
        NSDictionary *topTextAttrs = [NSDictionary dictionaryWithObjectsAndKeys: [UIFont fontWithName:@"BrandonGrotesque-Regular" size:20], NSFontAttributeName, nil];
        NSDictionary *subAttrs = [NSDictionary dictionaryWithObjectsAndKeys: [UIFont fontWithName:@"BrandonGrotesque-Bold" size:20], NSFontAttributeName, nil];
        NSRange range = NSMakeRange(0,5); // range of BRIEF
        NSMutableAttributedString *attributedTopText = [[NSMutableAttributedString alloc] initWithString:topText attributes:topTextAttrs];
        [attributedTopText setAttributes:subAttrs range:range];
        [attributedTopText addAttribute:NSKernAttributeName value:@1.2f range:NSMakeRange(0,[topText length])];
        [attributedTopText addAttribute:NSKernAttributeName value:@4.0f range:NSMakeRange(6,1)]; // extra spacing bewtween 'briefme' and 'has'
        
        UILabel *topLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, book.frame.origin.y+book.frame.size.height+10, MAIN_WIDTH, 40)];
        [topLabel setAttributedText:attributedTopText];
        [topLabel setTextColor:[UIColor whiteColor]];
        [topLabel setTextAlignment:NSTextAlignmentCenter];
        [self.view addSubview:topLabel];
        
        NSString *bottomText;
        if (shutDown) {
            bottomText = @"We appreciate your support.";
        } else {
            bottomText = @"Service will be suspended on 12/15/16.";
        }
        
        NSDictionary *bottomTextAttrs = [NSDictionary dictionaryWithObjectsAndKeys: [UIFont fontWithName:@"BrandonGrotesque-Regular" size:14], NSFontAttributeName, nil];
        NSMutableAttributedString *attributedBottomText = [[NSMutableAttributedString alloc] initWithString:bottomText attributes:bottomTextAttrs];
        [attributedBottomText addAttribute:NSKernAttributeName value:@1.2f range:NSMakeRange(0,[bottomText length])];
        
        UILabel *bottomLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, topLabel.frame.origin.y+topLabel.frame.size.height, MAIN_WIDTH, 40)];
        [bottomLabel setAttributedText:attributedBottomText];
        [bottomLabel setTextAlignment:NSTextAlignmentCenter];
        [bottomLabel setTextColor:[UIColor whiteColor]];
        [self.view addSubview:bottomLabel];
    
        NSString *buttonText = @"Read More";
        NSDictionary *buttonTextAttrs = [NSDictionary dictionaryWithObjectsAndKeys: [UIFont fontWithName:@"BrandonGrotesque-Regular" size:14], NSFontAttributeName, nil];
        NSMutableAttributedString *attributedButtonText = [[NSMutableAttributedString alloc] initWithString:buttonText attributes:buttonTextAttrs];
        [attributedButtonText addAttribute:NSKernAttributeName value:@1.2f range:NSMakeRange(0,[buttonText length])];
        
        UIButton *updateButton = [[UIButton alloc] initWithFrame:CGRectMake(MAIN_WIDTH/2-120*SCALEUP/2, bottomLabel.frame.origin.y+bottomLabel.frame.size.height+15, 120*SCALEUP, 30)];
        updateButton.layer.borderColor = [UIColor whiteColor].CGColor;
        updateButton.layer.borderWidth = 1.0f;
        updateButton.layer.cornerRadius = 4.0f;
        [updateButton setAttributedTitle:attributedButtonText forState:UIControlStateNormal];
        [updateButton.titleLabel setTextColor:[UIColor whiteColor]];
        [updateButton addTarget:self action:@selector(update) forControlEvents:UIControlEventTouchUpInside];
        
        if (!shutDown) {
            UIImageView *xout = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"x-out.png"]];
            [xout setFrame:CGRectMake(MAIN_WIDTH-50, 10, 35, 35)];
            [self.view addSubview:xout];

            UIButton *dismiss = [[UIButton alloc] initWithFrame:self.view.frame];
            dismiss.backgroundColor = [UIColor clearColor];
            [dismiss addTarget:self.delegate action:@selector(dismissDC) forControlEvents:UIControlEventTouchUpInside];
            
            [self.view addSubview:dismiss];
        }
        
        [self.view addSubview:updateButton];
    }
    return self;
}

- (void)update
{
    // go to blog post
    NSString *appStoreLink = @"http://www.getbriefme.com";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appStoreLink]];    
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

@end
