//
//  WebNavigationBarView.h
//  BriefMe
//
//  Created by Charlie Vrettos on 7/23/15.
//  Copyright © 2015 BriefMe Media, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExtraNavigationBarView : UIView

@property (nonatomic, strong) UIImageView *backTriangleImageView;
@property (nonatomic, strong) UIImageView *forwardTriangleImageView;

/** @brief An invisible button for WKWebview controls */
@property (nonatomic, strong) UIButton *backButton;
/** @brief An invisible button for WKWebview controls */
@property (nonatomic, strong) UIButton *forwardButton;
/** @brief An invisible button for WKWebview controls */
@property (nonatomic, strong) UIButton *refreshButton;

/** @discussion Whether or not the view has been displayed yet.
 It is not displayed until the user navigates and the webview
 canGoBack returns YES for the first time */
@property (nonatomic) BOOL visible;

/** @discussion Animates changing frame to upFrame. This is its
    position when the navigation bar is visible. */
- (void)moveUp;
/** @discussion Animates changing frame to downFrame. This is its
    position when the navigation bar is hidden. */
- (void)moveDown;
/** @discussion Animates setting alpha to 1.0 of view once
    canGoBack == YES for the first time. */
- (void)animateAppear;
@end
