//
//  NewsArticleCell.h
//  MOST
//
//  Created by Hari Ganesan on 6/20/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleCell.h"
#import "NewsArticleInnerCellView.h"

@class UICircleView, NewsArticle;

/** @brief View for cells within the Briefing or any of the sections (politics, sports, etc.) */
@interface NewsArticleCell : ArticleCell <UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIButton *explanationViewOpenButton;
/** @brief A dim overlay over the bubble that appears in the tutorial. */
@property (nonatomic, strong) UIView *dimView;
/** @brief A view containing title, rank, score, source. */
@property (nonatomic, strong) NewsArticleInnerCellView *newsArticleInnerCellView;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;

@end
