//
//  WebBrowserViewController.m
//  BriefMe
//
//  Created by Charlie Vrettos on 6/26/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import "WebBrowserViewController.h"
#import "CustomWebView.h"
#import "BottomNavigationBarView.h"
#import "Delegate.h"
#import "NavController.h"
#import "SettingsViewController.h"

@implementation WebBrowserViewController

- (id)initWithUrl:(NSURL *)url
{
    self = [super init];
    if (self) {
        _startURL = url;
        
        _statusBarBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAIN_WIDTH, STATUS_BAR_HEIGHT)];
        _statusBarBackground.backgroundColor = [Delegate sharedDel].articleBackgroundColor;
        [self.view addSubview:_statusBarBackground];
        
        if ([Delegate sharedDel].settingsvc.control.selectedSegmentIndex == THEME_NIGHT || ([Delegate sharedDel].settingsvc.control.selectedSegmentIndex == THEME_AUTO && [Delegate autoTheme] == AUTO_THEME_NIGHT)) {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        } else {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        }
        
        _bar = [[BottomNavigationBarView alloc] initWithType:BottomNavigationBarTypeWebBrowser];
        _bar.backgroundColor = [Delegate sharedDel].sectionBackgroundColor;
        
        WKWebViewConfiguration *webConfig = [CustomWebView createConfigWithScriptsForUrl:_startURL scroll:YES scrollKey:SCROLL_KEY openLink:NO openImage:NO loadedDOM:YES scale:YES nightMode:NO webBlocks:YES];
        [webConfig.userContentController addScriptMessageHandler:self name:SCROLL_KEY];
        [webConfig.userContentController addScriptMessageHandler:self name:READ_MORE_KEY];
        [webConfig.userContentController addScriptMessageHandler:self name:OPEN_LINK_KEY];
        [webConfig.userContentController addScriptMessageHandler:self name:OPEN_IMAGE_KEY];
        [webConfig.userContentController addScriptMessageHandler:self name:LOADED_DOM_KEY];
        _webView = [[CustomWebView alloc] initWithFrame:CGRectMake(0, STATUS_BAR_HEIGHT, MAIN_WIDTH, MAIN_HEIGHT-STATUS_BAR_HEIGHT) andWithNavBar:_bar andWithWebConfig:webConfig];
        _webView.navigationDelegate = self;
        [_webView loadFromURL:url];
        _webView.activityView.color = [UIColor grayColor];

        [_webView setBackgroundColor:[Delegate sharedDel].articleBackgroundColor];
        [self.view addSubview:_webView];
        
        // overlay the left edge with a skinny invisible column to trigger parallax on (fixes huffpo etc)
        UIView *panBackFixClearColumn = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 15*SCALEUP, MAIN_HEIGHT)];
        panBackFixClearColumn.backgroundColor = [UIColor clearColor];
        [self.view addSubview:panBackFixClearColumn];
        
        [_bar.shareButton addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
        [_bar.webForwardButton addTarget:_webView action:@selector(goForward) forControlEvents:UIControlEventTouchUpInside];
        [_bar.webBackButton addTarget:_webView action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
        [_bar.refreshButton addTarget:_webView action:@selector(reload) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_bar];
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    _webView = nil;
}

- (void)share
{
    NSURL *url = _webView.URL;
    if ([[url absoluteString] length] > 5)
        [[Delegate sharedDel].nc shareButtonPressedWithURL:url];
    else { // _webView.URL rets @"" may sometimes when share tapped immediately after browser is opened
        [[Delegate sharedDel].nc shareButtonPressedWithURL:_startURL];
    }
}

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message
{
    if ([message.name isEqualToString:SCROLL_KEY]) {
        [self handleScroll:message.body];
    } else if ([message.name isEqualToString:LOADED_DOM_KEY]) {
        [self handleLoadedDOM];
    } else {
        NSLog(@"Invalid script message with name: %@ and body: %@", message.name, message.body);
    }
}

- (void)handleLoadedDOM
{
    [_webView.activityView stopAnimating];
}

- (void)handleScroll:(NSString *)body
{
    
    float scrollOffset = [body floatValue];

    if (scrollOffset < 0) {
        [_bar show];
        return;
    }
    
    if (![_webView draggingOrBouncing]) {
        return;
    }
    
    // show or hide navbar upon scrolling
    ScrollDirection scrollDirection = ScrollDirectionNone;
    
    if (_webView.lastContentOffset < scrollOffset)
        scrollDirection = ScrollDirectionDown;
    else if (_webView.lastContentOffset > scrollOffset)
        scrollDirection = ScrollDirectionUp;
    _webView.lastContentOffset = scrollOffset;
  
    if (scrollDirection == ScrollDirectionUp)
        [_bar show];
    else if (scrollDirection == ScrollDirectionDown && scrollOffset > 30)
        [_bar hide];
    // no need to manually call [_bar show] when you reach the end of an article because bounces=YES will trigger it
}

- (void)webView:(nonnull WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation
{
    CustomWebView *cwv = (CustomWebView *)webView;
    [cwv finishedNavigation];
    
    // could use WKBackForwardList to get all urls in navigation history rather than relying on canGoBack / canGoForward
    if ([_webView canGoBack]) {
        _bar.backTriangleImageView.alpha = 1.0f;
    } else {
        _bar.backTriangleImageView.alpha = DISABLED_BUTTON_ALPHA;
    }
    
    if ([_webView canGoForward]) {
        _bar.forwardTriangleImageView.alpha = 1.0f;
    } else {
        _bar.forwardTriangleImageView.alpha = DISABLED_BUTTON_ALPHA;
    }
}

- (void)webView:(nonnull WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
    [_webView.activityView startAnimating];
}

- (void)webView:(nonnull WKWebView *)webView didFailLoadWithError:(NSError *)error
{
    [_webView.activityView stopAnimating];
}

- (void)webView:(nonnull WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    [_webView.activityView stopAnimating];
}

- (void)webView:(nonnull WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    [_webView.activityView stopAnimating];
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


@end
