//
//  OnboardingView.m
//  MOST
//
//  Created by Charlie Vrettos on 10/7/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import "Delegate.h"
#import "OnboardingView.h"
#import "Localytics.h"
#import "SettingsViewController.h"
#import "BriefingListViewController.h"
#import "TutorialView.h"

@implementation OnboardingView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor blackColor];
        
        int base = 0;
        if (MAIN_HEIGHT < IPHONE_5_HEIGHT)
            base = -20;
        
        _offScreenDistance = 230;
        
        // maps
        _greyMap = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"whattheworld-grey.png"]];
        _greyMap.frame = CGRectMake(MAIN_WIDTH/2 - .6*MAIN_WIDTH/2, 55 + base, .6*MAIN_WIDTH, .6*MAIN_WIDTH*405/759);

        _redMap = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"whattheworld-red.png"]];
        _redMap.frame = _greyMap.frame;
        [self addSubview:_redMap];
        [self addSubview:_greyMap];

        // left arrow, monitor, phone
        _greyReadingArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow1-grey.png"]];
        _greyReadingArrow.frame = CGRectMake(MAIN_WIDTH/2 - 40/2 + 3, (160 + base)*SCALEUP, 42, 42*260/162);

        _redReadingArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow1-red.png"]];
        _redReadingArrow.frame = _greyReadingArrow.frame;
        [self addSubview:_redReadingArrow];
        [self addSubview:_greyReadingArrow];
        
        _greyMonitor = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"readingcomp-grey.png"]];
        _greyMonitor.frame = CGRectMake(40*SCALEUP, (190 + base)*SCALEUP, 32*141/118, 32);
        _redMonitor = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"readingcomp-red.png"]];
        _redMonitor.frame = _greyMonitor.frame;
        [self addSubview:_redMonitor];
        [self addSubview:_greyMonitor];
        
        _greyPhone = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"readingphone-grey.png"]];
        _greyPhone.frame = CGRectMake(90*SCALEUP, (190 + base)*SCALEUP, 32*73/112, 32);
        _redPhone = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"readingphone-red.png"]];
        _redPhone.frame = _greyPhone.frame;
        [self addSubview:_redPhone];
        [self addSubview:_greyPhone];

        // right arrow, tw logo, fb logo
        _greySharingArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow2-grey.png"]];
        _greySharingArrow.frame = CGRectMake(MAIN_WIDTH/2 - 40/2 - 3, (160 + base)*SCALEUP, 42, 42*260/162);
        _redSharingArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow2-red.png"]];
        _redSharingArrow.frame = _greySharingArrow.frame;
        [self addSubview:_redSharingArrow];
        [self addSubview:_greySharingArrow];
        
        _greyTwitter = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sharingtwitter-grey.png"]];
        _greyTwitter.frame = CGRectMake(210*SCALEUP, (190 + base)*SCALEUP, 32*141/146, 32);
        _redTwitter = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sharingtwitter-red.png"]];
        _redTwitter.frame = _greyTwitter.frame;
        [self addSubview:_redTwitter];
        [self addSubview:_greyTwitter];
        
        _greyFacebook = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sharingfb-grey.png"]];
        _greyFacebook.frame = CGRectMake(255*SCALEUP, (190 + base)*SCALEUP, 32*62/128, 32);
        _redFacebook = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sharingfb-red.png"]];
        _redFacebook.frame = _greyFacebook.frame;
        [self addSubview:_redFacebook];
        [self addSubview:_greyFacebook];
        
        _redClock = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"clock-red.png"]];
        _redClock.frame = CGRectMake(MAIN_WIDTH/2 - 36/2, (250 + base)*SCALEUP, 36, 36);
        _greyClock = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"clock-grey.png"]];
        _greyClock.frame = _redBook.frame;
        [self addSubview:_redClock];
        [self addSubview:_greyClock];

        // book
        _redBook = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"redBook.png"]];
        _redBook.frame = CGRectMake(MAIN_WIDTH/2 - 130/2, (360 + 1.5*base)*SCALEUP, 130, 130*407/497);
        _whiteBook = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"whiteBook.png"]];
        _whiteBook.frame = _redBook.frame;
        [self addSubview:_redBook];
        [self addSubview:_whiteBook];
        
        // "BRIEFME" with bold 'brief'
        UIFont *boldFont = [UIFont fontWithName:@"BrandonGrotesque-Black" size:22];
        UIFont *mediumFont = [UIFont fontWithName:@"BrandonGrotesque-Medium" size:20];
        UIFont *regularFont = [UIFont fontWithName:@"BrandonGrotesque-Regular" size:20];
        NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys: mediumFont, NSFontAttributeName, nil];
        NSDictionary *subAttrs = [NSDictionary dictionaryWithObjectsAndKeys: boldFont, NSFontAttributeName, nil];
        NSRange range = NSMakeRange(0,5); //range of BRIEF
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:@"BRIEFME" attributes:@{NSFontAttributeName: regularFont}];
        [attributedText setAttributes:subAttrs range:range];
        
        _briefMe = [[UILabel alloc] initWithFrame:CGRectMake(MAIN_WIDTH/2-150/2, (470 + 1.5*base)*SCALEUP, 150, 30)];
        [_briefMe setTextColor:[UIColor whiteColor]];
        [_briefMe setAttributedText:attributedText];
        _briefMe.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_briefMe];
        
        // used for bug fix described later
        _blackBox = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAIN_WIDTH, .6*MAIN_HEIGHT + base)];
        _blackBox.backgroundColor = [UIColor blackColor];
        _blackBox.alpha = 0.0;
        [self addSubview:_blackBox];
        
        _whatTheWorld = [self createLabel:@"What the world" withFrame:CGRectMake(0, (20+base)*SCALEUP, MAIN_WIDTH, 30) withAlignment:@"center"];
        [self addSubview:_whatTheWorld];
        _isReading = [self createLabel:@"is reading" withFrame:CGRectMake(25*SCALEUP, (150 + base)*SCALEUP, 100, 30) withAlignment:@"right"];
        [self addSubview:_isReading];
        _andSharing = [self createLabel:@"and sharing" withFrame:CGRectMake(210*SCALEUP, (150 + base)*SCALEUP, 120, 30) withAlignment:@"left"];
        [self addSubview:_andSharing];
        _now = [self createLabel:@"NOW" withFrame:CGRectMake(MAIN_WIDTH/2 - 70/2, (290+base)*SCALEUP, 70, 30) withAlignment:@"center"];
        [self addSubview:_now];
        
        
        
        
        
        
        // PUSH NOTIFICATION ONBOARDING
        NSMutableAttributedString *attributedText2 = [[NSMutableAttributedString alloc] initWithString:@"BriefMe sends you the three most consumed articles of the day via push notifications" attributes:attrs];
//        [attributedText2 setAttributes:subAttrs range:range];
        
        _pushNotifText = [[UILabel alloc] initWithFrame:CGRectMake(MAIN_WIDTH/2 - 250/2, 90, 250, 150)];
        [_pushNotifText setTextColor:[UIColor whiteColor]];
        [_pushNotifText setAttributedText:attributedText2];
        _pushNotifText.textAlignment = NSTextAlignmentCenter;
        _pushNotifText.numberOfLines = 4;
        [self addSubview:_pushNotifText];
        
        _okayButton = [[UIButton alloc] initWithFrame:CGRectMake(MAIN_WIDTH/2 - MAIN_WIDTH*.62/2, MAIN_HEIGHT*.55 + 2*_offScreenDistance, MAIN_WIDTH*.62, 70)];
        _okayButton.backgroundColor = [UIColor blackColor];
        _okayButton.layer.cornerRadius = 12;
        _okayButton.layer.borderColor = [UIColor whiteColor].CGColor;
        _okayButton.layer.borderWidth = 2.0f;
        [self addSubview:_okayButton];
        
        _okay = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 20)];
        _okay.center = _okayButton.center;
        _okay.center = CGPointMake(_okayButton.frame.size.width / 2, _okayButton.frame.size.height / 2);
        _okay.text = @"OKAY";
        [_okay setFont:[UIFont fontWithName:@"BrandonGrotesque-Medium" size:18]];
        _okay.textColor = [UIColor whiteColor];
        _okay.textAlignment=NSTextAlignmentCenter;
        [_okayButton addTarget:self action:@selector(pushNotifOkay) forControlEvents:UIControlEventTouchUpInside];
        
        _triangle = [[UIView alloc] initWithFrame:CGRectMake(MAIN_WIDTH/2 - 40/2, _okayButton.frame.origin.y - 2*_offScreenDistance, 40, 22)];
        _triangle.backgroundColor = [UIColor clearColor];
        CAShapeLayer *triangleShape = [[CAShapeLayer alloc]initWithLayer:self];
        triangleShape.bounds = CGRectMake(0, 0, 40, 24);
        triangleShape.anchorPoint = CGPointMake(0,0);
        UIBezierPath *trianglePath = [UIBezierPath bezierPath];
        [trianglePath moveToPoint:CGPointMake(CGRectGetMinX(triangleShape.frame), CGRectGetMaxY(triangleShape.frame) - 1.5)];
        [trianglePath addLineToPoint:CGPointMake(CGRectGetMidX(triangleShape.frame) - 0.25, CGRectGetMinY(triangleShape.frame))];
        [trianglePath addLineToPoint:CGPointMake(CGRectGetMidX(triangleShape.frame) + 0.25, CGRectGetMinY(triangleShape.frame))];
        [trianglePath addLineToPoint:CGPointMake(CGRectGetMaxX(triangleShape.frame), CGRectGetMaxY(triangleShape.frame) - 1.5)];
        triangleShape.path = trianglePath.CGPath;
        triangleShape.fillColor = LIPSTICK_COLOR.CGColor;
        [_triangle.layer addSublayer:triangleShape];
        [self addSubview:_triangle];
        _triangle.hidden = YES;
        [_okayButton addSubview:_okay];
        
        UIView *hiddens[] = {_greyMap, _greyPhone, _greyMonitor, _greyReadingArrow, _greySharingArrow, _greyTwitter, _greyFacebook, _greyClock, _redBook, _whiteBook, _briefMe};
        for (int i = 0; i < (sizeof(hiddens) / sizeof(hiddens[0])); ++i)
            hiddens[i].alpha = 0.0;
        
        
        
        _skipButton = [[UIButton alloc] initWithFrame:CGRectMake(MAIN_WIDTH-90, MAIN_HEIGHT-30, 90,30)];
        [self addSubview:_skipButton];
        _skipButton.hidden = YES;
        _skipButton.alpha = 0.0;
        
        UILabel *skip = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 50)];
        skip.center = CGPointMake(_skipButton.frame.size.width / 2, _skipButton.frame.size.height / 2);
        skip.text = @"Skip";
        [skip setFont:[UIFont fontWithName:@"BrandonGrotesque-Medium" size:18]];
        skip.textColor = [UIColor whiteColor];
        skip.textAlignment=NSTextAlignmentCenter;
        [_skipButton addSubview:skip];
        [_skipButton addTarget:self action:@selector(pushNotifSkip) forControlEvents:UIControlEventTouchUpInside];

        UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"back.png"]];
        arrow.frame = CGRectMake(_skipButton.frame.size.width - 30, _skipButton.frame.size.height/2 - 30/2 +1, 30, 30);
        arrow.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(180));
        [_skipButton addSubview:arrow];
        
        UIView *gradient = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _skipButton.frame.size.width, _skipButton.frame.size.height)];
        gradient.backgroundColor = [UIColor blackColor];
        gradient.alpha = 0.3;
        [_skipButton addSubview:gradient];
        [gradient setUserInteractionEnabled:NO];
        
    }
    return self;
}

- (void)animate
{
    // views listed twice move twice the distance away (they're too big for 1)
    UIView *moveUps[] = {_whatTheWorld, _redMap, _greyMap, _pushNotifText, _pushNotifText};
    for (int i = 0; i < (sizeof(moveUps) / sizeof(moveUps[0])); ++i)
        [self moveView:moveUps[i] inDirection:@"up" pixels:_offScreenDistance];

    UIView *moveLefts[] = {_greyMonitor, _redMonitor, _greyPhone, _redPhone, _isReading, _greyReadingArrow, _redReadingArrow};
    for (int i = 0; i < (sizeof(moveLefts) / sizeof(moveLefts[0])); ++i)
        [self moveView:moveLefts[i] inDirection:@"left" pixels:_offScreenDistance];

    UIView *moveRights[] = {_greyFacebook, _redFacebook, _greyTwitter, _redTwitter, _greySharingArrow, _redSharingArrow, _andSharing};
    for (int i = 0; i < (sizeof(moveRights) / sizeof(moveRights[0])); ++i)
        [self moveView:moveRights[i] inDirection:@"right" pixels:_offScreenDistance];
    
    UIView *moveDowns[] = {_now, _redClock};
    for (int i = 0; i < (sizeof(moveDowns) / sizeof(moveDowns[0])); ++i)
        [self moveView:moveDowns[i] inDirection:@"down" pixels:2*_offScreenDistance];
    
    int base = 0;
    if (MAIN_HEIGHT < IPHONE_5_HEIGHT)
        base = -20;
    
    float wtwDuration = 0.6;
    float irDuration = 1.0;
    float asDuration = 1.0;
    float nowDuration = 1.0;
    float bookFadeDuration = 1.5;
    float wordMoveDuration = 1.0;
    float delays = 4*0.3;
    float total = wtwDuration + irDuration + asDuration + nowDuration + bookFadeDuration + wordMoveDuration + delays;
    
    // ugly bug fix code; create new uilabels to replace the _labels which don't animate well...
    //  if there are two animation frames acting on the _labels, they both execute at once for some reason
    UILabel *newWhatTheWorld = [self createLabel:@"What the world" withFrame:_whatTheWorld.frame withAlignment:@"center"];
    [self addSubview:newWhatTheWorld];
    newWhatTheWorld.alpha = 0.0;
    [self moveView:newWhatTheWorld inDirection:@"down" pixels:_offScreenDistance];
    
    UILabel *newNow = [self createLabel:@"NOW" withFrame:_now.frame withAlignment:@"center"];
    [self addSubview:newNow];
    newNow.alpha = 0.0;
    [self moveView:newNow inDirection:@"up" pixels:2*_offScreenDistance];
    
    UILabel *newIsReading = [self createLabel:@"is reading" withFrame:_isReading.frame withAlignment:@"right"];
    [self addSubview:newIsReading];
    newIsReading.alpha = 0.0;
    [self moveView:newIsReading inDirection:@"right" pixels:_offScreenDistance];

    UILabel *newAndSharing = [self createLabel:@"and sharing" withFrame:_andSharing.frame withAlignment:@"left"];
    [self addSubview:newAndSharing];
    newAndSharing.alpha = 0.0;
    [self moveView:newAndSharing inDirection:@"left" pixels:_offScreenDistance];
    
    [UIView animateKeyframesWithDuration:total delay:0.0 options:0 animations:^{
        float startTime = 0;
        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:wtwDuration/total animations:^{
            UIView *movings[] = {_whatTheWorld, _redMap, _greyMap};
            for (int i = 0; i < (sizeof(movings) / sizeof(movings[0])); ++i)
                [self moveView:movings[i] inDirection:@"down" pixels:_offScreenDistance];
        }];
        startTime += wtwDuration;
        [UIView addKeyframeWithRelativeStartTime:startTime/total relativeDuration:irDuration/total animations:^{
            _greyMap.alpha = 1.0;
            UIView *movings[] = {_isReading, _redPhone, _greyPhone, _redMonitor, _greyMonitor, _redReadingArrow, _greyReadingArrow};
            for (int i = 0; i < (sizeof(movings) / sizeof(movings[0])); ++i)
                [self moveView:movings[i] inDirection:@"right" pixels:_offScreenDistance];
        }];
        startTime += irDuration;
        [UIView addKeyframeWithRelativeStartTime:(startTime + 0.3)/total relativeDuration:asDuration/total animations:^{
            _greyMonitor.alpha = 1.0;
            _greyPhone.alpha = 1.0;
            _greyReadingArrow.alpha = 1.0;
            UIView *movings[] = {_andSharing, _greyTwitter, _redTwitter, _greyFacebook, _redFacebook, _greySharingArrow, _redSharingArrow};
            for (int i = 0; i < (sizeof(movings) / sizeof(movings[0])); ++i)
                [self moveView:movings[i] inDirection:@"left" pixels:_offScreenDistance];
        }];
        startTime += asDuration;
        [UIView addKeyframeWithRelativeStartTime:(startTime + 0.3)/total relativeDuration:nowDuration/total animations:^{
            _greyTwitter.alpha = 1.0;
            _greyFacebook.alpha = 1.0;
            _greySharingArrow.alpha = 1.0;
            UIView *movings[] = {_redClock, _now};
            for (int i = 0; i < (sizeof(movings) / sizeof(movings[0])); ++i)
                [self moveView:movings[i] inDirection:@"up" pixels:2*_offScreenDistance];
        }];
        startTime += nowDuration;
        [UIView addKeyframeWithRelativeStartTime:(startTime + 0.3)/total relativeDuration:bookFadeDuration/total animations:^{
            _redBook.alpha = 1.0;
            _briefMe.alpha = 1.0;
            _redClock.alpha = 1.0;
            
            // bug fix; hide old labels to replace with new moveable ones; .hidden = YES doesn't work here
            _now.alpha = 0.0;
            _isReading.alpha = 0.0;
            _andSharing.alpha = 0.0;
            _whatTheWorld.alpha = 0.0;
            
            // bug fix; appear new labels to replace old ones; new ones are movable
            newWhatTheWorld.alpha = 1.0;
            newNow.alpha = 1.0;
            newAndSharing.alpha = 1.0;
            newIsReading.alpha = 1.0;
            _whatTheWorld = newWhatTheWorld;
            _isReading = newIsReading;
            _andSharing = newAndSharing;
            _now = newNow;
        }];
        startTime += bookFadeDuration;
        [UIView addKeyframeWithRelativeStartTime:(startTime + 0.3)/total relativeDuration:wordMoveDuration/total animations:^{
            // black square appears in between images (like map, phone, fb icon) and "what the world is reading now", hiding the images
            //  setting all the images to hidden or alpha 0 was buggy
            _blackBox.alpha = 1.0;
            
            _whiteBook.alpha = 1.0;
            newWhatTheWorld.frame = CGRectMake(_whatTheWorld.frame.origin.x, (120 + base)*SCALEUP, _whatTheWorld.frame.size.width, _whatTheWorld.frame.size.height);
            
            // len('is reading') < len('and sharing') so center where they meet left of the view vertical centerline
            // +- 10 is for this shifted centerline; -2 is slight gap between 'reading' and 'and'
            [self moveView:newIsReading inDirection:@"right" pixels:(MAIN_WIDTH/2-newIsReading.frame.size.width - newIsReading.frame.origin.x - 10)-2];
            [self moveView:newAndSharing inDirection:@"left" pixels:(newAndSharing.frame.origin.x - MAIN_WIDTH/2 + 10)-2];
            newNow.frame = CGRectMake(_now.frame.origin.x, (186 + base)*SCALEUP, _now.frame.size.width, _now.frame.size.height);
        }];
    } completion:^(BOOL finished){
        _redBook.hidden = YES;
        
        [self animatePushNotificationOnboarding];
    }];
}

- (void)animatePushNotificationOnboarding
{
    [UIView animateWithDuration:1.0 animations:^{
        [self moveView:_whiteBook inDirection:@"down" pixels:2*_offScreenDistance];
        [self moveView:_briefMe inDirection:@"down" pixels:2*_offScreenDistance];
        [self moveView:_whatTheWorld inDirection:@"up" pixels:2*_offScreenDistance];
        [self moveView:_isReading inDirection:@"up" pixels:2*_offScreenDistance];
        [self moveView:_andSharing inDirection:@"up" pixels:2*_offScreenDistance];
        [self moveView:_now inDirection:@"up" pixels:2*_offScreenDistance];
        
        [self moveView:_pushNotifText inDirection:@"down" pixels:2*_offScreenDistance];
        [self moveView:_okayButton inDirection:@"up" pixels:2*_offScreenDistance];
        
    } completion:^(BOOL finished) {
        _skipButton.hidden = NO; //but still alpha 0
        [UIView animateWithDuration:1.0 animations:^{
            _skipButton.alpha = 1.0;
        } completion:^(BOOL finished) {
        }];
        
        [Localytics tagScreen:@"Push Notifications Prompted"];
    }];
}

- (void)pushNotifOkay
{
    [_okayButton removeTarget:nil
                       action:NULL
             forControlEvents:UIControlEventAllEvents];
    
    [UIView animateWithDuration:0.5 animations:^{
        _skipButton.hidden = YES;
        _okayButton.backgroundColor = LIPSTICK_COLOR;
        _okayButton.layer.borderColor = LIPSTICK_COLOR.CGColor;
//        _okay.text = @"GET BRIEFED";
    } completion:^(BOOL finished) {
        [NSThread sleepForTimeInterval:1.0f];
        self.hidden = YES;
        [self queueSwipeTutorial];
    }];
    
    [[Delegate sharedDel].settingsvc.notificationSwitch setOn:YES];
    [[Delegate sharedDel].settingsvc toggleNotifications];
    
    [Localytics tagScreen:@"Push Notifications Accepted"];
}

- (void)pushNotifSkip
{
    [[Delegate sharedDel].settingsvc.notificationSwitch setOn:NO];
    [[Delegate sharedDel].settingsvc toggleNotifications];
    
    [Localytics tagScreen:@"Push Notifications Rejected"];
    self.hidden = YES;
    [self queueSwipeTutorial];
}

- (void)queueSwipeTutorial
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([Delegate sharedDel].briefingvc != nil && ![Delegate sharedDel].briefingvc.viewHasScrolled) {
            [Delegate sharedDel].briefingvc.scrollTutorial.hidden = NO;
        }
    });
}

- (void)moveView:(UIView *)view inDirection:(NSString *)direction pixels:(int)pixels
{
    if ([direction isEqualToString:@"up"])
        view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y - pixels, view.frame.size.width, view.frame.size.height);
    else if ([direction isEqualToString:@"down"])
        view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y + pixels, view.frame.size.width, view.frame.size.height);
    else if ([direction isEqualToString:@"left"])
        view.frame = CGRectMake(view.frame.origin.x - pixels, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
    else if ([direction isEqualToString:@"right"])
        view.frame = CGRectMake(view.frame.origin.x + pixels, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
}

- (UILabel *)createLabel:(NSString *)text withFrame:(CGRect)frame withAlignment:(NSString *)alignment
{
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    
    NSAttributedString *attributedNow =
    [[NSAttributedString alloc]
     initWithString: text
     attributes:
     @{
       NSFontAttributeName : [UIFont fontWithName:@"BrandonGrotesque-Medium" size:22],
       NSKernAttributeName : @(0.2f)
       }];
    [label setAttributedText:attributedNow];
    if ([alignment isEqualToString:@"center"])
        [label setTextAlignment:NSTextAlignmentCenter];
    else if ([alignment isEqualToString:@"left"])
        [label setTextAlignment:NSTextAlignmentLeft];
    else if ([alignment isEqualToString:@"right"])
        [label setTextAlignment:NSTextAlignmentRight];
    [label setTextColor:[UIColor whiteColor]];

    return label;
}

@end
