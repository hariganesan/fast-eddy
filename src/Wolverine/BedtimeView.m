//
//  BedtimeView.m
//  BriefMe
//
//  Created by Hari Ganesan on 3/13/15.
//  Copyright (c) 2015 Campion Designs, LLC. All rights reserved.
//

#import "BedtimeView.h"
#import "Delegate.h"
#import "SettingsViewController.h"

@implementation BedtimeView

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = NIGHT_GOGGLES_TRANSCOLOR;
        
        float offset = 0;
        if (MAIN_WIDTH == IPHONE_6_WIDTH)
            offset = 50;
        else if (MAIN_WIDTH == IPHONE_6PLUS_WIDTH)
            offset = 70;
        
        UIImage *nightImage = [UIImage imageNamed:@"night.png"];
        UIImageView *bedTimeImage = [[UIImageView alloc] initWithImage:nightImage];
        bedTimeImage.frame = CGRectMake(160*SCALEUP-nightImage.size.width/2, 80*SCALEUP+offset, nightImage.size.width, nightImage.size.height);
        [self addSubview:bedTimeImage];
        
        UILabel *bedTimeTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 120*SCALEUP+offset, MAIN_WIDTH, 40)];
        bedTimeTitle.font = [UIFont fontWithName:@"BrandonGrotesque-Bold" size:24];
        bedTimeTitle.text = @"It's bedtime!";
        bedTimeTitle.textAlignment = NSTextAlignmentCenter;
        bedTimeTitle.textColor = [UIColor whiteColor];
        [self addSubview:bedTimeTitle];

        UILabel *bedTimeSubTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 120*SCALEUP+40+offset, MAIN_WIDTH, 25)];
        bedTimeSubTitle.font = [UIFont fontWithName:@"BrandonGrotesque-Bold" size:18];
        bedTimeSubTitle.text = @"(Our bedtime at least...)";
        bedTimeSubTitle.textAlignment = NSTextAlignmentCenter;
        bedTimeSubTitle.textColor = [UIColor whiteColor];
        [self addSubview:bedTimeSubTitle];

        UILabel *bedTimePrompt = [[UILabel alloc] initWithFrame:CGRectMake(20*SCALEUP, 230*SCALEUP+offset, MAIN_WIDTH-40*SCALEUP, 80)];
        bedTimePrompt.font = [UIFont fontWithName:@"BrandonGrotesque-Bold" size:18];
        bedTimePrompt.text = @"Would you like to \ncontinue in night mode?";
        bedTimePrompt.textAlignment = NSTextAlignmentCenter;
        bedTimePrompt.textColor = [UIColor whiteColor];
        bedTimePrompt.numberOfLines = 2;
        [self addSubview:bedTimePrompt];

        UIButton *negative = [[UIButton alloc] initWithFrame:CGRectMake(45*SCALEUP, 330*SCALEUP+offset, 100*SCALEUP, 40*SCALEUP)];
        [negative setTitle:@"Not Yet" forState:UIControlStateNormal];
        [negative addTarget:self action:@selector(negativeResponse:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *positive = [[UIButton alloc] initWithFrame:CGRectMake(175*SCALEUP, 330*SCALEUP+offset, 100*SCALEUP, 40*SCALEUP)];
        [positive setTitle:@"Sure!" forState:UIControlStateNormal];
        [positive addTarget:self action:@selector(positiveResponse:) forControlEvents:UIControlEventTouchUpInside];
        
        for (UIButton *button in @[negative, positive]) {
            button.titleLabel.font = [UIFont fontWithName:@"BrandonGrotesque-Bold" size:18];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            button.layer.cornerRadius = 12;
            button.layer.borderWidth = 2.0f;
            button.layer.borderColor = [UIColor whiteColor].CGColor;
            button.backgroundColor = [UIColor clearColor];
            button.layer.borderColor = [UIColor whiteColor].CGColor;
            button.titleLabel.textAlignment = NSTextAlignmentCenter;
            [self addSubview:button];
        }
    }
    
    return self;
}

- (void)negativeResponse:(id)sender
{
    self.hidden = YES;
    // switch back to day mode
    [Delegate sharedDel].settingsvc.control.selectedSegmentIndex = 0;
    [[Delegate sharedDel].settingsvc toggleDisplay];
}

- (void)positiveResponse:(id)sender
{
    self.hidden = YES;
    // stay on auto mode
}

@end
