//
//  ArticleListViewController.m
//  MOST
//
//  Created by Hari Ganesan on 12/17/13.
//  Copyright (c) 2013 Campion Designs, LLC. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "ArticleListViewController.h"
#import "ArticleViewController.h"
#import "Delegate.h"
#import "Article.h"
#import "Parser.h"
#import "ArticleCell.h"
#import "NavController.h"
#import "BlurrableImageView.h"
#import "ODRefreshControl.h"
#import "HeaderView.h"
#import "ExplanationView.h"
#import "UITabBarController+hidable.h"
#import "TBKTabBarController.h"
#import "ScoreBannerView.h"
#import "NewsArticleCell.h"
#import "StreamArticleCell.h"
#import "Localytics.h"
#import "BriefingListViewController.h"
#import "FeedbackCell.h"
#import "NewsArticleInnerCellView.h"
#import "StreamArticleInnerCellView.h"
#import "AFHTTPRequestOperationManager.h"

@implementation ArticleListViewController

- (id)initWithSection:(SectionType)section andTitle:(NSString *)title
{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        // navbar title
        _section = section;
		self.title = title;
				
		_initialParse = YES;
		_articlesLoaded = NO;
		
		_fakeScroll = NO;
		_animatingCell = NO;
		
		// navigation
		_navigationInView = YES;
		_expandedIndices = [[NSMutableDictionary alloc] init];
		
        // set up local pointer for rendering
        _objects = [[[Delegate sharedDel].parsedNews objectForKey:BM_ROUTES[_section]] objectForKey:@"articles"];
		
        // lets set up a new view for self.view and a separate subview for self.tableViewV2
        // this is a slight hack to keep uitableviewcontroller functionality and create a fixed header as well as a floating header
        // IMPORTANT: DO NOT USE self.tableView ANYWHERE! It is cast as a UIView below since ARC makes self.view == self.tableView
        // this way, we can add other subviews to self.view and not to self.tableView
        _tableViewV2 = self.tableView;
        UIView *replacementView = [[UIView alloc] initWithFrame:self.tableView.frame];
        self.view = replacementView;
        self.tableViewV2.backgroundColor = [UIColor clearColor];
        self.tableViewV2.separatorStyle = UITableViewCellSeparatorStyleNone;
		
		[_tableViewV2 setShowsVerticalScrollIndicator:NO];
		
		CGFloat secondBarHeight;
		
		if (section == SECTION_BRIEFING || section == SECTION_STREAM) {
			secondBarHeight = 0;
		} else {
			secondBarHeight = NAVIGATION_BAR_HEIGHT;
		}
		
		_tableViewV2.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, MAIN_HEIGHT-[self tableView:_tableViewV2 heightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] - NAVIGATION_BAR_HEIGHT - secondBarHeight)];
		
		// extend tableview off screen on top & bottom, but offset the increase using contentInset (like padding); allows an extra cell
		//  on top and on bottom to be load and be in 'visible cells' so that cells can slide from offscreen during cell expansion animations
		CGRect adjustedFrame = self.tableViewV2.frame;
		adjustedFrame.size.height += 2*[Delegate sharedDel].tableViewFrameOffset;
		adjustedFrame.origin.y -= [Delegate sharedDel].tableViewFrameOffset;
		_tableViewV2.frame = adjustedFrame;
		_tableViewV2.contentInset = UIEdgeInsetsMake([Delegate sharedDel].tableViewFrameOffset, 0, [Delegate sharedDel].tableViewFrameOffset, 0);
		_tableViewV2.contentOffset = CGPointMake(0, -[Delegate sharedDel].tableViewFrameOffset);
		
		// gradient overlays image
		UIImageView *gradientView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gradientoverlay.png"]];
		gradientView.frame = CGRectMake(0, 0, MAIN_WIDTH, MAIN_HEIGHT);

        // background at top behind refreshcontrol (and not rest of view)
        CGRect refreshViewFrame = CGRectMake(0, -320, self.view.frame.size.width, 320);
        UIView *refreshView = [[UIView alloc] initWithFrame:refreshViewFrame];
        refreshView.backgroundColor = [UIColor blackColor];
        [_tableViewV2 insertSubview:refreshView atIndex:0];
		
		// init empty bg image frame; img set once route loads
        _bgImageView = [[BlurrableImageView alloc] initWithFrame:CGRectMake(0, 0, MAIN_WIDTH, MAIN_HEIGHT)];
		
		UIView *blackBackground = [[UIView alloc] initWithFrame:_bgImageView.frame];
		blackBackground.backgroundColor = [UIColor blackColor];
		
        // attach all elements to the main view
		[self.view addSubview:blackBackground];
        [self.view addSubview: _bgImageView];
		[self.view addSubview:gradientView];
        [self.view addSubview:_tableViewV2];
		
		// header view with title and white line
		_hv = [[HeaderView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100*SCALEUP)];
		_hv.title = [title uppercaseString];
		[self.view addSubview:_hv];
		
		_tableViewV2.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(_tableViewV2.frame.size.height, 0, MAIN_WIDTH, 54)];
		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(replaceData) name:[NSString stringWithFormat:@"finishedParsingNews %@", BM_ROUTES[_section]] object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetCells) name:@"resetCells" object:nil];
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	[self refreshTimes];
	if ([_refreshTimer isValid]) {
		[_refreshTimer invalidate];
	}
	
	_refreshTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(refreshTimes) userInfo:nil repeats:YES];

}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	
	[_refreshTimer invalidate];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _customRefreshControl = [[ODRefreshControl alloc] initInScrollView:(UIScrollView *)self.view];
    [_customRefreshControl addTarget:self action:@selector(refresh)
             forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:_customRefreshControl];
}

- (void)refresh
{
	[[Delegate sharedDel].parser parseObjectsWithSection:_section];
	_refreshStart = [NSDate date];
}

- (void)updateTableViewData
{
	[_customRefreshControl endRefreshing];
	
	[_expandedIndices removeAllObjects];
	[_tableViewV2 reloadData];
    [self.view setNeedsDisplay];
	
	// localytics: log event
	if (!_initialParse) {
		NSTimeInterval parseTime = [[NSDate date] timeIntervalSinceDate:[[Delegate sharedDel] parser].parseStart];
		[Localytics tagEvent:@"Parse Refresh" attributes:@{@"interval": [NSString stringWithFormat:@"%f", parseTime*1000], @"section": BM_ROUTES[self.section]}];
	}
	
	_initialParse = NO;
}

// called whenever parser finished for this section; replaces bg img and articles
- (void)replaceData
{
	_objects = [[[Delegate sharedDel].parsedNews objectForKey:BM_ROUTES[_section]] objectForKey:@"articles"];
	if (_objects == nil || [_objects count] == 0) {
		NSLog(@"news for section %@ has no articles", BM_ROUTES[_section]);
		return;
	}
	
	if ([self needToUpdateBackgroundImage]) {
		[self updateBackgroundImage];
	}
	[self updateTableViewData];
	_articlesLoaded = YES;
}

- (BOOL)needToUpdateBackgroundImage
{
	NSString *newFirstArticleUrl = ((Article *)[_objects objectAtIndex:0]).url;
	// if #1 article or the imageNumber for the #1 article has changed return YES, else NO
	// on first image load _bgImageView.correspondingArticleUrl (and .imageUrl) will be nil so it will return YES
	return (![_bgImageView.correspondingArticleUrl isEqual:newFirstArticleUrl] || ![[self buildBgImageUrl] isEqual:_bgImageView.imageUrl]);
}

- (NSString *)buildBgImageUrl
{
	NSString *screen;
	if (MAIN_HEIGHT == IPHONE_4_HEIGHT) {
		screen = @"4";
	} else if (MAIN_HEIGHT == IPHONE_5_HEIGHT) {
		screen = @"5";
	} else if (MAIN_HEIGHT == IPHONE_6_HEIGHT) {
		screen = @"6";
	} else {
		screen = @"6p";
	}
	
	NSNumber *imageNumber = [[[Delegate sharedDel].parsedNews objectForKey:BM_ROUTES[_section]] objectForKey:@"imageNumber"];
	
	NSString *url = [NSString stringWithFormat:@"%@image?section=%@&id=%@&screen=%@", NEWS_URL, BM_ROUTES[self.section], imageNumber, screen];
	return url;
}


- (void)updateBackgroundImage
{
	Article *firstArticle = (Article *)[_objects objectAtIndex:0];
	
	_bgImageView.correspondingArticleUrl = firstArticle.url;
	_bgImageView.imageUrl = [self buildBgImageUrl];
	
	AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
	manager.responseSerializer = [AFImageResponseSerializer serializer];
	[manager GET:_bgImageView.imageUrl parameters:nil success:^(AFHTTPRequestOperation *operation, UIImage *im) {
		[_bgImageView setOriginalImageToBlur:im];
		
		// if no URLS or all URLS failed, use backup img
	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		NSLog(@"Unable to grab image from section %@.. using backup", BM_ROUTES[_section]);
		
		UIImage *backupIm = [UIImage imageNamed:[NSString stringWithFormat:@"%@-second-backup.jpg", BM_ROUTES[self.section]]];
		[_bgImageView setOriginalImageToBlur:backupIm];
	}];
	
	[_bgImageView setBounds:CGRectMake(0,0,MAIN_WIDTH,MAIN_HEIGHT)];
	// need to crop pictures that extend over left/right and then top/bottom boundaries of view
	_bgImageView.contentMode = UIViewContentModeScaleAspectFit;
	[_bgImageView clipsToBounds];
	[_bgImageView setNeedsDisplay];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    // (just returns number of articles)
    return [_objects count];
}

- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if ([_expandedIndices objectForKey:[NSNumber numberWithInteger:indexPath.row]]) {
		return 140*SCALEUP+[[_expandedIndices objectForKey:[NSNumber numberWithInteger:indexPath.row]] intValue];
	}
	
	return 140*SCALEUP;
}

// populate and display table cells
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{	
	Article *art = (Article *)[_objects objectAtIndex:indexPath.row];
	NSString *cellIdentifier = @"Cell";
    ArticleCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
		
    // (initial load)
    if (cell == nil) {
		if ([art isKindOfClass:[NewsArticle class]]) {
			cell = [[NewsArticleCell alloc] initWithReuseIdentifier:cellIdentifier];
			[((NewsArticleCell *)cell).explanationViewOpenButton addTarget:self action:@selector(displayExplanation:) forControlEvents:UIControlEventTouchUpInside];
		} else {
			cell = [[StreamArticleCell alloc] initWithReuseIdentifier:cellIdentifier];
		}

		UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(toggleCellExpansion:)];
		lpgr.minimumPressDuration = 0.8;
		[cell.contentView addGestureRecognizer:lpgr];
		[lpgr setDelegate:cell];
    }
	
	// set description
	if ([_expandedIndices objectForKey:[NSNumber numberWithInteger:indexPath.row]]) {
		cell.snippet.hidden = NO;
	} else {
		cell.snippet.hidden = YES;
	}	
	
	cell.row = indexPath.row;

	cell.bubble.frame = CGRectMake(cell.bubble.frame.origin.x, cell.bubble.frame.origin.y, cell.bubble.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]-10*SCALEUP);
	
	CGRect oldFrame = cell.snippet.frame;
	[cell.snippet setNumberOfLines:8];
	cell.snippet.text = art.snippet;
	[cell.snippet sizeToFit];
	cell.snippet.frame = CGRectMake(cell.snippet.frame.origin.x, cell.snippet.frame.origin.y, oldFrame.size.width, cell.snippet.frame.size.height);
	
	if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"articleIDs"] objectForKey:art.title]) {
		[cell setVisited:YES animated:NO];
	} else {
		[cell setVisited:NO animated:NO];
	}
	
    return cell;
}

- (void)displayExplanation:(id)sender
{
	[_explanationView displayExplanation:sender];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

	UIViewController *vc;
	
	if (_section == SECTION_STREAM) { // stream display article directly
		
		StreamArticleCell *cell = (StreamArticleCell *) [tableView cellForRowAtIndexPath:indexPath];
		// store visited info in nsuserdefaults data
		if (!cell.visited) {
			NSMutableDictionary *articleIDs = [[[NSUserDefaults standardUserDefaults] objectForKey:@"articleIDs"] mutableCopy];
			[articleIDs setObject:[NSDate date] forKey:cell.streamArticleInnerCellView.title.text];
			[[NSUserDefaults standardUserDefaults] setObject:articleIDs forKey:@"articleIDs"];
		}
		vc = [[ArticleViewController alloc] initWithArticle:[_objects objectAtIndex:indexPath.row]];

		
	} else {
		NewsArticleCell *cell = (NewsArticleCell *) [tableView cellForRowAtIndexPath:indexPath];
		// store visited info in nsuserdefaults data
		if (!cell.visited) {
			NSMutableDictionary *articleIDs = [[[NSUserDefaults standardUserDefaults] objectForKey:@"articleIDs"] mutableCopy];
			[articleIDs setObject:[NSDate date] forKey:cell.newsArticleInnerCellView.title.text];
			[[NSUserDefaults standardUserDefaults] setObject:articleIDs forKey:@"articleIDs"];
		}

		vc = [[ArticleViewController alloc] initWithArticle: (NewsArticle *)[_objects objectAtIndex:indexPath.row]];
		
	}
	
	// reload visited cell so it displays when you go back
	[tableView beginUpdates];
	[tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
	[tableView endUpdates];

	if ([Delegate sharedDel].nc.topViewController != vc) {
		[Delegate sharedDel].nc.currentViewController = vc;
		[[Delegate sharedDel].nc pushViewController:vc animated:YES];
	}
}

- (void)toggleCellExpansion:(UILongPressGestureRecognizer *)gr
{
	if (gr.state == UIGestureRecognizerStateBegan) {
		ArticleCell *cell = (ArticleCell *)gr.delegate;
		
		if (_animatingCell)
			return;
		_animatingCell = YES;
		
		CGRect cellLocation = [_tableViewV2 convertRect:[_tableViewV2 rectForRowAtIndexPath:[_tableViewV2 indexPathForCell:cell]] toView:[_tableViewV2 superview]];
		float visibleHeight = MAIN_HEIGHT;
		if (_navigationInView)
			visibleHeight -= NAVIGATION_BAR_HEIGHT;
		if (_section != SECTION_BRIEFING && _section != SECTION_STREAM)
			visibleHeight -= NAVIGATION_BAR_HEIGHT;
				
		// cell is expanding
		if (![_expandedIndices objectForKey:[NSNumber numberWithInteger:cell.row]]) {
			[_expandedIndices setObject:[NSNumber numberWithInteger:cell.snippet.frame.size.height+10*SCALEUP] forKey:[NSNumber numberWithInteger:cell.row]];

			// if expanding the cell will expand it off-screen (or behind nav bar)...
			//   above
			float offScreenDistance = (cellLocation.origin.y + cellLocation.size.height + cell.snippet.frame.size.height+10*SCALEUP) - visibleHeight;
			if (offScreenDistance < 0)
				offScreenDistance = 0;

			[UIView animateWithDuration:0.5 animations:^{
				// grow the current cell
				cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height + cell.snippet.frame.size.height+10*SCALEUP);
				cell.bubble.frame = CGRectMake(cell.bubble.frame.origin.x, cell.bubble.frame.origin.y, cell.bubble.frame.size.width, cell.bubble.frame.size.height + cell.snippet.frame.size.height+10*SCALEUP);

				// change the blurred bg image and parallax/blurred title to reflect any scrolling
				CGFloat blurOffset = _tableViewV2.contentOffset.y + offScreenDistance + [Delegate sharedDel].tableViewFrameOffset;
				NSInteger blurIndex = [self calculateBlurIndexFromOffset:blurOffset];
				[_bgImageView updateImageToBlurIndex:blurIndex animated:YES withDuration:0.5 forTheFirstTime:NO];
				[_bgImageView adjustAlphaWithOffset:blurOffset];
				[_hv animateDisappear:blurOffset];
				
				if (_section == SECTION_BRIEFING)
					[(BriefingListViewController *)self animateBannerWithOffset:_tableViewV2.contentOffset.y + offScreenDistance];
				for (UITableViewCell *realCell in self.tableViewV2.visibleCells) {
					ArticleCell* curCell = (ArticleCell *)realCell;
					CGRect newFrame = curCell.frame;

					// move up the current cell and cells above it if the current cell needs to move up
					if (curCell.frame.origin.y <= cell.frame.origin.y && offScreenDistance != 0)
						newFrame.origin.y -= offScreenDistance;
					// move down cells below current cell down
					else if (curCell.frame.origin.y > cell.frame.origin.y)
						newFrame.origin.y += cell.snippet.frame.size.height+10*SCALEUP;
					curCell.frame = newFrame;
				}
				
			} completion:^(BOOL finished) {
				// if we needed to move the cell and cells above it upwards (because the cell would've gone off-screen when expanded)
				//  offset this move upwards by scrolling upwards the same distance that they were moved. the bg blur img is changed
				//  above to reflect this fake scrolling.
				_fakeScroll = YES;
				_tableViewV2.contentOffset = CGPointMake(_tableViewV2.contentOffset.x, _tableViewV2.contentOffset.y + offScreenDistance);
				_fakeScroll = NO;
				cell.snippet.hidden = NO;
				[_tableViewV2 reloadData];
				_animatingCell = NO;
				
				Article *article = [self.objects objectAtIndex:cell.row];
				article.expanded = YES;
				NSMutableDictionary *attrs = [NSMutableDictionary dictionaryWithDictionary:[article getAttributes]];
				[Localytics tagEvent:@"Article Expanded" attributes:attrs];
			}];

		// shrinking
		} else {
			[_expandedIndices removeObjectForKey:[NSNumber numberWithInteger:cell.row]];
			cell.snippet.hidden = YES;
			
			BOOL lastCellVisible = NO;
			NSIndexPath *lastVisibleCellIndexPath = [[_tableViewV2 indexPathsForVisibleRows] lastObject];
			NSIndexPath *lastCellIndexPath = [NSIndexPath indexPathForRow:[_tableViewV2 numberOfRowsInSection:0] - 1 inSection:0];
			if ([lastVisibleCellIndexPath isEqual:lastCellIndexPath])
				lastCellVisible = YES;
			
			[UIView animateWithDuration:0.5 animations:^{
				
				// if the last cell is visible, and we are shrinking a cell, shrink them downwards so that the last cell doesn't extend
				//  away from the bottom of the screen leaving a gap (then snap back down)
				if (lastCellVisible) {
					// shrink the current cell
					cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height - cell.snippet.frame.size.height-10*SCALEUP);
					cell.bubble.frame = CGRectMake(cell.bubble.frame.origin.x, cell.bubble.frame.origin.y, cell.bubble.frame.size.width, cell.bubble.frame.size.height - cell.snippet.frame.size.height-10*SCALEUP);
					
					// move down the cells above the current cell
					for (UITableViewCell *cellBelow in self.tableViewV2.visibleCells) {
						ArticleCell* aCellBelow = (ArticleCell *)cellBelow;
						if (aCellBelow.frame.origin.y <= cell.frame.origin.y) {
							CGRect newFrame = aCellBelow.frame;
							newFrame.origin.y += cell.snippet.frame.size.height+10*SCALEUP;
							aCellBelow.frame = newFrame;
						}
					}
				}
				
				// if not, shrink the cell upwards
				else {
					// shrink the current cell
					cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height - cell.snippet.frame.size.height-10*SCALEUP);
					cell.bubble.frame = CGRectMake(cell.bubble.frame.origin.x, cell.bubble.frame.origin.y, cell.bubble.frame.size.width, cell.bubble.frame.size.height - cell.snippet.frame.size.height-10*SCALEUP);
					
					// move up the cells below the current cell
					for (UITableViewCell *cellBelow in self.tableViewV2.visibleCells) {
						ArticleCell* aCellBelow = (ArticleCell *)cellBelow;
						if (aCellBelow.frame.origin.y <= cell.frame.origin.y)
							continue;
						CGRect newFrame = aCellBelow.frame;
						newFrame.origin.y -= cell.snippet.frame.size.height+10*SCALEUP;
						aCellBelow.frame = newFrame;
					}
				}
			} completion:^(BOOL finished) {
				
				if (lastCellVisible) {
					_fakeScroll = YES;
					_tableViewV2.contentOffset = CGPointMake(_tableViewV2.contentOffset.x, _tableViewV2.contentOffset.y - cell.snippet.frame.size.height-10*SCALEUP);
					_fakeScroll = NO;
				}
				[_tableViewV2 reloadData];
				_animatingCell = NO;
			}];
		}
	}
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    ScrollDirection scrollDirection = ScrollDirectionNone;
    if (self.lastContentOffset < self.tableViewV2.contentOffset.y) {
        scrollDirection = ScrollDirectionDown;
    } else if (self.lastContentOffset > self.tableViewV2.contentOffset.y) {
        scrollDirection = ScrollDirectionUp;
    }
    
    self.lastContentOffset = self.tableViewV2.contentOffset.y;
		
	if (self.tableViewV2.contentOffset.y < (-200 - [Delegate sharedDel].tableViewFrameOffset)) {
		self.tableViewV2.contentOffset = CGPointMake(0, -200 - [Delegate sharedDel].tableViewFrameOffset);// CGPointZero;
		return;
	}
	
    // show/hide navigation items
    if (scrollDirection == ScrollDirectionUp || self.tableViewV2.contentOffset.y <= -1*[Delegate sharedDel].tableViewFrameOffset) {
		[self showNavigation];
    } else if (scrollDirection == ScrollDirectionDown) {
		[self hideNavigation];
    }
	
	CGFloat blurOffset = self.tableViewV2.contentOffset.y + [Delegate sharedDel].tableViewFrameOffset;
	[_hv animateDisappear:blurOffset];
	[_bgImageView adjustAlphaWithOffset:blurOffset];
	NSInteger blurIndex = [self calculateBlurIndexFromOffset:blurOffset];
	[_bgImageView updateImageToBlurIndex:blurIndex animated:NO withDuration:0.0 forTheFirstTime:NO];
}

- (void)showNavigation
{
	if (_fakeScroll)
		return;
    if (!_navigationInView) {
		if (_section == SECTION_BRIEFING || _section == SECTION_STREAM) {
			[self.tabBarController showNavigation];
		} else {
			[[Delegate sharedDel].tabsectvc showNavigation];
		}
		
        _navigationInView = YES;
    }
}

- (void)hideNavigation
{
	if (_fakeScroll)
		return;
    if (_navigationInView) {
		if (_section == SECTION_BRIEFING || _section == SECTION_STREAM) {
			[self.tabBarController hideNavigation];
		} else {
			[[Delegate sharedDel].tabsectvc hideNavigation];
		}
		
        _navigationInView = NO;
    }
}

- (void)resetCells
{
	// loop thru all cells setting their colors in case user changed theme in settings
	for (UITableViewCell *curCell in self.tableViewV2.visibleCells) {
		[curCell setHighlighted:curCell.highlighted];
	}
}

- (void)refreshTimes
{
	// update the displayedAges and ageUnits in the article objects
	for (int i = 0; i < [self.tableViewV2 numberOfRowsInSection:0]; i++) {
		Article *art = (Article *)[_objects objectAtIndex:i];
		[[Delegate sharedDel].parser updateDisplayedAge:art];
	}
}

- (NSInteger)calculateBlurIndexFromOffset:(CGFloat)scrollOffset
{
	long bgBlurIndex = lround((scrollOffset) / BLURSPEED);
	if (bgBlurIndex < 0)
		bgBlurIndex = 0;
	if (bgBlurIndex > (NUMBLURIMAGES - 1))
		bgBlurIndex = (NUMBLURIMAGES - 1);
	return bgBlurIndex;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
	return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
	return NO;
}

- (BOOL)prefersStatusBarHidden
{
	return YES;
}

@end
