//
//  SettingsViewController.h
//  MOST
//
//  Created by Hari Ganesan on 6/19/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

/** @discussion Shows settings full screen view that has
    theme, notifications toggle, feedback, etc. */
@interface SettingsViewController : UITableViewController <MFMailComposeViewControllerDelegate>

typedef enum SettingsRow {
    SettingsRowFAQ,
    SettingsRowPrivacyPolicy,
    SettingsRowHelpAndFeedback,
    SettingsRowShareThisApp
} SettingsRow;

typedef enum Theme {
    THEME_DAY,
    THEME_NIGHT,
    THEME_AUTO
} Theme;

/** @brief The top part of the view. */
@property (nonatomic, strong) UIView *tableHeaderView;
/** @brief The control for the theme (day, night, auto). */
@property (nonatomic, strong) UISegmentedControl *control;
/** @brief Explains the auto view when selected on the <code>control</code>. */
@property (nonatomic, strong) UILabel *autoExplanation;
/** @brief The switch for toggling notifications on and off. */
@property (nonatomic, strong) UISwitch *notificationSwitch;
/** @discussion Set to YES each time the controller is initialized. This
    prevents the events from being logged on each init. */
@property (nonatomic) BOOL initialLoad;

/** @discussion Toggles the display (not the control), setting the themes
    within the Delegate as well as individual view colors. This should
    immediately be called after changing the <code>control</code>. */
- (void)toggleDisplay;
/** @discussion Toggles notifications, (not the switch), registering
    for notifications if necessary. This should be immediately called after
    changing the <code>notificationSwitch</code> **/
- (void)toggleNotifications;

@end
