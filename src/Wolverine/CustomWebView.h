//
//  CustomWebView.h
//  BriefMe
//
//  Created by Charlie Vrettos on 6/26/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import <WebKit/WebKit.h>
#import <UIKit/UIKit.h>
#import "Delegate.h"

#define READ_MORE_KEY @"readMore"
#define OPEN_LINK_KEY @"openLink"
#define OPEN_IMAGE_KEY @"openImage"
#define SCROLL_KEY @"scroll"
#define LOADED_DOM_KEY @"loadedDOM"

@class BottomNavigationBarView;

/** @discussion a custom WKWebview that is used for mobile web views.
    A subclass `WebArticleView` is used for the generic view, the 
    subclass `BriefMeArticleView` is used for the briefmeview, and
    `WebBrowserViewController` uses this for additional navigation. */
@interface CustomWebView : WKWebView

@property (nonatomic, strong) UIActivityIndicatorView *activityView;
/** @brief The background behind the status bar. */
@property (nonatomic, strong) UIView *statusBarBackground;
/** @discussion A screenshot of the status bar. This is used
 during the transition between the article viewcontroller
 and the article list viewcontroller. */
@property (nonatomic, strong) UIView *statusBarScreenshot;
/** @discussion A view embedded at the top of the webview so
 that it scrolls up as if it is part of the website/content */

// could use nsnotification to tell controller that scroll occured
// instead of passing this in, but that many notifications would
// likely be buggy
/** @discussion Navigation Bar View of controller. Shown/hidden
 upon scroll. */
@property (nonatomic, strong) BottomNavigationBarView *navBar;
@property (nonatomic, assign) CGFloat lastContentOffset;

- (id)initWithFrame:(CGRect)frame andWithNavBar:(BottomNavigationBarView *)navBar andWithWebConfig:(WKWebViewConfiguration *)webConfig;
- (void)loadFromString:(NSString *)html;
- (void)loadFromURL:(NSURL *)url;
/** @brief Called when WKNavigation calls didFinishNavigation */
- (void)finishedNavigation;
/** @description Builds a WKWebViewConfiguration with the requested
    javascript userscripts added. WebConfig should be passed to
    init function after created. */
+ (WKWebViewConfiguration *)createConfigWithScriptsForUrl:(NSURL *)url scroll:(BOOL)scroll scrollKey:(NSString *)scrollKey openLink:(BOOL)openLink openImage:(BOOL)openImage loadedDOM:(BOOL)loadedDOM scale:(BOOL)scale nightMode:(BOOL)nightMode webBlocks:(BOOL)webBlocks;
- (BOOL)draggingOrBouncing;

@end
