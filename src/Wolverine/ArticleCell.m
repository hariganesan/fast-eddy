//
//  ArticleCell.m
//  MOST
//
//  Created by Hari Ganesan on 6/20/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import "Article.h"
#import "ArticleCell.h"
#import "Delegate.h"
#import "NewsArticleInnerCellView.h"

@implementation ArticleCell

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        // CGRectMake: (x, y, width, height)
        self.backgroundColor = [UIColor clearColor];

        _bubble = [[UIView alloc] initWithFrame:CGRectMake(15*SCALEUP, 5*SCALEUP, (self.frame.size.width-30)*SCALEUP, 130*SCALEUP)]; // reset in viewcontroller due to expanding cell
        _bubble.layer.cornerRadius = 12;
        [self.contentView addSubview:_bubble];
        
        _snippet = [[UILabel alloc] initWithFrame:CGRectMake(35*SCALEUP, 130*SCALEUP, 250*SCALEUP, 85*SCALEUP)];
        [_snippet setFont: [UIFont fontWithName:@"BrandonGrotesque-Regular" size:14]];
        [_snippet setBackgroundColor:[UIColor clearColor]];
        [_snippet setNumberOfLines:0];
        [self.contentView addSubview:_snippet];
        _snippet.hidden = YES;
        
        // overwrites default highlighting action
        UIView *bgColorView = [[UIView alloc] initWithFrame:CGRectMake(15*SCALEUP,5,self.frame.size.width-30, 125-10)];
        bgColorView.backgroundColor = [UIColor clearColor];
        bgColorView.layer.cornerRadius = 0;
        bgColorView.layer.masksToBounds = YES;
        [self setSelectedBackgroundView:bgColorView];
    }
    return self;
}

// turn cell gray if visited
- (void)setVisited:(BOOL)visited animated:(BOOL)animated
{
    [self setVisited:visited];
    
    if (visited) {
        [_bubble setBackgroundColor:[Delegate sharedDel].cellBackgroundVisitedColor];
    } else {
        [_bubble setBackgroundColor:[Delegate sharedDel].cellBackgroundColor];
    }
}

// touch followed by a drag gesture (not selected)
- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    
    if (highlighted) {
        [_bubble setBackgroundColor:[Delegate sharedDel].cellBackgroundHighlightColor];
        [_snippet setTextColor:[Delegate sharedDel].cellHighlightColor];
    } else {
        [self setVisited:_visited animated:NO];
        [_bubble setBackgroundColor:[Delegate sharedDel].cellBackgroundColor];
        [_snippet setTextColor:[Delegate sharedDel].cellTitleColor];
    }
    
    [self.contentView setNeedsDisplay];
}

@end
