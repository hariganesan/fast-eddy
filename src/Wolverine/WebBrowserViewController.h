//
//  WebBrowserViewController.h
//  BriefMe
//
//  Created by Charlie Vrettos on 6/26/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import <WebKit/WebKit.h>
#import <UIKit/UIKit.h>

@class BottomNavigationBarView, CustomWebView;

/** @discussion This controller is pushed onto the stack when a link is followed from
    the BriefMe View. */
@interface WebBrowserViewController : UIViewController<WKNavigationDelegate, WKScriptMessageHandler, WKUIDelegate>

/** @brief The navigation bar with a button to swap article views */
@property (nonatomic, strong) BottomNavigationBarView *bar;

@property (nonatomic, strong) CustomWebView *webView;
/** @brief The URL of the link tapped to open WBVC. Used for sharing. */
@property (nonatomic) NSURL *startURL;

/** @brief The background behind the status bar. */
@property (nonatomic, strong) UIView *statusBarBackground;

- (id)initWithUrl:(NSURL *)url;

@end
