//
//  LoadingView.m
//  MOST
//
//  Created by Charlie Vrettos on 10/9/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import "LoadingView.h"
#import "Localytics.h"

@implementation LoadingView

- (id)init
{
    self = [super initWithFrame:CGRectMake(0, 0, MAIN_WIDTH, MAIN_HEIGHT)];
    
    if (self) {
        _loadCount = 0;
        _animFinished = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hide) name:@"finishedParsingNews top" object:nil];
        
        self.backgroundColor = [UIColor blackColor];
        self.userInteractionEnabled = YES;
        
        _bookHolder = [[UIView alloc] initWithFrame:CGRectMake(MAIN_WIDTH/2 - 200/2, -500, 200, 200)];
        _bookHolder.backgroundColor = [UIColor blackColor];
        [self addSubview:_bookHolder];
        
        _book = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blank-flip.png"]];
        [_bookHolder addSubview:_book];

        _book.frame = CGRectMake(0, 0, 180, 180);
        _book.center = CGPointMake(_bookHolder.frame.size.width/2, _bookHolder.frame.size.height/2);
        
        NSString *text = @"BRIEFME";
        // make 'BRIEF' bold
        UIFont *boldFont = [UIFont fontWithName:@"BrandonGrotesque-Bold" size:20];
        UIFont *regularFont = [UIFont fontWithName:@"BrandonGrotesque-Regular" size:20];
        NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys: regularFont, NSFontAttributeName, nil];
        NSDictionary *subAttrs = [NSDictionary dictionaryWithObjectsAndKeys: boldFont, NSFontAttributeName, nil];
        NSRange range = NSMakeRange(0,5); //range of BRIEF
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text attributes:attrs];
        [attributedText setAttributes:subAttrs range:range];
        
        _BriefMe = [[UILabel alloc] initWithFrame:CGRectMake(MAIN_WIDTH/2-150/2, MAIN_HEIGHT-115, 150, 30)];
        [_BriefMe setTextColor:[UIColor whiteColor]];
        [_BriefMe setAttributedText:attributedText];
        _BriefMe.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_BriefMe];
        _BriefMe.alpha = 0.0;
        
        text = @"what the world is reading now";
        // make 'now' bold
        regularFont = [UIFont fontWithName:@"BrandonGrotesque-Regular" size:14];
        boldFont = [UIFont fontWithName:@"BrandonGrotesque-Bold" size:14];
        attrs = [NSDictionary dictionaryWithObjectsAndKeys: regularFont, NSFontAttributeName, nil];
        subAttrs = [NSDictionary dictionaryWithObjectsAndKeys: boldFont, NSFontAttributeName, nil];
        range = NSMakeRange(26,3); //range of now
        attributedText = [[NSMutableAttributedString alloc] initWithString:text attributes:attrs];
        [attributedText setAttributes:subAttrs range:range];
        
        _WhatTheWorld = [[UILabel alloc] initWithFrame:CGRectMake(MAIN_WIDTH/2-300/2, MAIN_HEIGHT-92, 300, 30)];
        [_WhatTheWorld setTextColor:[UIColor whiteColor]];
        _WhatTheWorld.textAlignment = NSTextAlignmentCenter;
        [_WhatTheWorld setAttributedText:attributedText];
        [self addSubview:_WhatTheWorld];
        _WhatTheWorld.alpha = 0.0;
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)animate
{
    _bookHolder.frame = CGRectMake(_bookHolder.frame.origin.x, MAIN_HEIGHT-310, _bookHolder.frame.size.width, _bookHolder.frame.size.height);

    [UIView animateWithDuration:0.2 animations:^{
        _BriefMe.alpha = 1.0;
        _WhatTheWorld.alpha = 1.0;
    } completion:^(BOOL finished) {
    }];

    _animFinished = YES;
    // if already finished parsing, hide anim
    if (_shouldHideAnim) {
        [NSThread sleepForTimeInterval:0.5];
        self.hidden = YES;
        
        // else start flipping pages
    } else {
        NSArray *imageNames = @[@"flip0.png", @"flip1.png", @"flip2.png", @"flip3.png", @"flip4.png", @"flip5.png", @"flip6.png", @"flip7.png", @"flip8.png", @"flip9.png", @"flip10.png", @"flip11.png", @"flip12.png", @"flip13.png", @"flip14.png", @"flip15.png", @"flip16.png", @"flip17.png", @"flip18.png", @"flip19.png", @"flip20.png", @"flip21.png", @"flip22.png", @"flip23.png", @"flip24.png"];
        
        NSMutableArray *images = [[NSMutableArray alloc] init];
        for (int i = 0; i < imageNames.count; i++) {
            [images addObject:[UIImage imageNamed:[imageNames objectAtIndex:i]]];
        }
        _book.animationImages = images;
        _book.animationDuration = 1.0;
        [_book startAnimating];
    }
}


- (void)hide
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    // if anim already finished, hide it; if not,
    if (_animFinished) {
        self.hidden = YES;
        [_book stopAnimating];
        // if not, set _shouldHideAnim so it hides when it finishes
    } else {
        _shouldHideAnim = YES;
    }
    
    [[Delegate sharedDel] appLoaded];
}



@end
