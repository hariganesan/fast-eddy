//
//  DeprecationViewController.h
//  BriefMe
//
//  Created by Charlie Vrettos on 7/30/15.
//  Copyright © 2015 BriefMe Media, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @discussion A controller that appears when the app
    version is out of date, as specified in the back end.
    Redirects users to BriefMe on the App Store. */

@protocol Deprecation

-(void)dismissDC;

@end

@interface DeprecationViewController : UIViewController

@property (nonatomic, weak) id<Deprecation> delegate;
- (instancetype)initWithShutDown:(BOOL)shutDown;

@end
