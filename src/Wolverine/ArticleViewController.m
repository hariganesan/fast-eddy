//
//  ArticleViewController.m
//  MOST
//
//  Created by Todd Lubin on 7/17/12.
//  Copyright (c) 2012 Campion Designs, LLC. All rights reserved.
//

#import "ArticleViewController.h"
#import "Delegate.h"
#import "NewsArticle.h"
#import "NavController.h"
#import "Localytics.h"
#import "WebArticleView.h"
#import "StreamArticleInnerCellView.h"
#import "NewsArticleInnerCellView.h"
#import "BottomNavigationBarView.h"
#import "BriefMeArticleView.h"
#import "ZoomableImageView.h"
#import "ArticleHeaderView.h"
#import "SettingsViewController.h"
#import "ExtraNavigationBarView.h"

@interface ArticleViewController ()

- (void)flashMainButton:(NSUInteger)flashes;

@end

@implementation ArticleViewController

- (id)initWithArticle:(Article *)art
{
    self = [super init];
    if (self) {
                
        _article = art;
        _swapping = NO;
        _webBrowserIsOpen = NO;
        _briefmeViewTapped = NO;
        _webViewDidFinishNavigation = NO;
        _viewStart = [NSDate date];
        self.view.backgroundColor = [UIColor whiteColor];

        // Add either a Stream article header or a News article header
        _articleHeader = [[ArticleHeaderView alloc] initWithFrame:CGRectMake(0, 0, MAIN_WIDTH, 150*SCALEUP)];
        _articleHeader.backgroundColor = [Delegate sharedDel].articleBackgroundColor;
        [_articleHeader setStyledTitle:art.title withMinFontSize:17.0f andMaxFontSize:20.0f andFontName:@"BrandonGrotesque-Medium"];
        [_articleHeader.title setTextColor:[Delegate sharedDel].articleForegroundColor];
        UIView *subtitleContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAIN_WIDTH, 15*SCALEUP)];
        UILabel *scoreOrAge = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, MAIN_WIDTH/2, subtitleContainer.frame.size.height)];
        UILabel *source = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, MAIN_WIDTH/2, subtitleContainer.frame.size.height)];
        [subtitleContainer addSubview:scoreOrAge];
        [subtitleContainer addSubview:source];
        [_articleHeader addSubview:subtitleContainer];
        [scoreOrAge setTextColor:[Delegate sharedDel].articleAlternateColor];
        [scoreOrAge setFont:[UIFont fontWithName:@"BrandonGrotesque-Bold" size:13.0f]];
        [source setTextColor:[UIColor grayColor]];
        [source setFont:[UIFont fontWithName:@"BrandonGrotesque-Bold" size:13.0f]];
        [source setText:art.source];
        // If news article, put big rank # at the top
        if (art.section == SECTION_STREAM) {
            _articleHeader.title.center = CGPointMake(_articleHeader.frame.size.width/2, _articleHeader.frame.size.height/2);
            _articleHeader.title.frame = CGRectMake(_articleHeader.frame.size.width/2-_articleHeader.title.frame.size.width/2, 20*SCALEUP, _articleHeader.title.frame.size.width, _articleHeader.title.frame.size.height);
            [scoreOrAge setText:[NSString stringWithFormat:@"%.0f %@", [art.displayedAge floatValue], art.ageUnits]];
        } else {
            NewsArticle *nArt = (NewsArticle *)art;
            UILabel *rank = [[UILabel alloc] initWithFrame:CGRectMake(MAIN_WIDTH/2-70*SCALEUP/2, 25*SCALEUP, 70*SCALEUP, 50*SCALEUP)];
            [rank setText:[NSString stringWithFormat:@"%@", nArt.rank]];
            [rank setFont:[UIFont fontWithName:@"BrandonGrotesque-Bold" size:65.0f]];
            [rank setTextColor:[Delegate sharedDel].articleAlternateColor];
            [rank setTextAlignment:NSTextAlignmentCenter];
            [_articleHeader addSubview:rank];
            [self center:_articleHeader.title below:rank withPadding:12.0f];
            [scoreOrAge setText:[NSString stringWithFormat:@"%.1f", [nArt.score floatValue]]];
        }
        // align scoreOrAge (right align) and Source (left align) at the center of the subtitleContainer with padding
        // then center subtitleContainer within _articleHeader under the title
        float paddingBetweenSubtitles = 7.0f;
        [scoreOrAge sizeToFit];
        [source sizeToFit];
        subtitleContainer.frame = CGRectMake(0, 0, scoreOrAge.frame.size.width+source.frame.size.width+paddingBetweenSubtitles, subtitleContainer.frame.size.height);
        scoreOrAge.frame = CGRectMake(0, 0, scoreOrAge.frame.size.width, subtitleContainer.frame.size.height);
        source.frame = CGRectMake(scoreOrAge.frame.size.width+paddingBetweenSubtitles, 0, source.frame.size.width, subtitleContainer.frame.size.height);
        [self center:subtitleContainer below:_articleHeader.title withPadding:17.0f];
        // resize header height then standardize padding below subtitle
        [_articleHeader resizeHeightToFitSubviews];
        _articleHeader.frame = CGRectMake(_articleHeader.frame.origin.x, _articleHeader.frame.origin.y, _articleHeader.frame.size.width, _articleHeader.frame.size.height+10*SCALEUP);
        
        // Add navigation bar
        _bar = [[BottomNavigationBarView alloc] initWithType:BottomNavigationBarTypeControl];
        _bar.backgroundColor = [Delegate sharedDel].sectionBackgroundColor;
        [_bar.shareButton addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
        [_bar.control addTarget:self action:@selector(swapView) forControlEvents:UIControlEventValueChanged];
        
        // Add article view
        WKWebViewConfiguration *webConfig = [WebArticleView createConfigWithScriptsForUrl:[NSURL URLWithString:art.url]];
        [webConfig.userContentController addScriptMessageHandler:self name:[NSString stringWithFormat:@"%@%d", SCROLL_KEY, WEB_ARTICLE_VIEW]];
        [webConfig.userContentController addScriptMessageHandler:self name:LOADED_DOM_KEY];
         _webArticleView = [[WebArticleView alloc] initWithFrame:CGRectMake(0, STATUS_BAR_HEIGHT, MAIN_WIDTH, MAIN_HEIGHT-STATUS_BAR_HEIGHT) andWithURL:art.url andWithNavBar:(BottomNavigationBarView *)_bar andWebConfig:webConfig];
        [_webArticleView loadFromURL:[NSURL URLWithString:art.url]];
        _webArticleView.alpha = 1.0f;
        _webArticleView.navigationDelegate = self;
        _visibleView = WEB_ARTICLE_VIEW;
        
        [_webArticleView.webNavBar.forwardButton addTarget:_webArticleView action:@selector(goForward) forControlEvents:UIControlEventTouchUpInside];
        [_webArticleView.webNavBar.backButton addTarget:_webArticleView action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
        [_webArticleView.webNavBar.refreshButton addTarget:_webArticleView action:@selector(reload) forControlEvents:UIControlEventTouchUpInside];
        
        // Add zoomableImageView
        // ZoomableImageView always exists, usually hidden, even if no imgs, recycled if multiple imgs opened
        // Subview of articleviewcontroller rather than webviewwithheader to so that the delegate isn't
        //  another scrollview which causes zooming bugs
        _zoomableImageView = [[ZoomableImageView alloc] initWithFrame:self.view.frame];
        _zoomableImageView.delegate = self;
        _zoomableImageView.hidden = YES;
        _zoomableImageView.userInteractionEnabled = YES;

        // Add briefme hidden view
        WKWebViewConfiguration *webConfig2 = [BriefMeArticleView createConfigWithScriptsForUrl:[NSURL URLWithString:art.url]];
        [webConfig2.userContentController addScriptMessageHandler:self name:[NSString stringWithFormat:@"%@%d", SCROLL_KEY, BRIEFME_ARTICLE_VIEW]];
        [webConfig2.userContentController addScriptMessageHandler:self name:READ_MORE_KEY];
        [webConfig2.userContentController addScriptMessageHandler:self name:OPEN_LINK_KEY];
        [webConfig2.userContentController addScriptMessageHandler:self name:OPEN_IMAGE_KEY];
        _briefMeArticleView = [[BriefMeArticleView alloc] initWithFrame:CGRectMake(0, STATUS_BAR_HEIGHT, MAIN_WIDTH, MAIN_HEIGHT-STATUS_BAR_HEIGHT) andWithHeader:self.articleHeader andWithNavBar:_bar andWebConfig:webConfig2];
        [_briefMeArticleView setBackgroundColor:[Delegate sharedDel].articleBackgroundColor];

        // Add invisible <div> right before </body> in art.content with height equal to _briefMeArticleView's header to offset how much it moves the
        // content down. This bug occurs when injecting a uiview into the scrollview of a wkwebview. It did not occur when injecting it into a uiwebview.
        CGFloat extraPadding = _briefMeArticleView.header.frame.size.height + 15*SCALEUP - NAVIGATION_BAR_HEIGHT; // NAV_BAR_HEIGHT == _bmav's bottom edge inset
        NSString *paddingDivString = [NSString stringWithFormat:@"<div id='headeroffset' style='width:100%%; height:%fpx; display:block;'></div>", extraPadding];
        
        NSString *fullContentString = @"";
        if (art.customStyling) {
            fullContentString = [art.content stringByReplacingOccurrencesOfString:@"</body>" withString:[NSString stringWithFormat:@"%@</body>", paddingDivString]];
        } else {
            fullContentString = [NSString stringWithFormat:@"%@%@%@%@", [Delegate sharedDel].styleBefore, art.content, paddingDivString, [Delegate sharedDel].styleAfter];
        }
        [_briefMeArticleView loadFromString:fullContentString];
        
        _briefMeArticleView.navigationDelegate = self;
        _briefMeArticleView.alpha = 0.0f;
        
        // Add view swap button only if briefme view is available
        if ([art.content length] < 20) {
            _bar.noBriefMeViewLabel.hidden = NO;
            _bar.control.hidden = YES;
            _bar.controlButtonLeft.hidden = YES;
            _bar.controlButtonRight.hidden = YES;
        }
        
        // overlay the left edge with a skinny invisible column to trigger parallax on (fixes huffpo etc)
        UIView *panBackFixClearColumn = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 15*SCALEUP, MAIN_HEIGHT)];
        panBackFixClearColumn.backgroundColor = [UIColor clearColor];
    
        [self.view addSubview:_webArticleView];
        [self.view addSubview:_briefMeArticleView];
        [self.view addSubview:panBackFixClearColumn];
        [self.view addSubview:_bar];
        [self.view addSubview:_zoomableImageView];
        
        // don't automatically handle moving the view down to make room for the status bar; offset it manually instead
        [self setAutomaticallyAdjustsScrollViewInsets:NO];
    }
    return self;
}

- (void)swapView
{
    if (_swapping) {
        // must reset the selectedSegmentIndex here manually so that _visibleView and the selected segment stay synchronized
        // (if not, we don't swap but the segmented control index still changes so they are misaligned)
        if (_visibleView == BRIEFME_ARTICLE_VIEW)
            _bar.control.selectedSegmentIndex = 1;
        else
            _bar.control.selectedSegmentIndex = 0;
        return;
    }
    _swapping = YES;

    UIView *viewToHide, *viewToAppear = nil;
    if (_visibleView == WEB_ARTICLE_VIEW) {
        viewToHide = _webArticleView;
        viewToAppear = _briefMeArticleView;
        _visibleView = BRIEFME_ARTICLE_VIEW;
        
        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"tappedBriefMeView"]) {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"tappedBriefMeView"];
        }
        
        // update view for tutorial
        _briefmeViewTapped = YES;
        // update model data
        _article.briefmeViewViewed = YES;
        
        [Localytics tagEvent:@"Swap View" attributes:@{@"view": @"BriefMe Article View"}];
    } else {
        viewToHide = _briefMeArticleView;
        viewToAppear = _webArticleView;
        _visibleView = WEB_ARTICLE_VIEW;
        
        [Localytics tagEvent:@"Swap View" attributes:@{@"view": @"Web Article View"}];
    }

    [UIView animateWithDuration:0.2f animations:^{
        viewToHide.alpha = 0.0f;
    } completion:^(BOOL finished) {
        // change text while hidden, then reappear it; if change text during animation, if has to recenter and looks jumpy
        [UIView animateWithDuration:0.2f animations:^{
            viewToAppear.alpha = 1.0f;
        } completion:^(BOOL finished) {
            _swapping = NO;
        }];
    }];
}

- (void)share
{
    [[Delegate sharedDel].nc shareButtonPressedWithArticle:self.article];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    _webBrowserIsOpen = NO;
    
//     flash "briefme view" immediately if not tapped at least once during any session, and after 2 seconds if tapped during a session and view still not loaded
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"tappedBriefMeView"] && _visibleView == WEB_ARTICLE_VIEW && !_bar.control.hidden) {
        [self flashMainButton:1];
    } else {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"tappedBriefMeView"] && _visibleView == WEB_ARTICLE_VIEW && !_bar.control.hidden && !_webViewDidFinishNavigation) {
                [self flashMainButton:2];
            }
        });
    }
}

- (void)flashMainButton:(NSUInteger)flashes
{
    if (flashes == 0 || flashes > 3 || _briefmeViewTapped || _webViewDidFinishNavigation) {
        return;
    }
    
    UIView *rightControl = [[_bar.control subviews] objectAtIndex:0];
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveLinear animations:^(){
        rightControl.alpha = 0.3;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveLinear animations:^(){
            rightControl.alpha = 1;
        } completion:^(BOOL finished) {
            [self flashMainButton:flashes-1];
        }];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [_bar show];
    [_webArticleView.webNavBar moveUp];
    
    if (_visibleView == WEB_ARTICLE_VIEW && [_webArticleView.statusBarScreenshot superview]) {
        [_webArticleView.statusBarScreenshot removeFromSuperview];
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
    } else if (_visibleView == BRIEFME_ARTICLE_VIEW && [_briefMeArticleView.statusBarScreenshot superview]) {
        [_briefMeArticleView.statusBarScreenshot removeFromSuperview];
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
    } else {
        if ([Delegate sharedDel].settingsvc.control.selectedSegmentIndex == THEME_NIGHT || ([Delegate sharedDel].settingsvc.control.selectedSegmentIndex == THEME_AUTO && [Delegate autoTheme] == AUTO_THEME_NIGHT)) {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        } else {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    if (_visibleView == WEB_ARTICLE_VIEW) {
        _webArticleView.statusBarScreenshot = [[UIScreen mainScreen] snapshotViewAfterScreenUpdates:NO];
        [_webArticleView.statusBarBackground addSubview:_webArticleView.statusBarScreenshot];
    } else if (_visibleView == BRIEFME_ARTICLE_VIEW) {
        _briefMeArticleView.statusBarScreenshot = [[UIScreen mainScreen] snapshotViewAfterScreenUpdates:NO];
        [_briefMeArticleView.statusBarBackground addSubview:_briefMeArticleView.statusBarScreenshot];
    }
}

-(void)viewDidDisappear:(BOOL)animated {

    [super viewDidDisappear:animated];

    if (!_webBrowserIsOpen) {
        NSMutableDictionary *attrs = [NSMutableDictionary dictionaryWithDictionary:[self.article getAttributes]];
        [attrs addEntriesFromDictionary:@{@"timespent": [NSString stringWithFormat:@"%f", -[_viewStart timeIntervalSinceNow]]}];
        [Localytics tagEvent:@"Article Viewed" attributes:attrs];
    }
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return _zoomableImageView.imageView;
}

- (void)center:(UIView *)bottomView below:(UIView *)topView withPadding:(float)paddingPx
{
    // first center
    bottomView.center = topView.center; // CGPointMake(topView.frame.size.width/2, topView.frame.size.height/2);
    // then overwrite y coordinate using padding
    bottomView.frame = CGRectMake(bottomView.frame.origin.x, topView.frame.origin.y+topView.frame.size.height+paddingPx, bottomView.frame.size.width, bottomView.frame.size.height);
}

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message
{
    if ([message.name isEqualToString:OPEN_LINK_KEY]) {
        [self handleOpenLink:message.body];
    } else if ([message.name isEqualToString:READ_MORE_KEY]) {
        [self handleReadMore];
    } else if ([message.name isEqualToString:OPEN_IMAGE_KEY]) {
        [self handleOpenImage:message.body];
    } else if ([message.name isEqualToString:LOADED_DOM_KEY]) {
        [self handleLoadedDOM];
    } else if ([message.name isEqualToString:[NSString stringWithFormat:@"%@%d", SCROLL_KEY, BRIEFME_ARTICLE_VIEW]]) {
        [self handleScroll:message.body forArticleViewType:BRIEFME_ARTICLE_VIEW];
    } else if ([message.name isEqualToString:[NSString stringWithFormat:@"%@%d", SCROLL_KEY, WEB_ARTICLE_VIEW]]) {
        [self handleScroll:message.body forArticleViewType:WEB_ARTICLE_VIEW];
    } else {
        NSLog(@"Invalid script message with name: %@ and body: %@", message.name, message.body);
    }
}

- (void)handleLoadedDOM
{
    [_webArticleView.activityView stopAnimating];
}

- (void)handleOpenLink:(NSString *)url
{
    if ([url length] > 3) {
        _webBrowserIsOpen = YES;
        [[Delegate sharedDel].nc pushWebBrowser:url];
        
        NSMutableDictionary *attrs = [NSMutableDictionary dictionaryWithDictionary:[self.article getAttributes]];
        [attrs addEntriesFromDictionary:@{@"tappedLink": url}];
        [Localytics tagEvent:@"BriefMe View Link Tapped" attributes:attrs];
    }
}

- (void)handleReadMore
{
    [_bar show];
    [_bar.control setSelectedSegmentIndex:0];
    [self swapView];
    
    // if user navigated webArticleView before opening briefMeView and clicking Read More, return it to the original URL
    if ([_webArticleView.backForwardList.backList count] != 0 || [_webArticleView.backForwardList.forwardList count] != 0) {
        [_webArticleView loadFromURL:[NSURL URLWithString:self.article.url]];
    }
    
    [Localytics tagEvent:@"BriefMe View Read More"];
}

- (void)handleOpenImage:(NSString *)url
{
    if ([url length] > 3) {
        [_zoomableImageView loadImage:[NSURL URLWithString:url]];
        
        NSMutableDictionary *attrs = [NSMutableDictionary dictionaryWithDictionary:[self.article getAttributes]];
        [attrs addEntriesFromDictionary:@{@"imageUrl": url}];
        [Localytics tagEvent:@"Opened Zoomable Image" attributes:attrs];
    }
}

- (void)handleScroll:(NSString *)body forArticleViewType:(ArticleViewType)avt;

{
    CustomWebView *articleView = nil;
    if (avt == BRIEFME_ARTICLE_VIEW)
        articleView = _briefMeArticleView;
    else
        articleView = _webArticleView;
    
    float scrollOffset = [body floatValue];
    
    if (avt == BRIEFME_ARTICLE_VIEW) {
        scrollOffset += _briefMeArticleView.header.frame.size.height;
        // update horizontal scroll bar in briefmeview
        [_briefMeArticleView moveScrollBar];
    }
    if (scrollOffset < 0) {
        [_bar show];
        if (avt == WEB_ARTICLE_VIEW)
            [_webArticleView.webNavBar moveUp];
        return;
    }
    
    // show or hide navbar upon scrolling
    ScrollDirection scrollDirection = ScrollDirectionNone;
        if (articleView.lastContentOffset < scrollOffset)
            scrollDirection = ScrollDirectionDown;
        else if (articleView.lastContentOffset > scrollOffset)
            scrollDirection = ScrollDirectionUp;
        articleView.lastContentOffset = scrollOffset;

    if (scrollDirection == ScrollDirectionUp && [articleView draggingOrBouncing]) {
        [_bar show];
        if (avt == WEB_ARTICLE_VIEW) {
            [_webArticleView.webNavBar moveUp];
        }
    } else if (scrollDirection == ScrollDirectionDown && scrollOffset > 30 && !(avt == WEB_ARTICLE_VIEW && _webArticleView.staticNav) && [articleView draggingOrBouncing]) {
        [_bar hide];
        if (avt == WEB_ARTICLE_VIEW) {
            [_webArticleView.webNavBar moveDown];
        }
    }
    // no need to manually call [_bar show] when you reach the end of an article because bounces=YES will trigger it
}

- (void)webView:(nonnull WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
    if ([webView isKindOfClass:[WebArticleView class]]) {
        [_webArticleView.activityView startAnimating];
    }
}

- (void)webView:(nonnull WKWebView *)webView didFailLoadWithError:(NSError *)error
{
    if ([webView isKindOfClass:[WebArticleView class]]) {
        [_webArticleView.activityView stopAnimating];
    }
}

- (void)webView:(nonnull WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    if ([webView isKindOfClass:[WebArticleView class]]) {
        [_webArticleView.activityView stopAnimating];
    }
}

- (void)webView:(nonnull WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    if ([webView isKindOfClass:[WebArticleView class]]) {
        [_webArticleView.activityView stopAnimating];
    }
}

- (void)webView:(nonnull WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation
{
    CustomWebView *cwv = (CustomWebView *)webView;
    [cwv finishedNavigation];
    
    if ([webView isKindOfClass:[WebArticleView class]]) {
        _webViewDidFinishNavigation = YES;
        
        if ([webView canGoBack]) {
            // set to appear first time you can go back
            if (!_webArticleView.webNavBar.visible) {
                [_webArticleView.webNavBar animateAppear];
            }
            
            _webArticleView.webNavBar.backTriangleImageView.alpha = 1.0f;
        } else {
            _webArticleView.webNavBar.backTriangleImageView.alpha = DISABLED_BUTTON_ALPHA;
        }
        
        if ([webView canGoForward]) {
            _webArticleView.webNavBar.forwardTriangleImageView.alpha = 1.0f;
        } else {
            _webArticleView.webNavBar.forwardTriangleImageView.alpha = DISABLED_BUTTON_ALPHA;
        }
    }
}

@end
