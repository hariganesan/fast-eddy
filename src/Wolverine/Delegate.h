//
//  Delegate.h
//  MOST
//
//  Created by Hari Ganesan on 12/17/13.
//  Copyright (c) 2013 Campion Designs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeprecationViewController.h"

@class NavController, BriefingListViewController, SettingsViewController, SectionViewController, TBKTabBarController, Parser, LoadingView, OnboardingView, WinningTableViewController, StreamListViewController;

/** @discussion  All app level configuration resources.
    DO NOT THROW RANDOM STUFF IN HERE!
    Reserved for any properties and methods that require multiple points of access. */
@interface Delegate : UIResponder <UIApplicationDelegate, Deprecation>

// helpful views and controllers
@property (strong, nonatomic) UIApplication *app;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NavController *nc;

@property (strong, nonatomic) UIViewController *vc;
@property (strong, nonatomic) BriefingListViewController *briefingvc;
@property (strong, nonatomic) StreamListViewController *streamvc;
@property (strong, nonatomic) SettingsViewController *settingsvc;
@property (strong, nonatomic) SectionViewController *sectvc;
@property (strong, nonatomic) TBKTabBarController *tabsectvc;
@property (nonatomic, strong) LoadingView *loadingView;
@property (nonatomic, strong) OnboardingView *onboardingView;
@property (nonatomic, strong) WinningTableViewController *wc;

@property (nonatomic, strong) UIView *shutDownView;

// helpful properties
@property (nonatomic) BOOL firstLaunch;
@property (nonatomic) int tableViewFrameOffset;
@property (nonatomic) BOOL autoWinners;

@property (strong, nonatomic) NSMutableDictionary *parsedNews;
@property (strong, nonatomic) Parser *parser;

// config
/** @discussion Most recent webblocks. Also stored in nsuserdefaults as @"webBlocks"].
    Stored in a property rather than just grabbing from nsuserdefaults to increase 
    speed. Has format: {"updated": <datestring>, "blocks": {"host": <webblock>}} */
@property (nonatomic, strong) NSDictionary *webBlocks;
@property (nonatomic, strong) NSArray *faq;
/** @brief String containing styling for briefmeview from <html> through <body> */
@property (nonatomic, strong) NSString *styleBefore;
/** @brief String containing styling for briefmeview from <\body> through </html> */
@property (nonatomic, strong) NSString *styleAfter;
/** @brief Dict with keys of hostnames where bottom nav shouldn't hide in webview */
@property (nonatomic, strong) NSDictionary *staticNavSources;
/** @brief Min number of app launches before feedbackcell is inserted */
@property (nonatomic) NSInteger launchesUntilFeedback;
/** @brief Cell number that the feedback cell will be inserted as if it is inserted */
@property (nonatomic) NSInteger feedbackCellNumber;
/** @discussion Contains (article url, article content) pairs for briefing articles;
    Set by parser and used by navcontroller during Watch handoff. Could not handoff
    full article with content because object was too large. Lookup will fail if 
    either the phone or Watch have stale data and they mismatch. */
@property (nonatomic, strong) NSMutableDictionary *briefingContentsDict;

// Colors for ALVC
@property (nonatomic, strong) UIColor *cellTitleColor;
@property (nonatomic, strong) UIColor *cellHighlightColor;
@property (nonatomic, strong) UIColor *cellBackgroundColor;
@property (nonatomic, strong) UIColor *cellBackgroundHighlightColor;
@property (nonatomic, strong) UIColor *cellBackgroundVisitedColor;
@property (nonatomic, strong) UIColor *cellNumberColor;

// light/dark theme
@property (nonatomic, strong) UIColor *sectionBackgroundColor;
@property (nonatomic, strong) UIColor *cellAlternateColor;
@property (nonatomic, strong) UIColor *cellAlternateTransparentColor;
@property (nonatomic, strong) UIColor *articleBackgroundColor;
@property (nonatomic, strong) UIColor *articleForegroundColor;
@property (nonatomic, strong) UIColor *articleAlternateColor;
@property (nonatomic, strong) UIColor *articleScrollBarColor;
@property (nonatomic, strong) UIColor *articleScrollBarFinishColor;
@property (nonatomic, strong) UIColor *navigationButtonForegroundColor;
@property (nonatomic, strong) UIColor *navigationButtonBackgroundColor;

typedef enum {
    TIME_OF_DAY_MORNING,
    TIME_OF_DAY_AFTERNOON,
    TIME_OF_DAY_EVENING,
    TIME_OF_DAY_NIGHT
} TimeOfDayType;

typedef enum {
    AUTO_THEME_DAY,
    AUTO_THEME_NIGHT
} AutoThemeType;

typedef enum {
    SECTION_BRIEFING,
    SECTION_STREAM,
    SECTION_POLITICS,
    SECTION_WORLD,
    SECTION_BUSINESS,
    SECTION_SPORTS,
    SECTION_TECHNOLOGY,
    SECTION_ENTERTAINMENT,
    SECTION_WINNERS
} SectionType;

typedef enum ScrollDirection {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
} ScrollDirection;

typedef enum {
    BRIEFME_ARTICLE_VIEW,
    WEB_ARTICLE_VIEW
} ArticleViewType;

- (void)initApp;
- (void)appLoaded;
+ (Delegate *)sharedDel;
+ (NSInteger)timeOfDay;
+ (NSInteger)timeZoneHour;
+ (NSInteger)autoTheme;
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)size;
+ (void)registerNotifications;

+ (void)updateParseWithToggle:(BOOL)toggle;
+ (CGFloat) AACStatusBarHeight;
+ (CGFloat) AACStatusBarWidth;

@end
