//
//  BriefMeArticleView.h
//  BriefMe
//
//  Created by Charlie Vrettos on 6/23/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import "CustomWebView.h"

@class ArticleInnerCellView, BottomNavigationBarView;

@interface BriefMeArticleView : CustomWebView
/** @brief header taken from old `WebViewWithHeader` class */
@property (nonatomic, strong) UIView *header;
/** @discussion A horizontal scroll bar behind the status bar
 that shows how far a user has scrolled/read */
@property (nonatomic, strong) UIView *horizontalScrollBar;

- (id)initWithFrame:(CGRect)frame andWithHeader:(ArticleInnerCellView *)header andWithNavBar:(BottomNavigationBarView *)navBar andWebConfig:(WKWebViewConfiguration *)webConfig;
/** @brief Animates horizontal scroll bar based on scroll distance */
- (void)moveScrollBar;
/** @brief Increases font size to 'zoom in'. */
- (void)increaseFontSize;
/** @brief Decreases font size to 'zoom out'. */
- (void)decreaseFontSize;
/** @description Builds a WKWebViewConfiguration with the appropriate
    javascript userscripts for briefmeview added, including scroll
    handling code, link opening code, image opening code,  and night
    mode handling code. */
+ (WKWebViewConfiguration *)createConfigWithScriptsForUrl:(NSURL *)url;

@end
