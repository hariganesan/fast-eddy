//
//  Parser.m
//  MOST
//
//  Created by Todd Lubin on 6/28/12.
//  Copyright (c) 2012 Campion Designs, LLC. All rights reserved.
//

#import "Parser.h"
#import "Delegate.h"
#import "AFHTTPRequestOperationManager.h"
#import "NewsArticle.h"
#import "Localytics.h"
#import "WinningTableViewController.h"
#import "StreamListViewController.h"
#import "BriefingListViewController.h"
#import "NavController.h"
#import "DeprecationViewController.h"

@implementation Parser

- (id)init
{
    self = [super init];
    if (self) {
        _networkErrorInView = NO;
        _deprecationErrorInView = NO;
    }
    
    return self;
}

- (void)parseObjectsWithSection:(NSInteger)section
{
    // set initial base time for parse calculation
    _parseStart = [NSDate date];
    NSString *sectionName = BM_ROUTES[section];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@%@", NEWS_URL, sectionName] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        if ([JSON objectForKey:@"deprecated"] && [(NSNumber *)[JSON objectForKey:@"deprecated"] isEqualToNumber:[NSNumber numberWithBool:YES]]) {
            [self presentDeprecation];
            return;
        }
        
        // load config from briefing section
        if (section == SECTION_BRIEFING && [JSON objectForKey:@"config"]) {
            NSDictionary *config = [[NSDictionary alloc] initWithDictionary:[JSON objectForKey:@"config"]];
            // load faq, briefmeview styling
            [Delegate sharedDel].faq = [config objectForKey:@"faq"];
            [Delegate sharedDel].styleBefore = [self injectBase64Fonts:[config objectForKey:@"style_before"]];
            [Delegate sharedDel].styleAfter = [config objectForKey:@"style_after"];
            // static navigation bar for sites
            NSArray *arr = [config objectForKey:@"static_navs"];
            // turn array of hostnames into dictionary with same values as keys
            [Delegate sharedDel].staticNavSources = [NSDictionary dictionaryWithObjects:arr forKeys:arr];
            // load backend configs for feedback cells
            if ([config objectForKey:@"app_inits_until_feedback"]) {
                [Delegate sharedDel].launchesUntilFeedback = [[config valueForKey:@"app_inits_until_feedback"] integerValue];
            }
            if ([config objectForKey:@"feedback_cell_number"]) {
                [Delegate sharedDel].feedbackCellNumber = [[config valueForKey:@"feedback_cell_number"] integerValue];
            }
            
            // if stream section already loaded, insert feedback cell (if the conditions for it to appear apply) now that we have
            //  feedbackCellNumber and launchesUntilFeedback
            if ([Delegate sharedDel].streamvc.articlesLoaded) {
                [[Delegate sharedDel].streamvc maybeInsertFeedbackCell];
            }
            // if not, we just set feedbackCellNumber and launchesUntilFeedback so StreamListViewController can inject it once articles load in replaceData
            
            // if local web blocks are outdated (or there are no local webblocks, which should never happen), get new webblocks from /webblocks
            if (![Delegate sharedDel].webBlocks || ![[Delegate sharedDel].webBlocks objectForKey:@"updated"] || ([config objectForKey:@"web_blocks_updated"] && ![[config objectForKey:@"web_blocks_updated"] isEqualToString:[[Delegate sharedDel].webBlocks objectForKey:@"updated"]])) {
                [self downloadNewWebBlocks];
            }
        }
        
        [self parseNewsWithJSON:JSON andSection:section];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"News Request Failed with Error: %@ for %@%@", error, NEWS_URL, sectionName);
        [self presentFailure];
    }];
}

- (void)parseNewsWithJSON:(id)JSON andSection:(NSInteger)section
{
    NSMutableDictionary *routeData = [[NSMutableDictionary alloc] init];
    [routeData setObject:[JSON objectForKey:@"imageNumber"] forKey:@"imageNumber"];
    
    NSMutableArray* parsedArticles = [[NSMutableArray alloc]init];
    NSString *sectionName = BM_ROUTES[section];
    
    //For each entry, get title, source, link, pubDate
    
    NSArray *articleList = [JSON objectForKey:@"articles"];

    int index = 0;
    for (NSDictionary *tempDict in articleList) {
        
         //add the routeUpdated date the json was updated to the dict for age+updated calculations
        NSMutableDictionary *dict = [tempDict mutableCopy];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        [dateFormatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss z"];
        NSDate *updated = [dateFormatter dateFromString:[JSON objectForKey:@"date"]];
        [dict setValue:updated forKey:@"routeUpdated"];
        
        Article *article;
        if (section == SECTION_STREAM) {
            article = [[Article alloc] initWithDictionary:dict];
            article.section = SECTION_STREAM;
        // 10 articles in each section
        } else if (index >= 10) {
            break;
        // check if manually switched off
        } else if ([(NSNumber *)[dict objectForKey:@"display"] isEqualToNumber:[NSNumber numberWithBool:NO]]) {
            continue;
        } else {
            article = [[NewsArticle alloc] initWithDictionary:dict];
            ((NewsArticle *)article).rank = [NSNumber numberWithInt:index+1];
            article.section = (int) section;
        }
        
        [self updateDisplayedAge:article];
        [parsedArticles addObject:article];
        index++;
    }
    
    // For briefing section, store the 10 art contents in briefingContentsDict in case article handed-off
    // from Apple Watch so that we can grab 'content' from it and display briefmeview. Cannot simply pass
    // article content during handoff because it makes the object too long, so this is a fix. It will not
    // fix the case where the watch and phone have different article sets open (either watch or phone have
    // stale data due to being open for too long); in that case, there will be no briefmeview via handoff.
    if (section == SECTION_BRIEFING) {
        for (NewsArticle *nArt in parsedArticles) {
            [[Delegate sharedDel].briefingContentsDict setObject:nArt.content forKey:nArt.url];
        }
    }
    
    [routeData setValue:parsedArticles forKey:@"articles"];
    
    [[Delegate sharedDel].parsedNews setValue:routeData forKey:sectionName];
    NSString *notificationName = [NSString stringWithFormat:@"finishedParsingNews %@", sectionName];
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil];
}

- (void)updateDisplayedAge:(Article *)art
{
    // in mins
    double age = [[NSDate date] timeIntervalSinceDate:art.published]/60;
    // is art.age ever used anymore
    art.age = [NSNumber numberWithDouble:age];
    
    // should be just 0; negatives are errors
    if ((int)age < 1) {
        art.displayedAge = [NSNumber numberWithInt:0];
        art.ageUnits = @"mins ago";
    } else if ((int)age == 1) {
        art.displayedAge = [NSNumber numberWithInt:1];
        art.ageUnits = @"min ago";
    } else if ((int)age > 1 && (int)age < 60) {
        art.displayedAge = [NSNumber numberWithInt:age];
        art.ageUnits = @"mins ago";
    } else if ((int)age >= 60 && (int)age < 120) {
        art.displayedAge = [NSNumber numberWithInt:1];
        art.ageUnits = @"hour ago";
    } else if ((int)age >= 120) {
        art.displayedAge = [NSNumber numberWithInt:age/60];
        art.ageUnits = @"hours ago";
    }
}

- (void)parseWinners
{
    // parse /winners objects and init winningView
    if ([Delegate timeOfDay] == TIME_OF_DAY_EVENING) {
        NSCalendar *g = [NSCalendar currentCalendar];
        [g setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setTimeZone:[NSTimeZone defaultTimeZone]];
        [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSInteger gmtHour = [[g components:NSCalendarUnitHour fromDate:[df dateFromString:@"1970-01-01 18:00:00"]] hour];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager GET:[NSString stringWithFormat:@"%@winners?hour=%ld", NEWS_URL, (long)gmtHour] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
            
            if ([JSON objectForKey:@"deprecated"] && [(NSNumber *)[JSON objectForKey:@"deprecated"] isEqualToNumber:[NSNumber numberWithBool:YES]]) {
                [self presentDeprecation];
                return;
            }
            
            if (![JSON objectForKey:@"articles"] || [[JSON objectForKey:@"articles"] count] < 3) {
                NSLog(@"Invalid winners data from hour %ld", (long)gmtHour);
                return;
            }

            [Delegate sharedDel].wc = [[WinningTableViewController alloc] initWithJson:JSON];
            [[Delegate sharedDel].briefingvc updateBanner];
            
            if ([Delegate sharedDel].autoWinners) {
                [Localytics tagEvent:@"Daily 3 Notification Swiped" attributes:@{@"time": [NSString stringWithFormat:@"%ld", (long)[Delegate timeOfDay]]}];
                [[Delegate sharedDel].briefingvc displayWinningView];
                [Delegate sharedDel].autoWinners = NO;
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"/winners Request Failed");
            [Delegate sharedDel].briefingvc.banner.hidden = YES;
        }];
        
    } else if ([Delegate sharedDel].wc) {
        // winning init'd on previous init; hide banner!
        [Delegate sharedDel].briefingvc.banner.hidden = YES;
    }
}

- (void)presentDeprecation
{
    if (!_deprecationErrorInView) {
        _deprecationErrorInView = YES;
        DeprecationViewController *dvc = [[DeprecationViewController alloc] init];
        [[Delegate sharedDel].nc presentViewController:dvc animated:YES completion:nil];
    }
}

- (void)presentFailure
{
    if (!_networkErrorInView) {
        _networkErrorInView = YES;
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error connecting to BriefMe" message:@"Check your network connection and try again later." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            _networkErrorInView = NO;
            [[Delegate sharedDel] initApp];
        }];
        [alert addAction:defaultAction];
        [[Delegate sharedDel].nc presentViewController:alert animated:YES completion:nil];
        
        [Localytics tagEvent:@"Network Failure"];
    }
}

- (NSString *)injectBase64Fonts:(NSString *)styleBefore
{
    styleBefore = [styleBefore stringByReplacingOccurrencesOfString: @"{{{REPLACE_Georgia-light_HERE}}}" withString:[self readFontString:@"Georgia_light" oftype:@"txt"]];
    styleBefore = [styleBefore stringByReplacingOccurrencesOfString: @"{{{REPLACE_BrandonGrotesque-Medium_HERE}}}" withString:[self readFontString:@"Brandon_med" oftype:@"txt"]];
    return styleBefore;
}

- (NSString *)readFontString:(NSString *)fileName oftype:(NSString *)type
{
    NSError *error = nil;
    NSString *fullPath = [[NSBundle mainBundle] pathForResource:fileName ofType:type];
    NSString *text = [NSString stringWithContentsOfFile:fullPath encoding:NSUTF8StringEncoding error:&error];
    if (error)
        return @"";
    return text;
}

/** @brief updates local webblocks with ones from /webblocks */
- (void)downloadNewWebBlocks
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@webblocks", NEWS_URL] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        if ([JSON objectForKey:@"blocks"] && [JSON objectForKey:@"updated"]) {
            NSLog(@"successfully found new web blocks");
            [[NSUserDefaults standardUserDefaults] setObject:JSON forKey:@"webBlocks"];
            [Delegate sharedDel].webBlocks = JSON;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"/webblocks Request Failed");
    }];

}

@end
