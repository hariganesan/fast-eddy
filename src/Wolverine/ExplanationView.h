//
//  ExplanationView.h
//  MOST
//
//  Created by Charlie Vrettos on 11/13/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScoreBannerView.h"

/** @discussion  View displayed to explain the BriefMe score.
    Shows a text explanation and a scorebannerview. */
@interface ExplanationView : UIView

@property (nonatomic, strong) UIButton *hideButton;

// TODO(hari): remove model data from view
@property (nonatomic, strong) NSMutableArray *arts;

- (void)hideExplanation;
- (void)displayExplanation:(id)sender;

@end
