//
//  StreamArticleInnerCellView.m
//  BriefMe
//
//  Created by Charlie Vrettos on 6/22/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import "StreamArticleInnerCellView.h"

@implementation StreamArticleInnerCellView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // title height must also be changed in constrain function below!
        self.title = [[UILabel alloc]initWithFrame:CGRectMake(35*SCALEUP, 20*SCALEUP, 250*SCALEUP, 85*SCALEUP)];
        [self.title setFont: [UIFont fontWithName:@"BrandonGrotesque-Regular" size:16]];
        [self.title setBackgroundColor:[UIColor clearColor]];
        self.title.lineBreakMode = NSLineBreakByClipping;
        [self addSubview:self.title];

        // non flexible widths to compensate for font size and relative origin.x
        if (MAIN_WIDTH == IPHONE_6PLUS_WIDTH) {
            _age = [[UILabel alloc] initWithFrame:CGRectMake(35*SCALEUP, 101*SCALEUP, 20, 22*SCALEUP)];
            _timeAgo = [[UILabel alloc] initWithFrame:CGRectMake(60*SCALEUP, 101*SCALEUP, 60, 22*SCALEUP)];
        } else {
            _age = [[UILabel alloc] initWithFrame:CGRectMake(35*SCALEUP, 100*SCALEUP, 20, 22*SCALEUP)];
            _timeAgo = [[UILabel alloc] initWithFrame:CGRectMake(60*SCALEUP, 100*SCALEUP, 60, 22*SCALEUP)];
        }
        
        _age.font = [UIFont fontWithName:@"BrandonGrotesque-Black" size:16];
        [_age setNumberOfLines:1];
        _age.textColor = [UIColor grayColor];
        _age.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_age];
        
        _timeAgo.font = [UIFont fontWithName:@"BrandonGrotesque-Black" size:16];
        _timeAgo.textColor = [UIColor grayColor];
        _timeAgo.textAlignment = NSTextAlignmentCenter;
        [_timeAgo setNumberOfLines:1];
        [self addSubview:_timeAgo];
        
        self.source = [[UILabel alloc] initWithFrame:CGRectMake(120*SCALEUP, 103*SCALEUP, 100, 20*SCALEUP)];
        [self.source setFont: [UIFont fontWithName:@"BrandonGrotesque-Bold" size:10]];
        self.source.textColor = [UIColor grayColor];
        [self.source setBackgroundColor:[UIColor clearColor]];
        [self addSubview:self.source];
        
        [self setBackgroundColor:[UIColor clearColor]];

    }
    return self;
}

@end
