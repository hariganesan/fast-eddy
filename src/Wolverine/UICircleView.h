//
//  UICircleView.h
//  MOST
//
//  Created by Hari Ganesan on 2/20/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICountingLabel.h"

/** @brief  Circles that are used to display numbers out of 100 */
@interface UICircleView : UIView

@property (nonatomic, strong) UIColor *color;
@property (nonatomic, strong) CAShapeLayer *circle;
@property (nonatomic, strong) CAShapeLayer *glowCircle;
@property (nonatomic, strong) UICountingLabel* attributedLabel;

- (id)initWithFrame:(CGRect)frame andColor:(UIColor *)color;
- (void)animateCircleTo:(NSNumber *)score withDuration:(NSNumber *)duration;

@end
