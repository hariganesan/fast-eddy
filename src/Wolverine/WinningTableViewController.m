//
//  WinningTableViewController.m
//  MOST
//
//  Created by Charlie Vrettos on 10/31/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import "WinningTableViewController.h"
#import "NewsArticle.h"
#import "NewsArticleCell.h"
#import "Delegate.h"
#import "Parser.h"
#import "NavController.h"
#import "UITabBarController+hidable.h"
#import "UICircleView.h"
#import "Localytics.h"
#import "BottomNavigationBarView.h"
#import "NewsArticleInnerCellView.h"
#import "ArticleViewController.h"

@implementation WinningTableViewController

- (id)initWithJson:(id)JSON
{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        self.tableViewV2 = self.tableView;
        // Same junk that's done in ArticleListViewController so we can have a fixed navbar
        UIView *replacementView = [[UIView alloc] initWithFrame:self.tableView.frame];
        self.view = replacementView;

        [self.view addSubview:self.tableViewV2];
        self.tableViewV2.scrollEnabled = YES;
        
        // if we want bounces=YES, need to fix bug where scroll down too far and cell leaves visiblecells then willDisplayCell is called animating it further up
        self.tableViewV2.bounces = NO;
        
        if (MAIN_HEIGHT == IPHONE_4_HEIGHT) {
            self.tableViewV2.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAIN_WIDTH, 100)];
            self.tableViewV2.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAIN_WIDTH, 50)];
        } else {
            self.tableViewV2.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAIN_WIDTH, MAIN_HEIGHT-NAVIGATION_BAR_HEIGHT-3*140*SCALEUP)];
        }
        
        self.tableViewV2.backgroundColor = [Delegate sharedDel].sectionBackgroundColor;
        self.tableViewV2.separatorStyle = UITableViewCellSeparatorStyleNone;
        UILabel *todays3Winners = [[UILabel alloc] initWithFrame:CGRectMake(0, 18*SCALEUP, MAIN_WIDTH, 25*SCALEUP)];
        NSAttributedString *attributedString =
        [[NSAttributedString alloc]
         initWithString: @"THE DAILY THREE"
         attributes:
         @{
           NSFontAttributeName : [UIFont fontWithName:@"BrandonGrotesque-Bold" size:20],
           NSKernAttributeName : @(1.8f)
           }];
        [todays3Winners setAttributedText:attributedString];
        todays3Winners.textColor = [UIColor whiteColor];
        [todays3Winners setTextAlignment:NSTextAlignmentCenter];
        [self.tableViewV2.tableHeaderView addSubview:todays3Winners];

        UILabel *winnersDesc = [[UILabel alloc] initWithFrame:CGRectMake(MAIN_WIDTH/2 - 220/2, todays3Winners.frame.origin.y+todays3Winners.frame.size.height, 220, 60)];
        NSAttributedString *attributedString2 =
        [[NSAttributedString alloc]
         initWithString: @"The three highest BriefMe Scores of the day"
         attributes:
         @{
           NSFontAttributeName : [UIFont fontWithName:@"BrandonGrotesque-Bold" size:12],
           NSKernAttributeName : @(1.8f)
           }];
        [winnersDesc setAttributedText:attributedString2];
        winnersDesc.textColor = [UIColor whiteColor];
        winnersDesc.numberOfLines = 2;
        [winnersDesc setTextAlignment:NSTextAlignmentCenter];
        [self.tableViewV2.tableHeaderView addSubview:winnersDesc];
        
        _articles = [[NSMutableArray alloc] init];
        
        int index = 1;
        for (NSDictionary *d in [JSON objectForKey:@"articles"]) {
            NewsArticle *art = [[NewsArticle alloc] initWithDictionary:d];
            art.section = SECTION_WINNERS;
            art.rank = [NSNumber numberWithInt:index];
            [_articles addObject:art];
            // calculated 'hours ago' vs 'mins ago' etc using art.published
            [[Delegate sharedDel].parser updateDisplayedAge:art];
            
            index++;
        }

        if ([_articles count] < 3)
            NSLog(@"ERROR: didn't get 3 articles from /winners... %@", _articles);
        
        // EXPLANATION VIEW
        //child so that it's not confused w/ super's explanationview
        _explanationView = [[ExplanationView alloc] initWithFrame:[Delegate sharedDel].window.frame];
        _explanationView.arts = _articles;
        [self.view addSubview:_explanationView];
        
        _bar = [[BottomNavigationBarView alloc] initWithType:BottomNavigationBarTypeLogo];
        _bar.backgroundColor = [Delegate sharedDel].sectionBackgroundColor;
        _bar.shareButtonImageView.hidden = YES;
        [self.view addSubview:_bar];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetCells) name:@"resetCells" object:nil];
    }
    
    return self;
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 140*SCALEUP;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NewsArticleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WCell"];
    NewsArticle *art = _articles[indexPath.row];
    
//     Configure the cell...
    if (cell == nil) {
        cell = [[NewsArticleCell alloc] initWithReuseIdentifier:@"WCell"];
        [cell.explanationViewOpenButton addTarget:self action:@selector(displayExplanation:) forControlEvents:UIControlEventTouchUpInside];
    }

    [cell.newsArticleInnerCellView.rank setText:[NSString stringWithFormat:@"%d", (int)(indexPath.row+1)]];
    cell.row = indexPath.row;
    
    [cell.newsArticleInnerCellView.vcircle animateCircleTo:((NewsArticle *)art).score withDuration:0];
    [cell.newsArticleInnerCellView.score setText:[NSString stringWithFormat:@"%.1f", [((NewsArticle *)art).score floatValue]]];
    
    [cell.newsArticleInnerCellView.title setNumberOfLines:3];
    [cell.newsArticleInnerCellView.title setText: art.title];
    CGRect oldFrame = cell.newsArticleInnerCellView.title.frame;
    [cell.newsArticleInnerCellView constrainTitleForText:art.title withMinFontSize:14.0 andMaxFontSize:17.0 andFontName:@"BrandonGrotesque-Regular"];
    [cell.newsArticleInnerCellView.title sizeToFit];
    cell.newsArticleInnerCellView.title.frame = CGRectMake(cell.newsArticleInnerCellView.title.frame.origin.x, 20*SCALEUP, oldFrame.size.width, cell.newsArticleInnerCellView.title.frame.size.height);
    [cell.newsArticleInnerCellView.source setText:art.source];
    
    cell.explanationViewOpenButton.tag = [art.rank intValue]-1;
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"articleIDs"] objectForKey:art.title]) {
        [cell setVisited:YES animated:NO];
    } else {
        [cell setVisited:NO animated:NO];
    }
    
    return cell;
}

- (void)displayExplanation:(id)sender
{
    [_explanationView displayExplanation:sender];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    Article *art = [_articles objectAtIndex:indexPath.row];
    
    // store visited info in nsuserdefaults data
    NewsArticleCell *cell = (NewsArticleCell *) [tableView cellForRowAtIndexPath:indexPath];
    
    if (!cell.visited) {
        NSMutableDictionary *articleIDs = [[[NSUserDefaults standardUserDefaults] objectForKey:@"articleIDs"] mutableCopy];
        [articleIDs setObject:[NSDate date] forKey:cell.newsArticleInnerCellView.title.text];
        [[NSUserDefaults standardUserDefaults] setObject:articleIDs forKey:@"articleIDs"];
    }
    
    UIViewController *vc = [[ArticleViewController alloc] initWithArticle: art];
    [Delegate sharedDel].nc.currentViewController = vc;
    [[Delegate sharedDel].nc pushViewController:vc animated:YES];

    // reload visited cell so it displays when you go back
    [tableView beginUpdates];
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [tableView endUpdates];
}

- (void)resetCells
{
    for (UITableViewCell *curCell in self.tableViewV2.visibleCells) {
        [curCell setHighlighted:curCell.highlighted];
    }
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


@end
