//
//  StreamListViewController.m
//  MOST
//
//  Created by Hari Ganesan on 9/16/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import "StreamListViewController.h"
#import "Delegate.h"
#import "TutorialView.h"
#import "StreamArticleCell.h"
#import "Article.h"
#import "FeedbackCell.h"
#import "ArticleListViewController.h"
#import "Parser.h"
#import "StreamArticleInnerCellView.h"
#import "BriefingListViewController.h"

@implementation StreamListViewController

- (id)initWithSection:(SectionType)section andTitle:(NSString *)title{
    self = [super initWithSection:section andTitle:title];
    if (self) {
        // _animating used so that the scrollcircle fade-out animation is only called once
        _animating = NO;
        _feedbackIndexPathRow = -1;
        
        // need a container for the circle so the button can be a larger rectangle
        _scrollCircle = [[UIView alloc] initWithFrame:CGRectMake(MAIN_WIDTH - 58, MAIN_HEIGHT - 96, 58, 50)];
        _scrollCircle.backgroundColor = [UIColor clearColor];
        
        UIView *scrollCircleShape = [[UIView alloc] initWithFrame:CGRectMake(5, 5, 35, 35)];
        scrollCircleShape.layer.cornerRadius = 35/2;
        scrollCircleShape.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.25];
        [_scrollCircle addSubview:scrollCircleShape];
        
        UIImageView *scrollCircleArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"back.png"]];
        scrollCircleArrow.frame = CGRectMake(0, 0, 35, 35);
        scrollCircleArrow.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
        [scrollCircleShape addSubview:scrollCircleArrow];
        
        UIButton *scrollButton = [[UIButton alloc] initWithFrame:CGRectMake(0,0,58,50)];
        //scrollButton.layer.cornerRadius = 35/2;
        scrollButton.backgroundColor = [UIColor clearColor];
        [scrollButton addTarget:self action:@selector(scrollToTop) forControlEvents:UIControlEventTouchUpInside];
        [_scrollCircle addSubview:scrollButton];
        [self.view addSubview:_scrollCircle];
        
        _scrollCircleUp = YES;
        _scrollCircle.hidden = YES;
    }
    
    return self;
}

- (void)adjustArticlesWithFeedback:(BOOL)feedback
{
    if (_feedbackIndexPathRow >= 0 && feedback) {
        [self.objects insertObject:[[Article alloc] init] atIndex:_feedbackIndexPathRow];
    } else if (_feedbackIndexPathRow >= 0) {
        // TODO(hari): cannot currently remove -- results in unexpected cell expansion behavior
//        [self.objects removeObjectAtIndex:_feedbackIndexPathRow];
    }
}

- (void)maybeInsertFeedbackCell
{
    if (_feedbackIndexPathRow > 0) {
        return;
    }
    
    // check to see if previously tapped and if launch count is past given number
    if ((![[NSUserDefaults standardUserDefaults] objectForKey:@"feedbackTapped"] || ![[NSUserDefaults standardUserDefaults] boolForKey:@"feedbackTapped"]) && [[NSUserDefaults standardUserDefaults] objectForKey:@"launchCount"] && [[NSUserDefaults standardUserDefaults] integerForKey:@"launchCount"] > [Delegate sharedDel].launchesUntilFeedback) {
        _feedbackIndexPathRow = [Delegate sharedDel].feedbackCellNumber; // change to -1 when not shown
        [self adjustArticlesWithFeedback:YES];
    }
}

- (void)hideFeedbackRow
{
    if (_feedbackIndexPathRow < 0) {
        return;
    }
    
    [self adjustArticlesWithFeedback:NO];
    _feedbackIndexPathRow = -1;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"feedbackTapped"];
    [self.tableViewV2 reloadData];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // handle feedback row
    if (indexPath.row == _feedbackIndexPathRow) {
        FeedbackCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FCell"];
        
        if (cell ==  nil) {
            cell = [[FeedbackCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FCell"];
        }
        
        return cell;
    }
    
    StreamArticleCell *scell = (StreamArticleCell *)[super tableView:tableView cellForRowAtIndexPath:indexPath];
    Article *art = (Article *)[self.objects objectAtIndex:indexPath.row];
    
    [scell.streamArticleInnerCellView setStyledTitle:art.title withMinFontSize:14.0f andMaxFontSize:17.0f andFontName:@"BrandonGrotesque-Regular"];
    
    // save the frame height because the text is vertically aligned
    scell.streamArticleInnerCellView.age.text = [NSString stringWithFormat:@"%.0f", [art.displayedAge floatValue]];
    CGFloat origH = scell.streamArticleInnerCellView.age.frame.size.height;
    [scell.streamArticleInnerCellView.age sizeToFit];
    scell.streamArticleInnerCellView.age.frame = CGRectMake(scell.streamArticleInnerCellView.age.frame.origin.x, scell.streamArticleInnerCellView.age.frame.origin.y, scell.streamArticleInnerCellView.age.frame.size.width, origH);

    // fixed width between age and timeago
    scell.streamArticleInnerCellView.timeAgo.text = art.ageUnits;
    scell.streamArticleInnerCellView.timeAgo.frame = CGRectMake(scell.streamArticleInnerCellView.age.frame.origin.x+scell.streamArticleInnerCellView.age.frame.size.width+1, scell.streamArticleInnerCellView.timeAgo.frame.origin.y, scell.streamArticleInnerCellView.timeAgo.frame.size.width, scell.streamArticleInnerCellView.timeAgo.frame.size.height);
    
    // flexible width between age/timeago and source
    [scell.streamArticleInnerCellView.source setText:art.source];
    scell.streamArticleInnerCellView.source.frame = CGRectMake(scell.streamArticleInnerCellView.timeAgo.frame.origin.x+scell.streamArticleInnerCellView.timeAgo.frame.size.width+5*SCALEUP, scell.streamArticleInnerCellView.source.frame.origin.y, scell.streamArticleInnerCellView.source.frame.size.width, scell.streamArticleInnerCellView.source.frame.size.height);
    
    return scell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == _feedbackIndexPathRow) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
        return;
    }
    
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
}

- (void)scrollToTop
{
    [self.tableViewV2 scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [super scrollViewDidScroll:scrollView];
    
    // display/hide stream arrow button
    if (_scrollCircle && _scrollCircle.hidden && self.tableViewV2.contentOffset.y > MAIN_HEIGHT-200) {
        _scrollCircle.alpha = 0.0;
        _scrollCircle.hidden = NO;
        [UIView animateWithDuration:0.7 animations:^() {
            _scrollCircle.alpha = 1.0;
        } completion:^(BOOL finished) {
        }];
    } else if (_scrollCircle && !_scrollCircle.hidden && self.tableViewV2.contentOffset.y <= MAIN_HEIGHT-200 && !_animating) {
        _scrollCircle.alpha = 1.0;
        _animating = YES;
        [UIView animateWithDuration:0.7 animations:^() {
            _scrollCircle.alpha = 0.01;
        }completion:^(BOOL finished) {
            _scrollCircle.hidden = YES;
            _animating = NO;
        }];
    }
}

- (void)showNavigation
{
    if (self.fakeScroll)
        return;
    
    [super showNavigation];
    
    if (_scrollCircle && !_scrollCircleUp) {
        _scrollCircleUp = YES;
        [UIView animateWithDuration:0.2 animations:^() {
            //_scrollCircle.alpha = 1.0;
            _scrollCircle.frame = CGRectMake(_scrollCircle.frame.origin.x, _scrollCircle.frame.origin.y - 50, _scrollCircle.frame.size.width, _scrollCircle.frame.size.height);
        } completion:^(BOOL finished){
        }];
    }
}

- (void)hideNavigation
{
    if (self.fakeScroll)
        return;
    
    [super hideNavigation];
    
    if (_scrollCircle && _scrollCircleUp) {
        _scrollCircleUp = NO;
        [UIView animateWithDuration:0.2 animations:^() {
            //_scrollCircle.alpha = 0.0;
            _scrollCircle.frame = CGRectMake(_scrollCircle.frame.origin.x, _scrollCircle.frame.origin.y + 50, _scrollCircle.frame.size.width, _scrollCircle.frame.size.height);
        } completion:^(BOOL finished){
            
        }];
    }
}

- (void)removeTutorial
{
    [UIView animateWithDuration:0.3 animations:^{
        _tutorial.alpha = 0.0;
    } completion:^(BOOL finished) {
        [_tutorial removeFromSuperview];
        [_tutorialButton removeFromSuperview];
    }];
}

- (void)refreshTimes
{
    // refresh ages in the article objs
    for (int i = 0; i < [self.tableViewV2 numberOfRowsInSection:0]; i++) {
        if (i == _feedbackIndexPathRow)
            continue;
        Article *art = (Article *)[self.objects objectAtIndex:i];
        [[Delegate sharedDel].parser updateDisplayedAge:art];
    }
    
    // redisplay with animation
    for (UITableViewCell *cell in self.tableViewV2.visibleCells) {
        if ([cell isKindOfClass:[FeedbackCell class]])
            continue;
        
        StreamArticleCell *scell = (StreamArticleCell *)cell;
        Article *art = [[self objects] objectAtIndex:scell.row];
        
        NSString *oldDisplayedAge = scell.streamArticleInnerCellView.age.text;
        NSString *oldDisplayedAgeUnits = scell.streamArticleInnerCellView.timeAgo.text;
        NSString *newDisplayedAge = [NSString stringWithFormat:@"%.0f", [art.displayedAge floatValue]];
        NSString *newDisplayedAgeUnits = art.ageUnits;
        BOOL displayedAgeChanged = ![oldDisplayedAge isEqualToString:newDisplayedAge];
        BOOL displayedAgeUnitsChanged = ![oldDisplayedAgeUnits isEqualToString:newDisplayedAgeUnits];
        
        if (displayedAgeChanged || displayedAgeUnitsChanged) {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.7];
            [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:scell.streamArticleInnerCellView.age cache:YES];
            
            scell.streamArticleInnerCellView.age.text = [NSString stringWithFormat:@"%.0f", [art.displayedAge floatValue]];
            CGFloat origH = scell.streamArticleInnerCellView.age.frame.size.height;
            [scell.streamArticleInnerCellView.age sizeToFit];
            scell.streamArticleInnerCellView.age.frame = CGRectMake(scell.streamArticleInnerCellView.age.frame.origin.x, scell.streamArticleInnerCellView.age.frame.origin.y, scell.streamArticleInnerCellView.age.frame.size.width, origH);
            
            scell.streamArticleInnerCellView.timeAgo.text = art.ageUnits;
            scell.streamArticleInnerCellView.timeAgo.frame = CGRectMake(scell.streamArticleInnerCellView.age.frame.origin.x+scell.streamArticleInnerCellView.age.frame.size.width+1, scell.streamArticleInnerCellView.timeAgo.frame.origin.y, scell.streamArticleInnerCellView.timeAgo.frame.size.width, scell.streamArticleInnerCellView.timeAgo.frame.size.height);
            
            [scell.streamArticleInnerCellView.source setText:art.source];
            scell.streamArticleInnerCellView.source.frame = CGRectMake(scell.streamArticleInnerCellView.timeAgo.frame.origin.x+scell.streamArticleInnerCellView.timeAgo.frame.size.width+5*SCALEUP, scell.streamArticleInnerCellView.source.frame.origin.y, scell.streamArticleInnerCellView.source.frame.size.width, scell.streamArticleInnerCellView.source.frame.size.height);
            
            [UIView commitAnimations];
        }
    }
}

- (void)replaceData
{
    [super replaceData];
    
    // if havent's already inserted feedback cell, and the briefing section already loaded (meaning launchesUntilFeedback and feedbackCellNumber
    // have been set), insert feedback cell if the conditions required for it are satisfied (ios8, launches > launchesUntilFeedback, etc)
    if (_feedbackIndexPathRow == -1 && [Delegate sharedDel].briefingvc.articlesLoaded) {
        [self maybeInsertFeedbackCell];
    }
}

@end
