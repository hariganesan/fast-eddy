//
//  StreamListViewController.h
//  MOST
//
//  Created by Hari Ganesan on 9/16/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import "ArticleListViewController.h"

/** @brief Controller for the stream feature. */
@interface StreamListViewController : ArticleListViewController

@property (nonatomic, strong) UIView *scrollCircle;
@property (nonatomic) bool scrollCircleUp;
@property (nonatomic) bool animating;

/** @discussion indexPath.row at which a feedback cell is rendered.
    Set to -1 if not rendered. */
@property (nonatomic) NSInteger feedbackIndexPathRow;

@property (nonatomic, strong) TutorialView *tutorial;
@property (nonatomic, strong) UIButton *tutorialButton;

- (void)removeTutorial;

/** @discussion Inserts feedback cell if requirements are there (not clicked before, ios 8, 
    launches > launchesUntilFeedback). Inserts a nil article object into self.objects at the 
    index at which the feedback cell is placed. */
- (void)maybeInsertFeedbackCell;
/** @discussion Hides a feedback cell. Removes article object at index of previous feedbackRowIndex.
    Called directly from feedback cell after an event that removes the cell. */
- (void)hideFeedbackRow;

@end
