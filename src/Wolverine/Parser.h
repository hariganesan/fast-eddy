//
//  Parser.h
//  MOST
//
//  Created by Todd Lubin on 6/28/12.
//  Copyright (c) 2012 Campion Designs, LLC. All rights reserved.
//
//  Includes all route requests except images
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class Article;

/** @discussion This class deals with most network requests
    and setting the application state from parsing the responses. */
@interface Parser : NSObject

@property (nonatomic, strong) NSDate *parseStart;
@property (atomic, assign) BOOL networkErrorInView;
@property (atomic, assign) BOOL deprecationErrorInView;

- (void)parseObjectsWithSection:(NSInteger)section;
- (void)updateDisplayedAge:(Article *)art;
- (void)parseWinners;

@end
