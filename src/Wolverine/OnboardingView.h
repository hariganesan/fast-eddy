//
//  OnboardingView.h
//  MOST
//
//  Created by Charlie Vrettos on 10/7/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @discussion  Series of slides displayed on first time launch.
    Includes tutorial up until briefing page is shown. */
@interface OnboardingView : UIView

// animations
@property (nonatomic, strong) UIView *greyMap;
@property (nonatomic, strong) UIView *redMap;
@property (nonatomic, strong) UIView *greyMonitor;
@property (nonatomic, strong) UIView *redMonitor;
@property (nonatomic, strong) UIView *greyPhone;
@property (nonatomic, strong) UIView *redPhone;
@property (nonatomic, strong) UIView *greyReadingArrow;
@property (nonatomic, strong) UIView *redReadingArrow;
@property (nonatomic, strong) UIView *greySharingArrow;
@property (nonatomic, strong) UIView *redSharingArrow;
@property (nonatomic, strong) UIView *greyTwitter;
@property (nonatomic, strong) UIView *redTwitter;
@property (nonatomic, strong) UIView *greyFacebook;
@property (nonatomic, strong) UIView *redFacebook;
@property (nonatomic, strong) UIView *whiteBook;
@property (nonatomic, strong) UIView *redBook;
@property (nonatomic, strong) UILabel *whatTheWorld;
@property (nonatomic, strong) UILabel *isReading;
@property (nonatomic, strong) UILabel *andSharing;
@property (nonatomic, strong) UIView *greyClock;
@property (nonatomic, strong) UIView *redClock;
@property (nonatomic, strong) UILabel *now;
@property (nonatomic, strong) UIView *blackBox;
@property (nonatomic, strong) UILabel *briefMe;
@property (nonatomic) int offScreenDistance;

//push notifications onboarding
@property (nonatomic, strong) UILabel *pushNotifText;
@property (nonatomic, strong) UIButton *okayButton;
@property (nonatomic, strong) UILabel *okay;
@property (nonatomic, strong) UIView *triangle;
@property (nonatomic, strong) UIButton *nahButton;
@property (nonatomic, strong) UIButton *skipButton;

/** @discussion Animates "What the world is reading and sharing now"
    and the book coming in */
- (void)animate;

@end
