//
//  ODRefreshControl.m
//  ODRefreshControl
//
//  Created by Fabio Ritrovato on 6/13/12.
//  Copyright (c) 2012 orange in a day. All rights reserved.
//
// https://github.com/Sephiroth87/ODRefreshControl
//

#import "ODRefreshControl.h"
#import "Delegate.h"

#define kTotalViewHeight    400
#define kOpenedViewHeight   74
#define kMinTopPadding      9
#define kMaxTopPadding      5
#define kMinTopRadius       12.5
#define kMaxTopRadius       16
#define kMinBottomRadius    3
#define kMaxBottomRadius    16
#define kMinBottomPadding   4
#define kMaxBottomPadding   6
#define kMinArrowSize       2
#define kMaxArrowSize       3
#define kMinArrowRadius     5
#define kMaxArrowRadius     7
#define kMaxDistance        53

@interface ODRefreshControl ()

@property (nonatomic, readwrite) BOOL refreshing;
@property (nonatomic, assign) UIScrollView *scrollView;
@property (nonatomic, assign) UIEdgeInsets originalContentInset;

@end

@implementation ODRefreshControl

@synthesize refreshing = _refreshing;
@synthesize scrollView = _scrollView;
@synthesize originalContentInset = _originalContentInset;

static inline CGFloat lerp(CGFloat a, CGFloat b, CGFloat p)
{
    return a + (b - a) * p;
}

- (id)initInScrollView:(UIScrollView *)scrollView {
    return [self initInScrollView:scrollView activityIndicatorView:nil];
}

- (id)initInScrollView:(UIScrollView *)scrollView activityIndicatorView:(UIView *)activity
{
    self = [super initWithFrame:CGRectMake(0, -(kTotalViewHeight + scrollView.contentInset.top) + [Delegate sharedDel].tableViewFrameOffset, scrollView.frame.size.width, kTotalViewHeight)];
    
    if (self) {
        self.scrollView = scrollView;
        self.originalContentInset = scrollView.contentInset;
        
        _requestsFinished = NO;
        _finishedOneFlip = NO;
        
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [scrollView addSubview:self];
        [scrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
        [scrollView addObserver:self forKeyPath:@"contentInset" options:NSKeyValueObservingOptionNew context:nil];
        
        _refreshing = NO;
        _canRefresh = YES;
        _ignoreInset = NO;
        _ignoreOffset = NO;
        _didSetInset = NO;
        _hasSectionHeaders = NO;

        _book = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blank-flip.png"]];
        _book.frame = CGRectMake(MAIN_WIDTH/2 - 70/2, self.frame.size.height - 70, 70, 70);
        [self addSubview:_book];
//        _book.hidden = YES;
        
        
        // Load images
        NSArray *imageNames = @[@"flip0.png", @"flip0.png", @"flip0.png", @"flip0.png", @"flip0.png", @"flip0.png", @"flip0.png", @"flip1.png", @"flip2.png", @"flip3.png", @"flip4.png", @"flip5.png", @"flip6.png", @"flip7.png", @"flip8.png", @"flip9.png", @"flip10.png", @"flip11.png", @"flip12.png", @"flip13.png", @"flip14.png", @"flip15.png", @"flip16.png", @"flip17.png", @"flip18.png", @"flip19.png", @"flip20.png", @"flip21.png", @"flip22.png", @"flip23.png", @"flip24.png"];
        
        NSMutableArray *images = [[NSMutableArray alloc] init];
        for (int i = 0; i < imageNames.count; i++) {
            [images addObject:[UIImage imageNamed:[imageNames objectAtIndex:i]]];
        }
        
        _book.animationImages = images;
        _book.animationDuration = 1.25;
    }
    return self;
}

- (void)dealloc
{
    [self.scrollView removeObserver:self forKeyPath:@"contentOffset"];
    [self.scrollView removeObserver:self forKeyPath:@"contentInset"];
    self.scrollView = nil;
}

- (void)setEnabled:(BOOL)enabled
{
    super.enabled = enabled;
    _shapeLayer.hidden = !self.enabled;
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    [super willMoveToSuperview:newSuperview];
    if (!newSuperview) {
        [self.scrollView removeObserver:self forKeyPath:@"contentOffset"];
        [self.scrollView removeObserver:self forKeyPath:@"contentInset"];
        self.scrollView = nil;
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"contentInset"]) {
        if (!_ignoreInset) {
            self.originalContentInset = [[change objectForKey:@"new"] UIEdgeInsetsValue];
            self.frame = CGRectMake(0, -(kTotalViewHeight + self.scrollView.contentInset.top) + [Delegate sharedDel].tableViewFrameOffset, self.scrollView.frame.size.width, kTotalViewHeight);
        }
        return;
    }
    
    if (!self.enabled || _ignoreOffset) {
        return;
    }
    
    CGFloat offset = [[change objectForKey:@"new"] CGPointValue].y + self.originalContentInset.top;
    
    _book.frame = CGRectMake(MAIN_WIDTH/2 + offset/2, self.frame.size.height + offset, -1*offset, -1*offset);
    
    if (_refreshing) {
        if (offset != 0) {
            // Keep thing pinned at the top
            
            [CATransaction begin];
            [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
            _shapeLayer.position = CGPointMake(0, kMaxDistance + offset + kOpenedViewHeight);
            [CATransaction commit];

            _book.hidden = NO;
            
            // hacky way to only start the pages flipping when user releases the pull
            //  but it actualyl starts refreshing when they hit distance=0 percentage=0 below
            //  a better fix would recognize when a user released the pull, figure out if they pulled
            //  down far enough, and only then trigger "UIControlEventValueChanged" which calls
            //  articlelistview's refresh
//            if (offset == -1 * kOpenedViewHeight){
//                [_book startAnimating];
//            }

            _ignoreInset = YES;
            _ignoreOffset = YES;
            
            if (offset < 0) {
                // Set the inset depending on the situation
                if (offset >= -kOpenedViewHeight) {
                    if (!self.scrollView.dragging) {
                        if (!_didSetInset) {
                            _didSetInset = YES;
                            _hasSectionHeaders = NO;
                            if ([self.scrollView isKindOfClass:[UITableView class]]){
                                for (int i = 0; i < [(UITableView *)self.scrollView numberOfSections]; ++i) {
                                    if ([(UITableView *)self.scrollView rectForHeaderInSection:i].size.height) {
                                        _hasSectionHeaders = YES;
                                        break;
                                    }
                                }
                            }
                        }
                        if (_hasSectionHeaders) {
                            [self.scrollView setContentInset:UIEdgeInsetsMake(MIN(-offset, kOpenedViewHeight) + self.originalContentInset.top, self.originalContentInset.left, self.originalContentInset.bottom, self.originalContentInset.right)];
                        } else {
                            [self.scrollView setContentInset:UIEdgeInsetsMake(kOpenedViewHeight + self.originalContentInset.top, self.originalContentInset.left, self.originalContentInset.bottom, self.originalContentInset.right)];
                        }
                    } else if (_didSetInset && _hasSectionHeaders) {
                        [self.scrollView setContentInset:UIEdgeInsetsMake(-offset + self.originalContentInset.top, self.originalContentInset.left, self.originalContentInset.bottom, self.originalContentInset.right)];
                    }
                }
            } else if (_hasSectionHeaders) {
                [self.scrollView setContentInset:self.originalContentInset];
            }
            _ignoreInset = NO;
            _ignoreOffset = NO;
        }
        return;
    } else {
//        _book.hidden = YES;
        // Check if we can trigger a new refresh and if we can draw the control
        BOOL dontDraw = NO;
        if (!_canRefresh) {
            if (offset >= 0) {
                // We can refresh again after the control is scrolled out of view
                _canRefresh = YES;
                _didSetInset = NO;
            } else {
                dontDraw = YES;
            }
        } else {
            if (offset >= 0) {
                // Don't draw if the control is not visible
                dontDraw = YES;
            }
        }
        if (offset > 50 && _lastOffset > offset && !self.scrollView.isTracking) {
            // If we are scrolling too fast, don't draw, and don't trigger unless the scrollView bounced back
            _canRefresh = NO;
            dontDraw = YES;
        }
        if (dontDraw) {
            _shapeLayer.path = nil;
            _shapeLayer.shadowPath = nil;
//            _arrowLayer.path = nil;
            _highlightLayer.path = nil;
            _lastOffset = offset;
            return;
        }
    }
    
    _lastOffset = offset;
    
    BOOL triggered = NO;
    
    CGMutablePathRef path = CGPathCreateMutable();
    
    //Calculate some useful points and values
    CGFloat verticalShift = MAX(0, -((kMaxTopRadius + kMaxBottomRadius + kMaxTopPadding + kMaxBottomPadding) + offset));
    CGFloat distance = MIN(kMaxDistance, fabs(verticalShift));
    CGFloat percentage = 1 - (distance / kMaxDistance);
    
    CGFloat currentTopPadding = lerp(kMinTopPadding, kMaxTopPadding, percentage);
    CGFloat currentTopRadius = lerp(kMinTopRadius, kMaxTopRadius, percentage);
    CGFloat currentBottomRadius = lerp(kMinBottomRadius, kMaxBottomRadius, percentage);
    CGFloat currentBottomPadding =  lerp(kMinBottomPadding, kMaxBottomPadding, percentage);
    
    CGPoint bottomOrigin = CGPointMake(floor(self.bounds.size.width / 2), self.bounds.size.height - currentBottomPadding -currentBottomRadius);
    CGPoint topOrigin = CGPointZero;
    if (distance == 0) {
        topOrigin = CGPointMake(floor(self.bounds.size.width / 2), bottomOrigin.y);
    } else {
        topOrigin = CGPointMake(floor(self.bounds.size.width / 2), self.bounds.size.height + offset + currentTopPadding + currentTopRadius);
        if (percentage == 0) {
            bottomOrigin.y -= (fabs(verticalShift) - kMaxDistance);
            triggered = YES;
        }
    }

    if (triggered && !self.scrollView.dragging) {
        _finishedOneFlip = NO;
        _requestsFinished = NO;
        _book.animationRepeatCount=1;
        [self performSelector:@selector(bookFlippedOnce) withObject:nil
                   afterDelay:_book.animationDuration];
        
        [_book startAnimating];
        self.refreshing = YES;
        _canRefresh = NO;
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    }
    
    CGPathRelease(path);
}

- (void)endRefreshing
{
    _requestsFinished = YES;
    if (_refreshing && _finishedOneFlip) {
        [self hide];
    }
}

- (void)bookFlippedOnce
{
    _finishedOneFlip = YES;
    
    // if requests already finished, hide refreshcontrol
    if (_requestsFinished)
        [self hide];
    // else allow ALV calling endRefreshing to hide refreshcontrol
    else {
        _book.animationRepeatCount=0;
        [_book startAnimating];
    }
}

- (void)hide
{
    [_book stopAnimating];
    self.refreshing = NO;
    // Create a temporary retain-cycle, so the scrollView won't be released
    // halfway through the end animation.
    // This allows for the refresh control to clean up the observer,
    // in the case the scrollView is released while the animation is running
    __block UIScrollView *blockScrollView = self.scrollView;
    [UIView animateWithDuration:0.4 animations:^{
        _ignoreInset = YES;
        [blockScrollView setContentInset:self.originalContentInset];
        _ignoreInset = NO;
        //            _activity.alpha = 0;
        //            _activity.layer.transform = CATransform3DMakeScale(0.1, 0.1, 1);
    } completion:^(BOOL finished) {
        [_shapeLayer removeAllAnimations];
        _shapeLayer.path = nil;
        _shapeLayer.shadowPath = nil;
        _shapeLayer.position = CGPointZero;
        //            [_arrowLayer removeAllAnimations];
        //            _arrowLayer.path = nil;
        [_highlightLayer removeAllAnimations];
        _highlightLayer.path = nil;
        // We need to use the scrollView somehow in the end block,
        // or it'll get released in the animation block.
        _ignoreInset = YES;
        [blockScrollView setContentInset:self.originalContentInset];
        _ignoreInset = NO;
    }];
}

@end
