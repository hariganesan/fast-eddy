
/*!
@project    TabBarKit
@header     TBKTabBarController.h
@copyright  (c) 2010 - 2011, David Morford
*/

#import <UIKit/UIKit.h>
#import "TBKTabBar.h"

@class ArticleListViewController;

@protocol  TBKTabBarControllerDelegate;

@interface TBKTabBarController : UIViewController <TBKTabBarDelegate>

// parallax stuff
typedef enum {
    DrawerSideNone,
    DrawerSideLeft,
    DrawerSideRight
} DrawerSide;

//typedef void (^VisualStateBlock)(ArticleListViewController * drawerController, DrawerSide drawerSide, CGFloat percentVisible);
@property (nonatomic) CGRect startingPanRect;
@property (nonatomic) CGRect startingImageRect;
@property (nonatomic) DrawerSide openSide;
//@property (nonatomic, copy) VisualStateBlock drawerVisualState;
@property (nonatomic) BOOL animatingDrawer;
@property (nonatomic) CGFloat parallaxFactor;


@property (nonatomic, weak) id <TBKTabBarControllerDelegate> delegate;

@property (nonatomic, strong) TBKTabBar *tabBar;
@property (nonatomic, assign) NSUInteger selectedIndex;

@property (nonatomic, strong) NSArray *viewControllers;
@property (nonatomic, strong) ArticleListViewController *selectedViewController;

@property (nonatomic, strong, readonly) UINavigationController *moreNavigationController;
@property (nonatomic, copy) NSArray *customizableViewControllers;

// used so that when a section is selected
// 1. whatever section is index 0 isn't 'clicked' triggering analytics upon init of the tab bar
// 2. whenever a section is clicked from the simple 2x3 section selection view, analytics is only triggered once
@property (nonatomic) int initialSectionSelected;

-(void) setViewControllers:(NSArray *)controllers animated:(BOOL)animated;
-(void) showNavigation;
-(void) hideNavigation;

@end

@protocol TBKTabBarControllerDelegate <NSObject>

@optional
-(BOOL) tabBarController:(TBKTabBarController *)tabBarController shouldSelectViewController:(ArticleListViewController *)viewController;
-(void) tabBarController:(TBKTabBarController *)tabBarController didSelectViewController:(ArticleListViewController *)viewController;

@end
