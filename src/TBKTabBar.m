
#import "TBKTabBar.h"
#import "Delegate.h"
#import "TBKTabBarController.h"

@implementation TBKArrowLayer

-(id) init
{
	self = [super init];
	if (!self) {
		return nil;
	}

	self.bounds = CGRectMake(0, 0, 40, 20);
	self.position = CGPointMake(0,0);
	self.anchorPoint = CGPointMake(0,0);
	
	UIBezierPath *trianglePath = [UIBezierPath bezierPath];
	[trianglePath moveToPoint:CGPointMake(CGRectGetMinX(self.frame), CGRectGetMaxY(self.frame) - 1.5)];
	[trianglePath addLineToPoint:CGPointMake(CGRectGetMidX(self.frame) - 0.25, CGRectGetMinY(self.frame))];
	[trianglePath addLineToPoint:CGPointMake(CGRectGetMidX(self.frame) + 0.25, CGRectGetMinY(self.frame))];
	[trianglePath addLineToPoint:CGPointMake(CGRectGetMaxX(self.frame), CGRectGetMaxY(self.frame) - 1.5)];
	self.path = trianglePath.CGPath;
    self.fillColor = [Delegate sharedDel].sectionBackgroundColor.CGColor;
	
	return self;
}

@end

@interface TBKTabBar ()
@property (atomic, assign) BOOL selectingTab;
-(void) setArrowPositionAnimated:(BOOL)animated;

@end

@implementation TBKTabBar

@synthesize delegate;
@synthesize items;
@synthesize selectedTabBarItem;
@synthesize arrowLayer;

+(Class) layerClass
{
	return [CAGradientLayer class];
}

-(id) initWithFrame:(CGRect)aFrame
{
	self = [super initWithFrame:aFrame];
	if (!self) {
		return nil;
	}

	self.userInteractionEnabled = YES;
    self.selectingTab = NO;
	self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin;
	
    arrowLayer = [[TBKArrowLayer alloc] init];
    CGRect arrowFrame = self.arrowLayer.frame;
    arrowFrame.origin.y = -(arrowFrame.size.height - 2.5f);
    arrowLayer.frame = arrowFrame;
    
    self.backgroundColor = [Delegate sharedDel].sectionBackgroundColor;
    [self.layer addSublayer:self.arrowLayer];
	return self;
}

-(void) setItems:(NSArray *)aTabBarItemArray
{    
	for (UIButton *tabBarItem in items) {
		[tabBarItem removeFromSuperview];
	}
    
	items = aTabBarItemArray;

	if ([items count]) {
        [self didSelectTabBarItem:[items objectAtIndex:[Delegate sharedDel].tabsectvc.initialSectionSelected]];
		[self.delegate tabBar:self didSelectTabAtIndex:[Delegate sharedDel].tabsectvc.initialSectionSelected];
	}
    
	for (UIButton *tabBarItem in items) {
		tabBarItem.userInteractionEnabled = YES;
		[tabBarItem addTarget:self action:@selector(didSelectTabBarItem:) forControlEvents:UIControlEventTouchDown];
	}
}

-(void) didSelectTabBarItem:(UIButton *)sender
{    
    if (self.selectingTab) {
        return;
    }
    
    self.selectingTab = YES;
    [self.selectedTabBarItem setSelected:NO];
    [sender setSelected:YES];
    self.selectedTabBarItem = sender;
	[self.delegate tabBar:self didSelectTabAtIndex:[self.items indexOfObject:sender]];
    [self setArrowPositionAnimated:YES];
    self.selectingTab = NO;
}

-(void) setArrowPositionWithOffset:(CGFloat)offset
{
    CGRect arrowLayerFrame		= self.arrowLayer.frame;
    arrowLayerFrame.origin.x	= self.selectedTabBarItem.frame.origin.x + ((self.selectedTabBarItem.frame.size.width / 2) - (arrowLayerFrame.size.width / 2))-(offset/MAIN_WIDTH*self.selectedTabBarItem.frame.size.width);
    self.arrowLayer.frame		= arrowLayerFrame;
}

-(void) setArrowPositionAnimated:(BOOL)animated
{
	if (animated) {
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:1.5];
	}
	CGRect arrowLayerFrame		= self.arrowLayer.frame;
	arrowLayerFrame.origin.x	= self.selectedTabBarItem.frame.origin.x + ((self.selectedTabBarItem.frame.size.width / 2) - (arrowLayerFrame.size.width / 2));
	self.arrowLayer.frame		= arrowLayerFrame;
	if (animated) {
		[UIView commitAnimations];
	}
}

-(void) layoutSubviews
{
	[super layoutSubviews];
	CGRect currentBounds = self.bounds;
	currentBounds.size.width /= self.items.count;

    for (UIButton *tab in self.items) {
		tab.frame = currentBounds;
		currentBounds.origin.x += currentBounds.size.width;
		[self addSubview:tab];
	}
    
    [self setArrowPositionAnimated:NO];
}

@end
