//
//  NSMutableArray.m
//  BriefMe
//
//  Created by Hari Ganesan on 8/25/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import "NSMutableArray+Queue.h"

@implementation NSMutableArray (Queue)

- (id) dequeue {
    if ([self count] == 0) return nil; // to avoid raising exception
    
    id headObject = [self objectAtIndex:0];
    if (headObject != nil) {
        [self removeObjectAtIndex:0];
    }
    
    return headObject;
}

- (void) enqueue:(id)anObject {
    [self addObject:anObject];
}

@end
