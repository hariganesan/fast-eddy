//
//  NSMutableArray.h
//  BriefMe
//
//  Created by Hari Ganesan on 8/25/15.
//  Copyright (c) 2015 BriefMe Media, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

/** @discussion An extension of NSMutableArray that handles queue functionality.
    This is pulled from http://stackoverflow.com/a/936497/1480438 */
@interface NSMutableArray (Queue)

/** @brief Pop an object off the front of a queue. */
- (id) dequeue;
/** @brief Push an object on the back of the queue. */
- (void) enqueue:(id)anObject;

@end
