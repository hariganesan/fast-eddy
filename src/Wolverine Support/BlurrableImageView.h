//
//  BlurrableImageView
//  lgcityguide2012
//
//  Created by Luis Duarte on 7/12/12.
//  Copyright (c) 2012 Harvard Student Agencies. All rights reserved.
//
//  Includes all image requests and processing (blur)
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/** @brief An image view that allows switching to blurred versions
    of an image. */
@interface BlurrableImageView : UIImageView<UIWebViewDelegate>

@property (nonatomic, strong) UIActivityIndicatorView *aIView;
/** @discussion Array of NSData, each a blurred image, one of which
    blurIndex will index. Stored as NSData rather than UIImages
    because it seems to reduce memory. */
@property (nonatomic, strong) NSMutableArray *blurredImagesData;
/** @discussion Index in blurredImagesData of the current displayed
    blurred image. */
@property (nonatomic) NSInteger blurIndex;
/** @discussion URL of the image blurred. Persists even if backup
    image is used. Format /section=politics&id=2&screen=4|5|6|6p */
@property (nonatomic, strong) NSString *imageUrl;
/** @discussion URL of the article that the image corresponds to.
    Used to detect when either #1 article changed or imageNumber
    changed, which means there is a new image to be blurred. */
@property (nonatomic, strong) NSString *correspondingArticleUrl;
@property (nonatomic, strong) UIImage *originalImage;

/** @brief Sets alpha value depending on ALVC offset. */
- (void)adjustAlphaWithOffset:(CGFloat)offset;
/** @discussion Sets the image to blur which is pushed onto the
    queue in NavController. */
- (void)setOriginalImageToBlur:(UIImage *)image;
/** @discussion Blurs a new image and sets self.image to it. 
    This function also sets the array of blurredImages afterwards. */
- (void)blurAndSetImages;
/** @discussion Sets _blurIndex, and sets blurred image unless
    it's a new image and all blurred images havent been created
    yet. Animates the image change if animated==True with the
    given duration. Stops AIView from spinning if it is. */
- (void)updateImageToBlurIndex:(NSInteger)blurIndex animated:(BOOL)animated withDuration:(CGFloat)duration forTheFirstTime:(BOOL)firstUpdate;

@end
