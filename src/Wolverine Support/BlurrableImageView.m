//
//  BlurrableImageView
//  lgcityguide2012
//
//  Created by Luis Duarte on 7/12/12.
//  Copyright (c) 2012 Harvard Student Agencies. All rights reserved.
//

#import "BlurrableImageView.h"
#import "ArticleListViewController.h"
#import "Delegate.h"
#import "AFHTTPRequestOperationManager.h"
#import "UIImage+ImageEffects.h"
#import "NavController.h"

@interface BlurrableImageView ()

@property (nonatomic) BOOL newImageDisplayed;

@end

@implementation BlurrableImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _aIView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        _aIView.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
        [self addSubview:_aIView];
        [_aIView startAnimating];
        
        _correspondingArticleUrl = nil;
        _imageUrl = nil;
        _newImageDisplayed = NO;
        _originalImage = nil;
        _blurIndex = 0;

        // these will be set later once the route loads and we have the img url
        _blurredImagesData = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)setOriginalImageToBlur:(UIImage *)image
{
    _originalImage = image;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"addToBlurQueue" object:self];
}

- (void)blurAndSetImages
{
    if (self.originalImage) {
        UIImage *im = self.originalImage;
        _newImageDisplayed = NO;
        // crop iphone 4 image if necessary
        if (MAIN_HEIGHT == IPHONE_4_HEIGHT && im.size.height/im.size.width != IPHONE_4_HEIGHT/IPHONE_4_WIDTH) {
            CGRect rect = CGRectMake(0, 0, IPHONE_4_WIDTH, IPHONE_4_HEIGHT);
            CGImageRef imageRef = CGImageCreateWithImageInRect([im CGImage], rect);
            im = [UIImage imageWithCGImage:imageRef];
            CGImageRelease(imageRef);
        }
        
        [self createBlurredImages:im];
        [Delegate sharedDel].nc.blurring = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"performedBlur" object:nil];
        [self performSelectorOnMainThread:@selector(updateImageToNewImage) withObject:nil waitUntilDone:NO];
        [_aIView removeFromSuperview];
    }
}

- (void)createBlurredImages:(UIImage *)im
{
    // Now that the bg image is set, create the array of blurred images in NSData form
    [_blurredImagesData removeAllObjects];

    if (im){
        NSData *imageData = [[NSData alloc] initWithData:UIImageJPEGRepresentation(im, 0.5)];
        [_blurredImagesData addObject:imageData];
        for (int i = 1; i < NUMBLURIMAGES; ++i) {
            // if not iPhone 4, blur
            if (MAIN_HEIGHT != IPHONE_4_HEIGHT) {
                im = [im applyBlurWithRadius:1*BLURPOWER tintColor:nil saturationDeltaFactor:1.0 maskImage:nil];
            }
            
            imageData = [[NSData alloc] initWithData:UIImageJPEGRepresentation(im, 0.5)];
            [_blurredImagesData addObject:imageData];
        }
    } else {
        NSLog(@"ERROR passed nil image to createBlurredImages!");
    }
}

- (void)updateImageToBlurIndex:(NSInteger)blurIndex animated:(BOOL)animated withDuration:(CGFloat)duration forTheFirstTime:(BOOL)firstUpdate
{
    if (!firstUpdate && blurIndex == _blurIndex) {
        // self.image already == [UIImage imageWithData:[_blurredImagesData objectAtIndex:_blurIndex]] so no need to change it
        return;
    }
    
    _blurIndex = blurIndex;
    
    // don't actually display the image if we havent finished creating all the of blurred images and calling this
    // function with firstUpdate == True; This ensures that the image animates in the first time and that all blurred
    // images have been created before displaying any
    if (!firstUpdate && !_newImageDisplayed) {
        return;
    }
    
    if ([_blurredImagesData count] <= _blurIndex) {
        NSLog(@"ERROR: called updateImage... but don't have the blurred image.. doing nothing");
        return;
    }

    UIImage *newImage = [UIImage imageWithData:[_blurredImagesData objectAtIndex:_blurIndex]];
    
    if (animated) {
        [UIView transitionWithView:self duration:duration options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            self.image = newImage;
        } completion:^(BOOL finished) {
            if (firstUpdate) {
                _newImageDisplayed = YES;
            }
            // changing the img was disabled during this animation, so if user scrolled during
            // the animation then stopped, the wrong img (wrong blur index) may be displayed
            // so just swap it to the right one with a quick animation.
            if (blurIndex != _blurIndex) {
                [UIView transitionWithView:self duration:0.3f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                    self.image = [UIImage imageWithData:[_blurredImagesData objectAtIndex:_blurIndex]];
                } completion:^(BOOL finished) {}];
            }
        }];
    } else {
        self.image = newImage;
    }
    [_aIView stopAnimating];
}

- (void)adjustAlphaWithOffset:(CGFloat)offset
{
    CGFloat alphaLimit = 0.7;
    CGFloat offsetLimit = BLURSPEED*NUMBLURIMAGES;
    
    if (offset < 0) {
        self.alpha = 1;
    } else if (offset > offsetLimit) {
        self.alpha = alphaLimit;
    } else {
        self.alpha = 1-(1-alphaLimit)*offset/offsetLimit;
    }
}

- (void)updateImageToNewImage
{
    [self updateImageToBlurIndex:_blurIndex animated:YES withDuration:1.0f forTheFirstTime:YES];
}

@end
