//
//  UITabBarController+hidable.h
//  MOST
//
//  Created by Hari Ganesan on 6/17/14.
//  Copyright (c) 2014 Campion Designs, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UITabBarController (hidable)

- (void)showNavigation;
- (void)hideNavigation;
- (void)setTabBarHidden:(BOOL)hidden animated:(BOOL)animated;

@end
